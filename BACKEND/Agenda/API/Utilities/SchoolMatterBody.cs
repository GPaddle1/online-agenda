﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Utilities
{
    public class SchoolMatterBody
    {

        public Guid TeacherGuid { get; set; }
        public string Name { get; set; }
    }
}
