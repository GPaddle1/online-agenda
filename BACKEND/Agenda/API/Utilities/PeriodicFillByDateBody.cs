﻿using Agenda;
using System;

namespace API.Utilities
{
    public class PeriodicFillByDateBody
    {
        public Guid SchoolMatterGuid { get; set; }
        public DayOfWeek TargetDay { get; set; }
        public EPeriod TargetPeriod { get; set; }
        public bool FinishWithBlockGeneration { get; set; } = false;
    }
}
