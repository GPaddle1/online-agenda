﻿using Agenda;
using System.Collections.Generic;

namespace API.Utilities
{
    public class TeacherBody : UserBody
    {

        public List<Period> Unavailability { get; set; }

        public TeacherBody(string firstName, string lastName, string email, string password, List<Period> unavailability) : base(firstName, lastName, email, password)
        {
            Unavailability = unavailability;
        }
    }
}
