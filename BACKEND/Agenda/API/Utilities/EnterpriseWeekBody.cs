﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Utilities
{
    public class EnterpriseWeekBody
    {
        public int weekNumber { get; set; }
        public Guid promotionGuid { get; set; }
    }
}
