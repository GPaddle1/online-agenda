﻿using System;

namespace API.Utilities
{
    public class StudentBody : UserBody
    {
        public Guid PromotionGuid { get; set; }
        public StudentBody(string firstName, string lastName, string email, string password, Guid promotionGuid) : base(firstName, lastName, email, password)
        {
            PromotionGuid = promotionGuid;
        }
    }
}
