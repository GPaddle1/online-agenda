﻿namespace API.Utilities
{

    public class UserBody
    {

        public string firstName { get; set; }
        public string lastName { get; set; }
        public string email { get; set; }
        public string password { get; set; }

        public UserBody(string firstName, string lastName, string email, string password)
        {
            this.firstName = firstName;
            this.lastName = lastName;
            this.email = email;
            this.password = password;
        }

        public override string ToString()
        {
            return "[\n" +
                $"firstName = {firstName}\n" +
                $"lastName = {lastName}\n" +
                $"email = {email}\n" +
                $"password = {password}]\n";
        }
    }
}
