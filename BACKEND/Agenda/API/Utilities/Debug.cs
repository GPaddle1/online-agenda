﻿using Agenda;

namespace API.Utilities
{
    public class Debug
    {
        public SchoolMatter SchoolMatter { get; set; }
        public SemesterSchedule SemesterSchedule { get; set; }
        public Student Student { get; set; }
        public Teacher Teacher { get; set; }
        public HeadTeacher HeadTeacher { get; set; }
        public Promotion Promotion { get; set; }

    }
}
