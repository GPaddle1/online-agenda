﻿using Agenda;
using System;

namespace API.Utilities
{
    public class SemesterScheduleBody
    {
        public Guid promotionGuid { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        public string SemesterScheduleName { get; set; }
    }
}
