﻿using Agenda;
using Agenda.Repository;
using Microsoft.AspNetCore.Http;
using System;
using System.Linq;
using System.Security.Claims;

namespace CNAMScheduleAPI.Controllers
{


    public class AuthenticationHelper
    {
        public static string GUID = "Guid";
        public static string UNAUTHORIZED_ACTION_MESSAGE = "Only head teacher can do this action";


        public static bool CanSeeSemesterSchedule(HttpContext httpContext, SemesterSchedule semesterSchedule)
        {
            var currentUser = httpContext.User;

            if (IsAuthenticated(httpContext) && currentUser.HasClaim(c => c.Type == GUID))
            {
                IHaveASchedule user;

                try
                {
                    user = (IHaveASchedule)InMemoryRepositoryUser.GetInstance().GetByGuid(new Guid(GetPropsValue(currentUser, GUID)));
                    return user.CanConsultSemesterSchedule(semesterSchedule);
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return false;
        }

        public static bool IsTheRightUser(HttpContext httpContext, User targetUser)
        {
            var currentUser = httpContext.User;

            if (IsAuthenticated(httpContext) && currentUser.HasClaim(c => c.Type == GUID))
            {
                return GetPropsValue(currentUser, GUID) == targetUser.UserGuid.ToString();
            }
            return false;
        }

        private static string GetPropsValue(ClaimsPrincipal currentUser, string targetValue)
        {
            return currentUser.Claims.FirstOrDefault(c =>
            {
                return c.Type == targetValue;
            }).Value;
        }

        public static bool IsAuthenticated(HttpContext httpContext)
        {
            var currentUser = httpContext.User;
            return currentUser.HasClaim(c => c.Type == ClaimTypes.Role);
        }
    }
}
