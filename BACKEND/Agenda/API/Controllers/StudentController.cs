﻿using Agenda;
using Agenda.Repository;
using API.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CNAMScheduleAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    public class StudentController : ControllerBase
    {
        private readonly ILogger<StudentController> _logger;

        public StudentController(ILogger<StudentController> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// Get all students
        [HttpGet]
        public IEnumerable<Student> Get()
        {
            InMemoryRepositoryUser userRepository = InMemoryRepositoryUser.GetInstance();

            return userRepository.Get().Where(user => user is Student).ToList().Cast<Student>();
        }


        /// <summary>
        /// Get one student by guid
        [HttpGet]
        [Route("{studentGuid}")]
        [ProducesResponseType(typeof(Student), 200)]
        public Student GetOneByGuid([FromRoute] Guid studentGuid)
        {
            InMemoryRepositoryUser userRepository = InMemoryRepositoryUser.GetInstance();

            User user;
            try
            {
                user = userRepository.GetByGuid(studentGuid);
            }
            catch (Exception)
            {
                return null;
            }
            return user is Student ? (Student)user : null;
        }
    }
}
