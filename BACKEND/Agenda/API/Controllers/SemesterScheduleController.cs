﻿using Agenda;
using Agenda.Repository;
using API.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CNAMScheduleAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    public class SemesterScheduleController : ControllerBase
    {
        private readonly ILogger<SemesterScheduleController> _logger;

        public SemesterScheduleController(ILogger<SemesterScheduleController> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// Get all semesterSchedules
        [HttpGet]
        public IEnumerable<SemesterSchedule> Get()
        {
            InMemoryRepositorySemesterSchedule semesterScheduleRepository = InMemoryRepositorySemesterSchedule.GetInstance();

            return semesterScheduleRepository.Get();
        }

        /// <summary>
        /// Create a new schedule for a given promotion
        [HttpPost]
        [Authorize(Policy = "HeadTeacher")]
        [ProducesResponseType(typeof(SemesterSchedule), 200)]
        public IActionResult CreateNewSemesterSchedule([FromBody] SemesterScheduleBody semesterScheduleBody)
        {

            InMemoryRepositorySemesterSchedule semesterScheduleRepository = InMemoryRepositorySemesterSchedule.GetInstance();
            InMemoryRepositoryPromotion promotionRepository = InMemoryRepositoryPromotion.GetInstance();
            Promotion promotion = promotionRepository.GetByGuid(semesterScheduleBody.promotionGuid);

            SemesterSchedule obj = new SemesterSchedule(promotion, semesterScheduleBody.startDate, semesterScheduleBody.endDate, semesterScheduleBody.SemesterScheduleName);

            Guid semesterScheduleGuid = obj.SemesterScheduleGuid;
            bool postSuccess = semesterScheduleRepository.Post(obj);

            return postSuccess ? Accepted(value: $"Operation worked well, item created with Guid {semesterScheduleGuid}") : UnprocessableEntity(ResponseHelper.AN_ERROR_OCCURED);
        }

        /// <summary>
        /// Get one semesterSchedule detail
        [HttpGet]
        [Route("{semesterScheduleGuid}")]
        [ProducesResponseType(typeof(SemesterSchedule), 200)]
        public SemesterSchedule GetByGuid([FromRoute] Guid semesterScheduleGuid)
        {
            InMemoryRepositorySemesterSchedule semesterScheduleRepository = InMemoryRepositorySemesterSchedule.GetInstance();

            try
            {
                return semesterScheduleRepository.GetByGuid(semesterScheduleGuid);
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Get one semesterSchedule detail
        [HttpGet]
        [Authorize(Policy = "Student")]
        [Route("{semesterScheduleGuid}/detail/student/{studentGuid}")]
        [ProducesResponseType(typeof(SemesterSchedule), 200)]
        public IActionResult GetStudentDetail([FromRoute] Guid semesterScheduleGuid, [FromRoute] Guid studentGuid)
        {
            InMemoryRepositorySemesterSchedule semesterScheduleRepository = InMemoryRepositorySemesterSchedule.GetInstance();
            SemesterSchedule semesterSchedule;
            try
            {
                semesterSchedule = semesterScheduleRepository.GetByGuid(semesterScheduleGuid);
            }
            catch (Exception)
            {
                return UnprocessableEntity(ResponseHelper.AN_ERROR_OCCURED);
            }

            if (!AuthenticationHelper.CanSeeSemesterSchedule(HttpContext, semesterSchedule))
            {
                return Unauthorized("Impossible to see the schedule");
            }

            InMemoryRepositoryStudent studentRepository = InMemoryRepositoryStudent.GetInstance();
            Student student;
            try
            {
                student = studentRepository.GetByGuid(studentGuid);
            }
            catch (Exception)
            {
                return UnprocessableEntity(ResponseHelper.AN_ERROR_OCCURED);
            }


            return Ok(MapLessons(semesterSchedule.GetLessons(student)));
        }

        /// <summary>
        /// Get one semesterSchedule detail
        [HttpGet]
        [Authorize(Policy = "Teacher")]
        [Route("{semesterScheduleGuid}/detail/teacher/{teacherGuid}")]
        [ProducesResponseType(typeof(SemesterSchedule), 200)]
        public IActionResult GetTeacherDetail([FromRoute] Guid semesterScheduleGuid, [FromRoute] Guid teacherGuid)
        {
            InMemoryRepositorySemesterSchedule semesterScheduleRepository = InMemoryRepositorySemesterSchedule.GetInstance();
            SemesterSchedule semesterSchedule;
            try
            {
                semesterSchedule = semesterScheduleRepository.GetByGuid(semesterScheduleGuid);
            }
            catch (Exception)
            {
                return UnprocessableEntity("SemesterSchedule is not retrievable");
            }

            if (!AuthenticationHelper.CanSeeSemesterSchedule(HttpContext, semesterSchedule))
            {
                return Unauthorized(AuthenticationHelper.UNAUTHORIZED_ACTION_MESSAGE);
            }

            InMemoryRepositoryTeacher teacherRepository = InMemoryRepositoryTeacher.GetInstance();

            Teacher teacher;

            try
            {
                teacher = teacherRepository.GetByGuid(teacherGuid);
            }
            catch (Exception)
            {
                return UnprocessableEntity(ResponseHelper.AN_ERROR_OCCURED);
            }


            return Ok(MapLessons(semesterSchedule.GetLessons(teacher)));
        }

        public static IEnumerable<Lesson> MapLessons(Dictionary<Period, SchoolMatter> lessons)
        {
            IEnumerable<Lesson> lessonsToReturn = new List<Lesson>();

            lessonsToReturn = lessons.Select(x => new Lesson(x.Key, x.Value));

            return lessonsToReturn;
        }
    }
}
