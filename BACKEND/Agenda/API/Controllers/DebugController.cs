﻿using Agenda;
using Agenda.Repository;
using API.Utilities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Net.Mail;

namespace CNAMScheduleAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class DebugController : ControllerBase
    {
        private readonly ILogger<DebugController> _logger;

        public DebugController(ILogger<DebugController> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// Get debug values
        [HttpGet]
        public Debug GetDebug()
        {
            InMemoryRepositorySchoolMatter inMemoryRepositorySchoolMatter = InMemoryRepositorySchoolMatter.GetInstance();

            InMemoryRepositorySemesterSchedule inMemoryRepositorySemesterSchedule = InMemoryRepositorySemesterSchedule.GetInstance();

            InMemoryRepositoryStudent inMemoryRepositoryStudent = InMemoryRepositoryStudent.GetInstance();
            InMemoryRepositoryTeacher inMemoryRepositoryTeacher = InMemoryRepositoryTeacher.GetInstance();
            InMemoryRepositoryHeadTeacher inMemoryRepositoryHeadTeacher = InMemoryRepositoryHeadTeacher.GetInstance();

            InMemoryRepositoryPromotion inMemoryRepositoryPromotion = InMemoryRepositoryPromotion.GetInstance();

            Debug debug = new Debug
            {
                SchoolMatter = inMemoryRepositorySchoolMatter.Get().ElementAt(0),
                SemesterSchedule = inMemoryRepositorySemesterSchedule.Get().ElementAt(0),
                Student = inMemoryRepositoryStudent.Get().ElementAt(0),
                Teacher = inMemoryRepositoryTeacher.Get().ElementAt(0),
                HeadTeacher = inMemoryRepositoryHeadTeacher.Get().ElementAt(0),
                Promotion = inMemoryRepositoryPromotion.Get().ElementAt(0)
            };

            return debug;
        }

        /// <summary>
        /// Set debug values
        [HttpPost]
        public IActionResult SetValues()
        {

            InMemoryRepositorySchoolMatter inMemoryRepositorySchoolMatter = InMemoryRepositorySchoolMatter.GetInstance();

            Debug debug = GetDebug();

            SchoolMatter schoolMatter = inMemoryRepositorySchoolMatter.Get().First();
            debug.SemesterSchedule.AddLessonDuration(schoolMatter, 5);
            debug.SemesterSchedule.RegisterSchoolMatterByBlock();

            return Accepted();
        }
    }
}
