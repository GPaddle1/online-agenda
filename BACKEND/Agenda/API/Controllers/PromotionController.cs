﻿using Agenda;
using Agenda.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;

namespace CNAMScheduleAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    public class PromotionController : ControllerBase
    {
        private readonly ILogger<PromotionController> _logger;

        public PromotionController(ILogger<PromotionController> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// Get all promotions
        [HttpGet]
        public IEnumerable<Promotion> Get()
        {
            InMemoryRepositoryPromotion promotionRepository = InMemoryRepositoryPromotion.GetInstance();

            return promotionRepository.Get();
        }


        /// <summary>
        /// Get one promotion by guid
        [HttpGet]
        [Route("{promotionGuid}")]
        [ProducesResponseType(typeof(Promotion), 200)]
        public Promotion GetOneByGuid([FromRoute] Guid promotionGuid)
        {

            InMemoryRepositoryPromotion promotionRepository = InMemoryRepositoryPromotion.GetInstance();

            try
            {
                return promotionRepository.GetByGuid(promotionGuid);
            }
            catch (Exception)
            {
                return null;
            }
        }


        /// <summary>
        /// supress promotion by guid
        [HttpDelete]
        [Authorize(Policy = "HeadTeacher")]
        [Route("{promotionGuid}")]
        [ProducesResponseType(typeof(string), 200)]
        public IActionResult SuppressOneByGuid([FromRoute] Guid promotionGuid)
        {
            InMemoryRepositoryPromotion promotionRepository = InMemoryRepositoryPromotion.GetInstance();

            return promotionRepository.RemoveByGuid(promotionGuid) ? Ok("Resource deleted") : UnprocessableEntity("Promotion not found");
        }



        /// <summary>
        /// Modify a promotion by guid
        [HttpPut]
        [Authorize(Policy = "HeadTeacher")]
        [Route("{promotionGuid}")]
        [ProducesResponseType(typeof(Promotion), 200)]
        public IActionResult ModifyOneByGuid([FromRoute] Guid promotionGuid, [FromBody] Promotion promotion)
        {
            InMemoryRepositoryPromotion promotionRepository = InMemoryRepositoryPromotion.GetInstance();
            try
            {
                promotionRepository.PutByGuid(promotionGuid, promotion);
                return Ok("Object modified");
            }
            catch (Exception)
            {
                return NotFound($"Impossible to retrieve the object with guid {promotionGuid}");
            }
        }
    }
}
