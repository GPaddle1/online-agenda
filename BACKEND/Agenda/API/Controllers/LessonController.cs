﻿using Agenda;
using Agenda.Repository;
using API.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CNAMScheduleAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    public class LessonController : ControllerBase
    {
        private readonly ILogger<LessonController> _logger;

        public LessonController(ILogger<LessonController> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// Get the lesson for a dedicated moment
        [HttpGet]
        [Route("semesterSchedule/{semesterScheduleGuid}/{date}/{ePeriod}")]
        public IActionResult GetAtAMoment([FromRoute] Guid semesterScheduleGuid, [FromRoute] DateTime date, [FromRoute] EPeriod ePeriod)
        {
            InMemoryRepositorySemesterSchedule semesterScheduleRepository = InMemoryRepositorySemesterSchedule.GetInstance();

            Period period = new Period(date, ePeriod);
            try
            {
                SchoolMatter schoolMatter = semesterScheduleRepository.GetByGuid(semesterScheduleGuid).GetLesson(period);

                if (schoolMatter is null)
                {
                    return NotFound();
                }

                return Ok(new Lesson(period, schoolMatter));
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        /// <summary>
        /// 
        [HttpDelete]
        [Authorize(Policy = "HeadTeacher")]
        [Route("semesterSchedule/{semesterScheduleGuid}/{date}/{ePeriod}")]
        public IActionResult DeleteAtAMoment([FromRoute] Guid semesterScheduleGuid, [FromRoute] DateTime date, [FromRoute] EPeriod ePeriod)
        {
            InMemoryRepositorySemesterSchedule semesterScheduleRepository = InMemoryRepositorySemesterSchedule.GetInstance();

            Period period = new Period(date, ePeriod);

            IActionResult defaultResponse = UnprocessableEntity(ResponseHelper.AN_ERROR_OCCURED);
            try
            {
                return semesterScheduleRepository.GetByGuid(semesterScheduleGuid).RemoveLesson(period) ? Accepted(value: "Operation worked well") : defaultResponse;
            }
            catch (Exception)
            {
                return defaultResponse;
            }
        }

        /// <summary>
        /// 
        [HttpGet]
        [Route("semesterSchedule/{semesterScheduleGuid}/{schoolMatterGuid}")]
        public IEnumerable<Lesson> GetBySchoolMatter([FromRoute] Guid semesterScheduleGuid, [FromRoute] Guid schoolMatterGuid)
        {
            InMemoryRepositorySemesterSchedule semesterScheduleRepository = InMemoryRepositorySemesterSchedule.GetInstance();
            InMemoryRepositorySchoolMatter schoolMatterRepository = InMemoryRepositorySchoolMatter.GetInstance();

            try
            {
                SchoolMatter schoolMatter = schoolMatterRepository.GetByGuid(schoolMatterGuid);

                Dictionary<Period, SchoolMatter> lessons = semesterScheduleRepository.GetByGuid(semesterScheduleGuid).GetLessonsBySchoolMatter(schoolMatter);

                return SemesterScheduleController.MapLessons(lessons);
            }
            catch (Exception)
            {
                return new List<Lesson>();
            }
        }

        /// <summary>
        /// 
        [HttpGet]
        [Route("semesterSchedule/{semesterScheduleGuid}")]
        public IEnumerable<Lesson> GetBySemesterSchedule([FromRoute] Guid semesterScheduleGuid)
        {
            InMemoryRepositorySemesterSchedule semesterScheduleRepository = InMemoryRepositorySemesterSchedule.GetInstance();
            InMemoryRepositoryHeadTeacher headTeacherRepository = InMemoryRepositoryHeadTeacher.GetInstance();

            try
            {
                var lessons = semesterScheduleRepository.GetByGuid(semesterScheduleGuid).GetLessons(headTeacherRepository.Get().First());

                return SemesterScheduleController.MapLessons(lessons);
            }
            catch (Exception)
            {
                return new List<Lesson>();
            }
        }

        /// <summary>
        /// 
        [HttpPost]
        [Authorize(Policy = "HeadTeacher")]
        [Route("semesterSchedule/{semesterScheduleGuid}/{date}/{ePeriod}/schoolMatter/{schoolMatterGuid}"
            )]
        public IActionResult AddLessonToSemesterSchedule([FromRoute] Guid semesterScheduleGuid, [FromRoute] DateTime date, [FromRoute] EPeriod ePeriod, [FromRoute] Guid schoolMatterGuid)
        {

            InMemoryRepositorySemesterSchedule semesterScheduleRepository = InMemoryRepositorySemesterSchedule.GetInstance();
            InMemoryRepositorySchoolMatter schoolMatterRepository = InMemoryRepositorySchoolMatter.GetInstance();

            SchoolMatter schoolMatter = schoolMatterRepository.GetByGuid(schoolMatterGuid);

            try
            {
                bool actionSuccess = semesterScheduleRepository.GetByGuid(semesterScheduleGuid).AddLesson(schoolMatter, new Period(date, ePeriod));

                return actionSuccess ? Accepted() : UnprocessableEntity(ResponseHelper.AN_ERROR_OCCURED);
            }
            catch (Exception)
            {
                return UnprocessableEntity(ResponseHelper.AN_ERROR_OCCURED);
            }
        }

        /// <summary>
        /// 
        [HttpPut]
        [Authorize(Policy = "HeadTeacher")]
        [Route("semesterSchedule/{semesterScheduleGuid}/swap/{periodGuid1}/{periodGuid2}")]
        public IActionResult SwapTwoLessonsOnSemesterSchedule([FromRoute] Guid semesterScheduleGuid, [FromRoute] Guid periodGuid1, [FromRoute] Guid periodGuid2)
        {
            InMemoryRepositorySemesterSchedule semesterScheduleRepository = InMemoryRepositorySemesterSchedule.GetInstance();
            InMemoryRepositoryPeriod periodRepository = InMemoryRepositoryPeriod.GetInstance();

            try
            {
                Period period1 = periodRepository.GetByGuid(periodGuid1),
                        period2 = periodRepository.GetByGuid(periodGuid2);

                semesterScheduleRepository.GetByGuid(semesterScheduleGuid).SwapTwoLessons(period1, period2);
                return Ok("Lessons swapped");
            }
            catch (Exception)
            {
                return UnprocessableEntity(ResponseHelper.AN_ERROR_OCCURED);
            }
        }
    }
}
