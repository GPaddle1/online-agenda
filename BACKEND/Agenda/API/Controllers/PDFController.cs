﻿using Agenda;
using Agenda.Repository;
using API.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Mail;
using System.Security.Claims;
using System.Text;

namespace CNAMScheduleAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    public class PDFController : ControllerBase
    {
        private readonly ILogger<PDFController> _logger;
        private readonly IConfiguration _config;

        public PDFController(ILogger<PDFController> logger, IConfiguration config)
        {
            _logger = logger;
            _config = config;
        }

        /// <summary>
        /// Get one PDF by guid
        [HttpGet]
        [Route("semesterSchedule/{semesterScheduleGuid}")]
        [ProducesResponseType(typeof(string), 200)]
        public IActionResult GetOneByGuid([FromRoute] Guid semesterScheduleGuid)
        {
            InMemoryRepositorySemesterSchedule semesterScheduleRepository = InMemoryRepositorySemesterSchedule.GetInstance();
            try
            {
                SemesterSchedule semesterSchedule = semesterScheduleRepository.GetByGuid(semesterScheduleGuid);

                string HTMLSchedule = SchedulePDFGenerator.ScheduleToHTML(semesterSchedule);

                return Ok(HTMLSchedule);
            }
            catch (Exception)
            {
                return UnprocessableEntity(ResponseHelper.AN_ERROR_OCCURED);
            }
        }
    }
}
