﻿using Agenda;
using Agenda.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;

namespace CNAMScheduleAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    public class PeriodController : ControllerBase
    {
        private readonly ILogger<PeriodController> _logger;

        public PeriodController(ILogger<PeriodController> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// Get all periods
        [HttpGet]
        public IEnumerable<Period> Get()
        {
            InMemoryRepositoryPeriod periodRepository = InMemoryRepositoryPeriod.GetInstance();

            return periodRepository.Get();
        }


        /// <summary>
        /// Get one period by guid
        [HttpGet]
        [Route("{periodGuid}")]
        [ProducesResponseType(typeof(Period), 200)]
        public Period GetOneByGuid([FromRoute] Guid periodGuid)
        {

            InMemoryRepositoryPeriod periodRepository = InMemoryRepositoryPeriod.GetInstance();

            return periodRepository.GetByGuid(periodGuid);
        }
    }
}
