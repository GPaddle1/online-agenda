﻿using Agenda;
using Agenda.Repository;
using API.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CNAMScheduleAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    public class TeacherController : ControllerBase
    {
        private readonly ILogger<TeacherController> _logger;

        public TeacherController(ILogger<TeacherController> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// Get all teachers
        [HttpGet]
        public IEnumerable<Teacher> Get()
        {
            InMemoryRepositoryUser userRepository = InMemoryRepositoryUser.GetInstance();

            return userRepository.Get().Where(user => user is Teacher).ToList().Cast<Teacher>();
        }


        /// <summary>
        /// Get one teacher by teacherGuid
        [HttpGet]
        [Route("{teacherGuid}")]
        [ProducesResponseType(typeof(Teacher), 200)]
        public Teacher GetOneByGuid([FromRoute] Guid teacherGuid)
        {
            InMemoryRepositoryUser userRepository = InMemoryRepositoryUser.GetInstance();
            try
            {
                User user = userRepository.GetByGuid(teacherGuid);
                return user is Teacher ? (Teacher)user : null;
            }
            catch (Exception)
            {
                return null;
            }
        }


        /// <summary>
        /// Get one teacher by guid
        [HttpPost]
        [Authorize(Policy = "HeadTeacher")]
        [Route("{teacherGuid}/unavailability")]
        [ProducesResponseType(typeof(Teacher), 200)]
        public IActionResult AddUnavailability([FromRoute] Guid teacherGuid, [FromBody] List<Period> unavailabilities)
        {
            InMemoryRepositoryUser userRepository = InMemoryRepositoryUser.GetInstance();
            User user;
            try
            {
                user = userRepository.GetByGuid(teacherGuid);

            }
            catch (Exception)
            {
                return UnprocessableEntity(ResponseHelper.AN_ERROR_OCCURED);
            }
            if (user is Teacher)
            {
                Teacher teacher = (Teacher)user;

                try
                {
                    teacher.AddUnavailabilities(unavailabilities);
                }
                catch (AgendaException)
                {
                    return UnprocessableEntity("A problem occured during the modification");
                }

                return Accepted(value: "Periods successfully added");
            }
            return UnprocessableEntity("A problem occured during the modification");
        }


        /// <summary>
        /// Get one teacher by guid
        [HttpDelete]
        [Authorize(Policy = "HeadTeacher")]
        [Route("{teacherGuid}/unavailability/{periodGuid}")]
        [ProducesResponseType(typeof(Teacher), 200)]
        public IActionResult AddUnavailability([FromRoute] Guid teacherGuid, [FromRoute] Guid periodGuid)
        {
            InMemoryRepositoryUser userRepository = InMemoryRepositoryUser.GetInstance();
            User user;

            try
            {
                user = userRepository.GetByGuid(teacherGuid);
            }
            catch (Exception)
            {
                return UnprocessableEntity(ResponseHelper.AN_ERROR_OCCURED);
            }

            if (user is Teacher)
            {
                Teacher teacher = (Teacher)user;

                teacher.RemoveUnavailability(periodGuid);

                return Accepted("Periods successfully removed");
            }
            return UnprocessableEntity("A problem occured during the modification");
        }
    }
}
