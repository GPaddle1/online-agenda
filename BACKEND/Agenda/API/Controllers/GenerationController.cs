﻿using Agenda;
using Agenda.Repository;
using API.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CNAMScheduleAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize(Policy = "HeadTeacher")]
    public class GenerationController : ControllerBase
    {
        private static readonly string[] Routes = new[]
        {
            "/Generation/semesterSchedule/{semesterScheduleGuid}/block",
            "/Generation/semesterSchedule/{semesterScheduleGuid}/periodic/byMoment",
            "/Generation/semesterSchedule/{semesterScheduleGuid}/periodic/full"
        };

        private static readonly string[] Descriptions = new[]
        {
            "Generate by block",
            "Periodic filling for all the registered matters",
            "Periodic filling for a dedicated school matter at a special moment (Monday afternoon, Tuesday morning, ...)"
        };

        private readonly ILogger<GenerationController> _logger;

        public GenerationController(ILogger<GenerationController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        [Route("infos")]
        public IEnumerable<object> Get()
        {

            var rng = new Random();
            return Enumerable.Range(0, Routes.Count()).Select(index =>
               new
               {
                   Route = Routes[index],
                   Description = Descriptions[index]
               })
            .ToArray();
        }


        [HttpPost]
        [Route("semesterSchedule/{semesterScheduleGuid}/block")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public IActionResult GenerateByBlock([FromRoute] Guid semesterScheduleGuid)
        {

            SemesterSchedule semesterSchedule;
            try
            {
                semesterSchedule = GetSemesterScheduleByGuid(semesterScheduleGuid);

            }
            catch (Exception)
            {
                return UnprocessableEntity("Semester schedule Guid has not been found");
            }

            try
            {
                bool generationOk = semesterSchedule.RegisterSchoolMatterByBlock();
                return generationOk ? Accepted() : UnprocessableEntity(ResponseHelper.AN_ERROR_OCCURED);
            }
            catch (AgendaException error)
            {
                return UnprocessableEntity(error.Message);
            }
        }

        [HttpPost]
        [Route("semesterSchedule/{semesterScheduleGuid}/periodic/byMoment")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public IActionResult GenerateByPeriodicMoment([FromBody] PeriodicFillByDateBody data, [FromRoute] Guid semesterScheduleGuid)
        {

            SemesterSchedule semesterSchedule;

            try
            {
                semesterSchedule = GetSemesterScheduleByGuid(semesterScheduleGuid);
            }
            catch (Exception)
            {
                return UnprocessableEntity("Semester schedule Guid has not been found");
            }

            SchoolMatter schoolMatter;

            try
            {
                schoolMatter = InMemoryRepositorySchoolMatter.GetInstance().GetByGuid(data.SchoolMatterGuid);
            }
            catch (Exception)
            {
                return UnprocessableEntity("Semester schedule Guid has not been found");
            }

            try
            {
                bool generationOk = semesterSchedule.PeriodicFillByDate(data.TargetDay, data.TargetPeriod, schoolMatter, data.FinishWithBlockGeneration);
                return generationOk ? Accepted() : UnprocessableEntity(ResponseHelper.AN_ERROR_OCCURED);
            }
            catch (AgendaException error)
            {
                return UnprocessableEntity(error.Message);
            }
        }

        [HttpPost]
        [Route("semesterSchedule/{semesterScheduleGuid}/periodic/full")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]

        public IActionResult GeneratePeriodic([FromRoute] Guid semesterScheduleGuid)
        {
            SemesterSchedule semesterSchedule;

            try
            {
                semesterSchedule = GetSemesterScheduleByGuid(semesterScheduleGuid);
            }
            catch (Exception)
            {
                return UnprocessableEntity("Semester schedule Guid has not been found");
            }

            try
            {
                semesterSchedule.PeriodicScheduleGeneration();
            }
            catch (AgendaException error)
            {
                return UnprocessableEntity(error.Message);
            }
            return Accepted();

        }

        private SemesterSchedule GetSemesterScheduleByGuid(Guid semesterScheduleGuid)
        {
            InMemoryRepositorySemesterSchedule semesterScheduleRepository = InMemoryRepositorySemesterSchedule.GetInstance();

            try
            {
                return semesterScheduleRepository.GetByGuid(semesterScheduleGuid);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
