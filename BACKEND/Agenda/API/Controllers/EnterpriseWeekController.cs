﻿using Agenda;
using Agenda.Repository;
using API.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CNAMScheduleAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    public class EnterpriseWeeksController : ControllerBase
    {
        private readonly ILogger<EnterpriseWeeksController> _logger;

        public EnterpriseWeeksController(ILogger<EnterpriseWeeksController> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// Get all enterpriseWeeks
        [HttpGet]
        public IEnumerable<EnterpriseWeek> Get()
        {
            InMemoryRepositoryEnterpriseWeeks enterpriseWeekRepository = InMemoryRepositoryEnterpriseWeeks.GetInstance();

            return enterpriseWeekRepository.Get();
        }

        /// <summary>
        /// Create a new schedule for a given promotion
        [HttpPost]
        [Authorize(Policy = "HeadTeacher")]
        [ProducesResponseType(typeof(EnterpriseWeek), 200)]
        public IActionResult CreateNewEnterpriseWeeks([FromBody] EnterpriseWeekBody enterpriseWeek)
        {
            InMemoryRepositoryEnterpriseWeeks enterpriseWeekRepository = InMemoryRepositoryEnterpriseWeeks.GetInstance();
            InMemoryRepositoryPromotion inMemoryRepositoryPromotion = InMemoryRepositoryPromotion.GetInstance();

            Promotion promotion;
            try
            {
                promotion = inMemoryRepositoryPromotion.GetByGuid(enterpriseWeek.promotionGuid);
            }
            catch (Exception)
            {
                return UnprocessableEntity(ResponseHelper.AN_ERROR_OCCURED);
            }
            bool postSuccess = enterpriseWeekRepository.Post(new EnterpriseWeek(enterpriseWeek.weekNumber, promotion));

            return postSuccess ? Accepted(value: "Operation worked well") : UnprocessableEntity(ResponseHelper.AN_ERROR_OCCURED);
        }

        /// <summary>
        /// Create a new schedule for a given promotion
        [HttpDelete]
        [Authorize(Policy = "HeadTeacher")]
        [Route("{enterpriseWeekGuid}")]
        [ProducesResponseType(typeof(EnterpriseWeek), 200)]
        public IActionResult RemoveEnterpriseWeeks([FromRoute] Guid enterpriseWeekGuid)
        {
            InMemoryRepositoryEnterpriseWeeks enterpriseWeekRepository = InMemoryRepositoryEnterpriseWeeks.GetInstance();
            bool postSuccess = enterpriseWeekRepository.RemoveByGuid(enterpriseWeekGuid);

            return postSuccess ? Accepted(value: "Operation worked well") : UnprocessableEntity(ResponseHelper.AN_ERROR_OCCURED);
        }

    }
}
