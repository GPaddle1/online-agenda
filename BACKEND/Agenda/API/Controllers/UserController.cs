﻿using Agenda;
using Agenda.Repository;
using API.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Mail;
using System.Security.Claims;
using System.Text;

namespace CNAMScheduleAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    public class UserController : ControllerBase
    {
        private readonly ILogger<UserController> _logger;
        private readonly IConfiguration _config;

        public UserController(ILogger<UserController> logger, IConfiguration config)
        {
            _logger = logger;
            _config = config;
        }

        /// <summary>
        /// Get all users
        [HttpGet]
        public IEnumerable<User> Get()
        {
            InMemoryRepositoryUser userRepository = InMemoryRepositoryUser.GetInstance();

            return userRepository.Get();
        }

        /// <summary>
        /// Login
        [HttpPost]
        [AllowAnonymous]
        [Route("login")]
        public IActionResult Login([FromBody] Credentials credentials)
        {
            InMemoryRepositoryUser userRepository = InMemoryRepositoryUser.GetInstance();

            User user = userRepository.ContainsUser(credentials);

            if (user != null)
            {
                var tokenString = GenerateJSONWebToken(user);
                HttpContext.Response.Headers.Add("Authorization", $"Bearer {tokenString}");

                return Ok(new
                {
                    token = tokenString,
                    userGuid = user.UserGuid
                });
            }

            return Unauthorized("Unknown credentials");
        }


        /// <summary>
        /// Create new user
        [HttpPost]
        [Authorize(Policy = "HeadTeacher")]
        public IActionResult Post([FromBody] UserBody user)
        {
            InMemoryRepositoryUser userRepository = InMemoryRepositoryUser.GetInstance();
            //TODO check values

            User obj = new User(user.firstName, user.lastName, new MailAddress(user.email), user.password);
            try
            {
                userRepository.Post(obj);
                return Accepted(value: $"Object created with guid = {obj.UserGuid}");
            }
            catch (RepositoryException e)
            {
                return UnprocessableEntity(e.Message);
            }
        }

        /// <summary>
        /// Create new teacher
        [HttpPost]
        [Authorize(Policy = "HeadTeacher")]
        [Route("teacher")]
        public IActionResult PostTeacher([FromBody] TeacherBody user)
        {
            InMemoryRepositoryUser userRepository = InMemoryRepositoryUser.GetInstance();

            Teacher obj = new Teacher(user.firstName, user.lastName, new MailAddress(user.email), user.password);
            try
            {
                userRepository.Post(obj);
                return Accepted(value: $"Object created with guid = {obj.UserGuid}");
            }
            catch (RepositoryException e)
            {
                return UnprocessableEntity(e.Message);
            }
        }

        /// <summary>
        /// Create new teacher
        [HttpPost]
        [Authorize(Policy = "HeadTeacher")]
        [Route("student")]
        public IActionResult PostStudent([FromBody] StudentBody user)
        {
            InMemoryRepositoryUser userRepository = InMemoryRepositoryUser.GetInstance();
            InMemoryRepositoryPromotion promotionRepository = InMemoryRepositoryPromotion.GetInstance();

            Promotion promotion;
            try
            {
                promotion = promotionRepository.GetByGuid(user.PromotionGuid);
            }
            catch (Exception)
            {
                return UnprocessableEntity("Impossible to get promotion by GUID");
            }

            Student obj = new Student(user.firstName, user.lastName, new MailAddress(user.email), user.password, promotion);
            try
            {
                userRepository.Post(obj);
                return Accepted(value:$"Object created with guid = {obj.UserGuid}");
            }
            catch (RepositoryException e)
            {
                return UnprocessableEntity(e.Message);
            }
        }

        /// <summary>
        /// Create new users
        [HttpPost]
        [Authorize(Policy = "HeadTeacher")]
        [Route("multiple")]
        public IActionResult PostMultipleUsers([FromBody] IEnumerable<UserBody> users)
        {
            InMemoryRepositoryUser userRepository = InMemoryRepositoryUser.GetInstance();

            List<UserBody> failedObjects = new List<UserBody>();
            foreach (UserBody user in users)
            {
                try
                {
                    User createdUser = new User(user.firstName, user.lastName, new MailAddress(user.email), user.password);
                    if (!userRepository.Post(createdUser))
                    {
                        failedObjects.Append(user);
                    }
                }
                catch (Exception)
                {
                    failedObjects.Add(user);
                }
            }

            if (failedObjects.Count() > 0)
            {
                return UnprocessableEntity($"Objects failed : {string.Join(", ", failedObjects.Select(user => user.ToString()).ToArray())}");
            }
            return Accepted(value: "Objects created");
        }


        /// <summary>
        /// Get one user by guid
        [HttpGet]
        [Route("{userGuid}")]
        [ProducesResponseType(typeof(User), 200)]
        public User GetOneByGuid([FromRoute] Guid userGuid)
        {
            InMemoryRepositoryUser userRepository = InMemoryRepositoryUser.GetInstance();

            try
            {
                return userRepository.GetByGuid(userGuid);
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Get one user by guid
        [HttpPut]
        [Authorize(Policy = "HeadTeacher")]
        [Route("{userGuid}/promote")]
        [ProducesResponseType(typeof(User), 200)]
        public IActionResult PromoteUser([FromRoute] Guid userGuid)
        {
            InMemoryRepositoryUser userRepository = InMemoryRepositoryUser.GetInstance();

            try
            {
                userRepository.GetByGuid(userGuid).Role = ERole.Teacher;

                return Ok("Action registered");
            }
            catch (Exception)
            {
                return UnprocessableEntity(ResponseHelper.AN_ERROR_OCCURED);
            }
        }


        /// <summary>
        /// supress user by userGuid
        [HttpDelete]
        [Route("{userGuid}")]
        [ProducesResponseType(typeof(string), 200)]
        public IActionResult SuppressOneByGuid([FromRoute] Guid userGuid)
        {
            InMemoryRepositoryUser userRepository = InMemoryRepositoryUser.GetInstance();

            return userRepository.RemoveByGuid(userGuid) ? Accepted("Resource deleted") : UnprocessableEntity("User not found");
        }

        /// <summary>
        /// Get one user by userGuid
        [HttpPut]
        [Route("{userGuid}")]
        [ProducesResponseType(typeof(string), 200)]
        public IActionResult ModifyOneByGuid([FromRoute] Guid userGuid, [FromBody] TemporaryUser user)
        {
            InMemoryRepositoryUser userRepository = InMemoryRepositoryUser.GetInstance();
            try
            {
                userRepository.PutByGuid(userGuid, user);
                return Ok("Object modified");
            }
            catch (Exception)
            {
                return NotFound($"Impossible to retrieve the object with guid {userGuid}");
            }
        }

        private string GenerateJSONWebToken(User userInfo)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            SigningCredentials credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new[] {
                new Claim(JwtRegisteredClaimNames.Sub, userInfo.Email.Address),
                new Claim(JwtRegisteredClaimNames.Email, userInfo.Email.Address),
                new Claim(AuthenticationHelper.GUID, userInfo.UserGuid.ToString()),
                new Claim(ClaimTypes.Role, userInfo.Role.ToString()),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
             };

            var token = new JwtSecurityToken(_config["Jwt:Issuer"],
              _config["Jwt:Issuer"],
              claims,
              expires: DateTime.Now.AddMinutes(120),
              signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
