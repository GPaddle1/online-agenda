﻿using Agenda;
using API.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;

namespace CNAMScheduleAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    public class HolidaysController : ControllerBase
    {
        private readonly ILogger<HolidaysController> _logger;

        public HolidaysController(ILogger<HolidaysController> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// Get all holidays
        [HttpGet]
        public IEnumerable<DateTime> Get()
        {
            InMemoryRepositoryHolidays holidayRepository = InMemoryRepositoryHolidays.GetInstance();

            return holidayRepository.Get();
        }

        /// <summary>
        /// Add a new holiday
        [HttpPost]
        [Route("{dateTime}")]
        [Authorize(Policy = "HeadTeacher")]
        [ProducesResponseType(typeof(DateTime), 200)]
        public IActionResult CreateNewHolidays([FromRoute] DateTime dateTime)
        {
            InMemoryRepositoryHolidays holidayRepository = InMemoryRepositoryHolidays.GetInstance();
            bool postSuccess = holidayRepository.Post(dateTime);

            return postSuccess ? Accepted(value: "Operation worked well") : UnprocessableEntity(ResponseHelper.AN_ERROR_OCCURED);
        }

        /// <summary>
        /// Remove a holiday
        [HttpDelete]
        [Authorize(Policy = "HeadTeacher")]
        [Route("{dateTime}")]
        [ProducesResponseType(typeof(EnterpriseWeek), 200)]
        public IActionResult RemoveHoliday([FromRoute] DateTime dateTime)
        {
            InMemoryRepositoryHolidays enterpriseHolidays = InMemoryRepositoryHolidays.GetInstance();
            bool postSuccess = enterpriseHolidays.Remove(dateTime);

            return postSuccess ? Accepted(value: "Operation worked well") : UnprocessableEntity(ResponseHelper.AN_ERROR_OCCURED);
        }

    }
}
