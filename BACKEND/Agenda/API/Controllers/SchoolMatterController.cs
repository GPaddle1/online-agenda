﻿using Agenda;
using Agenda.Repository;
using API.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;

namespace CNAMScheduleAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    public class SchoolMatterController : ControllerBase
    {
        private readonly ILogger<SchoolMatterController> _logger;

        public SchoolMatterController(ILogger<SchoolMatterController> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// Get all schoolMatters
        [HttpGet]
        public IEnumerable<SchoolMatter> Get()
        {
            InMemoryRepositorySchoolMatter schoolMatterRepository = InMemoryRepositorySchoolMatter.GetInstance();

            return schoolMatterRepository.Get();
        }


        /// <summary>
        /// Get one schoolMatter by guid
        [HttpGet]
        [Route("{schoolMatterGuid}")]
        [ProducesResponseType(typeof(SchoolMatter), 200)]
        public SchoolMatter GetOneByGuid([FromRoute] Guid schoolMatterGuid)
        {
            InMemoryRepositorySchoolMatter schoolMatterRepository = InMemoryRepositorySchoolMatter.GetInstance();
            try
            {
                return schoolMatterRepository.GetByGuid(schoolMatterGuid);
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Get all the schoolMatter for a semester schedule
        [HttpGet]
        [Route("semesterSchedule/{semesterScheduleGuid}")]
        [ProducesResponseType(typeof(SchoolMatter), 200)]
        public IEnumerable<SchoolMatter> GetBySemesterSchedule([FromRoute] Guid semesterScheduleGuid)
        {
            InMemoryRepositorySemesterSchedule semesterScheduleMatterRepository = InMemoryRepositorySemesterSchedule.GetInstance();

            try
            {
                return semesterScheduleMatterRepository.GetByGuid(semesterScheduleGuid).GetRegisteredSchoolMatters();
            }
            catch (Exception)
            {

                return new List<SchoolMatter>();
            }
        }

        /// <summary>
        /// Add one schoolMatter
        [HttpPost]
        [Authorize(Policy = "HeadTeacher")]
        [ProducesResponseType(typeof(bool), 200)]
        public IActionResult PostSchoolMatter([FromBody] SchoolMatterBody schoolMatterTemplate)
        {

            InMemoryRepositorySchoolMatter schoolMatterRepository = InMemoryRepositorySchoolMatter.GetInstance();

            InMemoryRepositoryTeacher repositoryTeacher = InMemoryRepositoryTeacher.GetInstance();

            try
            {
                Teacher teacher = repositoryTeacher.GetByGuid(schoolMatterTemplate.TeacherGuid);

                SchoolMatter schoolMatter = new SchoolMatter(schoolMatterTemplate.Name, teacher);

                bool postOk = schoolMatterRepository.Post(schoolMatter);
                Guid schoolMatterGuid = schoolMatter.SchoolMatterGuid;
                return postOk ? Accepted(value: $"Object created with guid {schoolMatterGuid}") : UnprocessableEntity("Impossible to add the object");
            }
            catch (Exception e)
            {
                return UnprocessableEntity(e.Message);
            }
        }

        /// <summary>
        /// Get one schoolMatter by guid
        [HttpPut]
        [Authorize(Policy = "HeadTeacher")]
        [Route("{schoolMatterGuid}/duration/{semesterScheduleGuid}")]
        [ProducesResponseType(typeof(bool), 200)]
        public IActionResult PostSchoolMatterDuration([FromRoute] string schoolMatterGuid, [FromRoute] string semesterScheduleGuid, [FromBody] int duration)
        {

            InMemoryRepositorySchoolMatter schoolMatterRepository = InMemoryRepositorySchoolMatter.GetInstance();
            InMemoryRepositorySemesterSchedule semesterScheduleRepository = InMemoryRepositorySemesterSchedule.GetInstance();

            SchoolMatter schoolMatter;

            try
            {
                schoolMatter = schoolMatterRepository.GetByGuid(new Guid(schoolMatterGuid));
            }
            catch (Exception)
            {
                return UnprocessableEntity("School matter Guid has not been found");
            }

            SemesterSchedule semesterSchedule;
            try
            {
                semesterSchedule = semesterScheduleRepository.GetByGuid(new Guid(semesterScheduleGuid));
            }
            catch (Exception)
            {
                return UnprocessableEntity("Semester schedule Guid has not been found");
            }

            try
            {
                semesterSchedule.AddLessonDuration(schoolMatter, duration);

                return Ok("Association has been done");
            }
            catch (Exception e)
            {
                return UnprocessableEntity(e.Message);
            }
        }

        /// <summary>
        /// supress schoolMatter by guid
        [HttpDelete]
        [Authorize(Policy = "HeadTeacher")]
        [Route("{schoolMatterGuid}")]
        [ProducesResponseType(typeof(string), 200)]
        public IActionResult SuppressOneByGuid([FromRoute] Guid schoolMatterGuid)
        {
            InMemoryRepositorySchoolMatter schoolMatterRepository = InMemoryRepositorySchoolMatter.GetInstance();

            return schoolMatterRepository.RemoveByGuid(schoolMatterGuid) ? Ok("Resource deleted") : UnprocessableEntity("SchoolMatter not found");
        }

        /// <summary>
        /// Modify a schoolMatter by guid
        [HttpPut]
        [Authorize(Policy = "HeadTeacher")]
        [Route("{schoolMatterGuid}")]
        [ProducesResponseType(typeof(SchoolMatter), 200)]
        public IActionResult ModifyOneByGuid([FromRoute] Guid schoolMatterGuid, [FromBody] SchoolMatter schoolMatter)
        {
            InMemoryRepositorySchoolMatter schoolMatterRepository = InMemoryRepositorySchoolMatter.GetInstance();
            try
            {
                schoolMatterRepository.PutByGuid(schoolMatterGuid, schoolMatter);
                return Ok("Object modified");
            }
            catch (Exception)
            {
                return NotFound($"Impossible to retrieve the object with guid {schoolMatterGuid}");
            }
        }
    }
}
