﻿using Agenda;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using System.Threading.Tasks;

namespace API.Requirements
{
    public class HeadTeacherRequirement : AuthorizationHandler<HeadTeacherRequirement>, IAuthorizationRequirement
    {

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, HeadTeacherRequirement requirement)
        {

            if (!context.User.HasClaim(c => c.Type == ClaimTypes.Role))
            {
                context.Fail();
            }

            var role = context.User.FindFirst(c => c.Type == ClaimTypes.Role);

            if (role is null)
            {
                context.Fail();
            }

            if (!context.HasFailed && role.Value == ERole.HeadTeacher.ToString())
            {
                context.Succeed(requirement);
            }
            else
            {
                context.Fail();
            }

            return Task.CompletedTask;
        }
    }
}
