﻿using System;

namespace Agenda
{
    public class Month : IScheduleView
    {
        private int TargetMonth { get; set; }
        private int TargetYear { get; set; }

        public bool IsDateShown(DateTime dateTime)
        {
            int month = dateTime.Month;
            int year = dateTime.Year;

            return month == this.TargetMonth && year == this.TargetYear;
        }

        public Month(int targetMonth, int targetYear)
        {
            this.TargetMonth = targetMonth;
            this.TargetYear = targetYear;
        }
    }
}
