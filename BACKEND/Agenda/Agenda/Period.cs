using System;

namespace Agenda
{
    public class Period
    {
        public DateTime Date { get; set; }
        public EPeriod EPeriod { get; set; }
        public Guid PeriodGuid { get; internal set; }

        public Period(DateTime date, EPeriod ePeriod)
        {
            Date = date;
            EPeriod = ePeriod;
            PeriodGuid = Guid.NewGuid();
        }

        public override bool Equals(object obj)
        {
            return obj is Period period &&
                   EPeriod == period.EPeriod &&
                   Date.Month == period.Date.Month &&
                   Date.Day == period.Date.Day &&
                   Date.Year == period.Date.Year;
        }

        public override int GetHashCode()
        {
            return EPeriod.GetHashCode() ^ Date.Year ^ Date.Month ^ Date.Day;
        }

        public override string ToString()
        {
            return Date.ToString() + " " + EPeriod;
        }
    }
}
