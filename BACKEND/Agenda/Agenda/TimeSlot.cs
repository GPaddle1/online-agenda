﻿using System;
using System.Collections.Generic;

namespace Agenda
{
    public class TimeSlot
    {
        public DayOfWeek Day { get; set; }
        public EPeriod EPeriodValue { get; set; }

        public TimeSlot(DayOfWeek day, EPeriod ePeriodValue)
        {
            this.Day = day;
            this.EPeriodValue = ePeriodValue;
        }



        public static List<TimeSlot> GetAvailableTimeSlots()
        {
            List<DayOfWeek> daysToFill = new List<DayOfWeek>()
                {
                    DayOfWeek.Monday,
                    DayOfWeek.Tuesday,
                    DayOfWeek.Wednesday,
                    DayOfWeek.Thursday,
                    DayOfWeek.Friday
                };

            List<TimeSlot> availableTimeSlots = new List<TimeSlot>();


            foreach (DayOfWeek day in daysToFill)
            {
                foreach (EPeriod ePeriodValue in Enum.GetValues(typeof(EPeriod)))
                {
                    availableTimeSlots.Add(new TimeSlot(day, ePeriodValue));
                }
            }

            return availableTimeSlots;
        }
    }
}