using System.Collections.Generic;
using System.Net.Mail;

namespace Agenda
{
    public class HeadTeacher : Teacher, IHaveASchedule
    {
        public HeadTeacher(string firstName, string lastName, MailAddress email, string password) : base(firstName, lastName, email, password)
        {
            Role = ERole.HeadTeacher;
        }

        public HeadTeacher(string firstName, string lastName, MailAddress email, string password, List<Period> unavailability) : base(firstName, lastName, email, password, unavailability)
        {
            Role = ERole.HeadTeacher;
        }

        public new bool CanSeeLesson(SchoolMatter schoolMatter)
        {
            return true;
        }

        public new bool CanConsultSemesterSchedule(SemesterSchedule semesterSchedule)
        {
            return true;
        }

    }
}
