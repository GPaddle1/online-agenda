﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Agenda.Repository
{
    public class InMemoryRepositoryPromotion : IRepositoryPromotion
    {

        IEnumerable<Promotion> Promotions;

        private static InMemoryRepositoryPromotion _instance;

        private InMemoryRepositoryPromotion()
        {
            Promotions = new List<Promotion>
            {
                new Promotion("X"),
                new Promotion("Y"),
                new Promotion("Z"),
            };
        }

        public static InMemoryRepositoryPromotion GetInstance()
        {

            if (_instance == null)
            {
                _instance = new InMemoryRepositoryPromotion();
            }
            return _instance;
        }



        public IEnumerable<Promotion> Get()
        {
            return Promotions;
        }

        public Promotion GetByGuid(Guid guid)
        {
            return Promotions.Single(Promotion => Promotion.PromotionGuid == guid);
        }

        public bool Post(Promotion obj)
        {
            Promotions = Promotions.Append(obj);
            return true;
        }

        public bool RemoveByGuid(Guid guid)
        {
            try
            {
                Promotion targetPromotion = GetByGuid(guid);
                int oldSize = Promotions.Count();
                Promotions = Promotions.Where(user => user != targetPromotion);

                return oldSize != Promotions.Count();
            }
            catch (Exception)
            {
                return false;
            }
        }

        public Promotion PutByGuid(Guid guid, Promotion temporaryObject)
        {
            Promotion promotion = GetByGuid(guid);
            promotion.Title = temporaryObject.Title;

            return promotion;
        }
    }
}
