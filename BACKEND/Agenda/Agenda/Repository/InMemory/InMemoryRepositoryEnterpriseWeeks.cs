using Agenda.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Agenda
{
    public class InMemoryRepositoryEnterpriseWeeks : IRepositoryEnterpriseWeeks
    {

        IEnumerable<EnterpriseWeek> EnterpriseWeeks;

        private static InMemoryRepositoryEnterpriseWeeks _instance;

        private InMemoryRepositoryEnterpriseWeeks()
        {
            Promotion promotion = InMemoryRepositoryPromotion.GetInstance().Get().First();

            EnterpriseWeeks = new List<EnterpriseWeek>
            {
                new EnterpriseWeek(2, promotion),
                new EnterpriseWeek(4, promotion),
                new EnterpriseWeek(6, promotion),
                new EnterpriseWeek(8, promotion),
                new EnterpriseWeek(10, promotion),
                new EnterpriseWeek(12, promotion),
                new EnterpriseWeek(14, promotion),
                new EnterpriseWeek(16, promotion),
                new EnterpriseWeek(18, promotion),
                new EnterpriseWeek(19, promotion),
                new EnterpriseWeek(22, promotion),
                new EnterpriseWeek(24, promotion),
                new EnterpriseWeek(26, promotion)
            };
        }

        public static InMemoryRepositoryEnterpriseWeeks GetInstance()
        {

            if (_instance == null)
            {
                _instance = new InMemoryRepositoryEnterpriseWeeks();
            }
            return _instance;
        }

        public EnterpriseWeek GetByGuid(Guid guid)
        {
            return EnterpriseWeeks.FirstOrDefault(enterpriseWeek => enterpriseWeek.EnterpriseWeekGuid == guid);
        }

        public bool Post(EnterpriseWeek obj)
        {
            EnterpriseWeeks = EnterpriseWeeks.Append(obj);
            return true;
        }

        public bool RemoveByGuid(Guid guid)
        {
            EnterpriseWeeks= EnterpriseWeeks.Where(enterpriseWeek => enterpriseWeek.EnterpriseWeekGuid != guid);
                return true;
        }

        public IEnumerable<EnterpriseWeek> Get()
        {
            return EnterpriseWeeks;
        }
    }
}
