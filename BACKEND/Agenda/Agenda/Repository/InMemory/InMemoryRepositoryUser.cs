﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;

namespace Agenda.Repository
{
    public class InMemoryRepositoryUser : IRepositoryUser
    {

        IEnumerable<User> Users;

        private static InMemoryRepositoryUser _instance;

        private InMemoryRepositoryUser()
        {
            IEnumerable<Promotion> Promotions = InMemoryRepositoryPromotion.GetInstance().Get();

            Users = new List<User>
            {
                new User("X", "Y", new MailAddress("xx1@yy.com"), "ABCD"),
                new User("Y", "Z", new MailAddress("yy1@zz.com"), "ABCD"),
                new User("Z", "X", new MailAddress("zz1@xx.com"), "ABCD"),


                new HeadTeacher("BossX","BossY", new MailAddress ("BossX@BossY.com"),"XXX"),


                 new Teacher("X", "Y", new MailAddress ("X@Y.com"), "XXX", new List<Period>{
                    new Period(new DateTime(2022,1,1),EPeriod.AM),
                    new Period(new DateTime(2022,1,1),EPeriod.PM)
                }),

                new Teacher("Y", "Z", new MailAddress ("Y@Z.com"), "XXX", new List<Period>{
                    new Period(new DateTime(2022,1,2),EPeriod.AM),
                    new Period(new DateTime(2022,1,2),EPeriod.PM)
                }),

                new Teacher("Z", "X", new MailAddress("Z@X.com"), "XXX"),

                new Student("X", "Y", new MailAddress("xx@yy.com"), "ABCD", Promotions.ElementAt(0)),
                new Student("Y", "Z", new MailAddress("yy@zz.com"), "ABCD", Promotions.ElementAt(1)),
                new Student("Z", "X", new MailAddress("zz@xx.com"), "ABCD", Promotions.ElementAt(2)),
            };
        }

        public static InMemoryRepositoryUser GetInstance()
        {
            if (_instance == null)
            {
                _instance = new InMemoryRepositoryUser();
            }
            return _instance;
        }

        public IEnumerable<User> Get()
        {
            return Users;
        }

        public User GetByGuid(Guid guid)
        {
            //TODO check if exists
            return Users.Single(user => user.UserGuid == guid);
        }

        public bool Post(User obj)
        {
            if (Users.Where(user => user.Email == obj.Email).Count() > 0)
            {
                throw new RepositoryException("Email already exists in our data");
            }
            Users = Users.Append(obj);
            return true;
        }

        public User ContainsUser(Credentials credentials)
        {
            try
            {
                return Users.Single(user => user.Email.Address == credentials.Email && user.VerifyPassword(credentials.Password));
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool RemoveByGuid(Guid guid)
        {
            try
            {
                User targetUser = GetByGuid(guid);
                int oldSize = Users.Count();
                Users = Users.Where(user => user != targetUser);

                return oldSize != Users.Count();
            }
            catch (Exception)
            {
                return false;
            }
        }

        public User PutByGuid(Guid guid, TemporaryUser temporaryObject)
        {
            User user = GetByGuid(guid);

            user.FirstName = temporaryObject.FirstName;
            user.LastName = temporaryObject.LastName;
            user.Email = new MailAddress(temporaryObject.Email);
            user.SetNewPassword(temporaryObject.password);

            return user;
        }
    }
}
