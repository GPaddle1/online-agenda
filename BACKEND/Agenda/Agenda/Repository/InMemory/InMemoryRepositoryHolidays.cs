using Agenda.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Agenda
{
    public class InMemoryRepositoryHolidays : IRepositoryHolidays
    {

        IEnumerable<DateTime> Holidays;

        private static InMemoryRepositoryHolidays _instance;

        private InMemoryRepositoryHolidays()
        {
            Holidays = new List<DateTime>
            {
                new DateTime(2022, 01, 01),
                new DateTime(2022, 04, 15),
                new DateTime(2022, 04, 18),
                new DateTime(2022, 05, 01),
                new DateTime(2022, 05, 08),
                new DateTime(2022, 05, 26),
                new DateTime(2022, 06, 06)
            };
        }

        public static InMemoryRepositoryHolidays GetInstance()
        {

            if (_instance == null)
            {
                _instance = new InMemoryRepositoryHolidays();
            }
            return _instance;
        }

        public IEnumerable<DateTime> Get()
        {
            return Holidays;
        }

        public DateTime GetByGuid(Guid guid)
        {
            throw new NotImplementedException();
        }

        public bool Post(DateTime obj)
        {
            if (Holidays.Contains(obj))
            {
                return false;
            }

            Holidays = Holidays.Append(obj);
            return true;
        }

        public bool RemoveByGuid(Guid guid)
        {
            throw new NotImplementedException();
        }

        public bool Remove(DateTime dateTime)
        {
            Holidays = Holidays.Where(date => date != dateTime);
            return true;
        }
    }
}
