﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;

namespace Agenda.Repository
{
    public class InMemoryRepositoryStudent : IRepositoryStudent
    {

        private static InMemoryRepositoryStudent _instance;

        private InMemoryRepositoryStudent()
        { }

        public static InMemoryRepositoryStudent GetInstance()
        {
            if (_instance == null)
            {
                _instance = new InMemoryRepositoryStudent();
            }
            return _instance;
        }

        public IEnumerable<Student> Get()
        {
            return InMemoryRepositoryUser.GetInstance().Get().Where(user=>user is Student).Cast<Student>();
        }

        public Student GetByGuid(Guid guid)
        {
            return (Student)InMemoryRepositoryUser.GetInstance().GetByGuid(guid);
        }

        public bool Post(Student obj)
        {
            return InMemoryRepositoryUser.GetInstance().Post(obj);
        }

        public bool RemoveByGuid(Guid guid)
        {
            return InMemoryRepositoryUser.GetInstance().RemoveByGuid(guid);
        }

        public Student PutByGuid(Guid guid, TemporaryUser temporaryObject)
        {
            return (Student)InMemoryRepositoryUser.GetInstance().PutByGuid(guid, temporaryObject);
        }
    }
}
