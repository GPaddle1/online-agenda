﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;

namespace Agenda.Repository
{
    public class InMemoryRepositoryHeadTeacher : IRepositoryHeadTeacher
    {

        private static InMemoryRepositoryHeadTeacher _instance;

        private InMemoryRepositoryHeadTeacher()
        { }

        public static InMemoryRepositoryHeadTeacher GetInstance()
        {
            if (_instance == null)
            {
                _instance = new InMemoryRepositoryHeadTeacher();
            }
            return _instance;
        }

        public IEnumerable<HeadTeacher> Get()
        {
            return InMemoryRepositoryUser.GetInstance().Get().Where(user=>user is HeadTeacher).Cast<HeadTeacher>();
        }

        public HeadTeacher GetByGuid(Guid guid)
        {
            return (HeadTeacher)InMemoryRepositoryUser.GetInstance().GetByGuid(guid);
        }

        public bool Post(HeadTeacher obj)
        {
            return InMemoryRepositoryUser.GetInstance().Post(obj);
        }

        public bool RemoveByGuid(Guid guid)
        {
            return InMemoryRepositoryUser.GetInstance().RemoveByGuid(guid);
        }

        public HeadTeacher PutByGuid(Guid guid, TemporaryUser temporaryObject)
        {
            return (HeadTeacher)InMemoryRepositoryUser.GetInstance().PutByGuid(guid, temporaryObject);
        }
    }
}
