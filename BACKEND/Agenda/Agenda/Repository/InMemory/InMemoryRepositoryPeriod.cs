﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Agenda.Repository
{
    public class InMemoryRepositoryPeriod : IRepositoryPeriod
    {

        IEnumerable<Period> Periods;

        private static InMemoryRepositoryPeriod _instance;

        private InMemoryRepositoryPeriod()
        {
            Periods = new List<Period>
            {
                new Period(new DateTime().AddDays(0),EPeriod.AM),
                new Period(new DateTime().AddDays(1),EPeriod.PM),
                new Period(new DateTime().AddDays(2),EPeriod.PM),
            };
        }

        public static InMemoryRepositoryPeriod GetInstance()
        {

            if (_instance == null)
            {
                _instance = new InMemoryRepositoryPeriod();
            }
            return _instance;
        }



        public IEnumerable<Period> Get()
        {
            return Periods;
        }

        public Period GetByGuid(Guid guid)
        {
            return Periods.Single(Period => Period.PeriodGuid == guid);
        }

        public bool Post(Period obj)
        {
            Periods = Periods.Append(obj);
            return true;
        }
    }
}
