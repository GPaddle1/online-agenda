﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;

namespace Agenda.Repository
{
    public class InMemoryRepositoryTeacher : IRepositoryTeacher
    {

        private static InMemoryRepositoryTeacher _instance;

        private InMemoryRepositoryTeacher()
        { }

        public static InMemoryRepositoryTeacher GetInstance()
        {
            if (_instance == null)
            {
                _instance = new InMemoryRepositoryTeacher();
            }
            return _instance;
        }

        public IEnumerable<Teacher> Get()
        {
            return InMemoryRepositoryUser.GetInstance().Get().Where(user=>user is Teacher).Cast<Teacher>();
        }

        public Teacher GetByGuid(Guid guid)
        {
            return (Teacher)InMemoryRepositoryUser.GetInstance().GetByGuid(guid);
        }

        public bool Post(Teacher obj)
        {
            return InMemoryRepositoryUser.GetInstance().Post(obj);
        }

        public bool RemoveByGuid(Guid guid)
        {
            return InMemoryRepositoryUser.GetInstance().RemoveByGuid(guid);
        }

        public Teacher PutByGuid(Guid guid, TemporaryUser temporaryObject)
        {
            return (Teacher)InMemoryRepositoryUser.GetInstance().PutByGuid(guid, temporaryObject);
        }
    }
}
