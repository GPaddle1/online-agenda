﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Agenda.Repository
{
    public class InMemoryRepositorySchoolMatter : IRepositorySchoolMatter
    {

        IEnumerable<SchoolMatter> SchoolMatters;
        IEnumerable<Teacher> Teachers;

        private static InMemoryRepositorySchoolMatter _instance;

        private InMemoryRepositorySchoolMatter()
        {
            Teachers = InMemoryRepositoryUser.GetInstance().Get().Where(user=>user is Teacher).Cast<Teacher>().ToList();
            Random random = new Random();
            SchoolMatters = new List<SchoolMatter>
            {
                new SchoolMatter("X",Teachers.ElementAt(random.Next(0, Teachers.Count()))),
                new SchoolMatter("Y",Teachers.ElementAt(random.Next(0, Teachers.Count()))),
                new SchoolMatter("Z",Teachers.ElementAt(random.Next(0, Teachers.Count()))),
            };
        }

        public static InMemoryRepositorySchoolMatter GetInstance()
        {

            if (_instance == null)
            {
                _instance = new InMemoryRepositorySchoolMatter();
            }
            return _instance;
        }



        public IEnumerable<SchoolMatter> Get()
        {
            return SchoolMatters;
        }

        public SchoolMatter GetByGuid(Guid guid)
        {
            return SchoolMatters.Single(schoolMatter => schoolMatter.SchoolMatterGuid == guid);
        }

        public bool Post(SchoolMatter obj)
        {
            SchoolMatters = SchoolMatters.Append(obj);
            return true;
        }

        public SchoolMatter PutByGuid(Guid guid, SchoolMatter temporaryObject)
        {
            SchoolMatter schoolMatter = GetByGuid(guid);
            schoolMatter.Name = temporaryObject.Name;
            schoolMatter.Teacher = temporaryObject.GetTeacher();

            return schoolMatter;
        }

        public bool RemoveByGuid(Guid guid)
        {
            try
            {
                SchoolMatter targetSchoolMatter = GetByGuid(guid);
                int oldSize = SchoolMatters.Count();
                SchoolMatters = SchoolMatters.Where(user => user != targetSchoolMatter);

                return oldSize != SchoolMatters.Count();
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
