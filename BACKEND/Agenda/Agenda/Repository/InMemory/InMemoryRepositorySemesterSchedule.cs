﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Agenda.Repository
{
    public class InMemoryRepositorySemesterSchedule : IRepositorySemesterSchedule
    {
        IEnumerable<SemesterSchedule> SemesterSchedules;

        private static InMemoryRepositorySemesterSchedule _instance;

        private InMemoryRepositorySemesterSchedule()
        {
            InMemoryRepositoryPromotion inMemoryRepositoryPromotion = InMemoryRepositoryPromotion.GetInstance();

            IEnumerable<Promotion> promotions = inMemoryRepositoryPromotion.Get();
            DateTime baseDate = new DateTime(2022, 1, 1);
            SemesterSchedules = new List<SemesterSchedule>
            {
                new SemesterSchedule(promotions.ElementAt(0), baseDate, baseDate.AddDays(200), "Semester schedule 1"),
                new SemesterSchedule(promotions.ElementAt(1), baseDate, baseDate.AddDays(150), "Semester schedule 2"),
                new SemesterSchedule(promotions.ElementAt(2), baseDate, baseDate.AddDays(100), "Semester schedule 3"),
            };
        }

        public static InMemoryRepositorySemesterSchedule GetInstance()
        {

            if (_instance == null)
            {
                _instance = new InMemoryRepositorySemesterSchedule();
            }
            return _instance;
        }

        public IEnumerable<SemesterSchedule> Get()
        {
            return GetInstance().SemesterSchedules;
        }

        public SemesterSchedule GetByGuid(Guid guid)
        {
            return GetInstance().SemesterSchedules.Single(semesterSchedule => semesterSchedule.SemesterScheduleGuid == guid);
        }

        public bool Post(SemesterSchedule obj)
        {
            SemesterSchedules = SemesterSchedules.Append(obj);
            return true;
        }
    }
}
