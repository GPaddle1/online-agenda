using Agenda.Repository;
using System;
using System.Collections.Generic;

namespace Agenda
{
    public interface IRepositoryHolidays : IGetRepository<DateTime>, IPostRepository<DateTime>
    {

        public bool Remove(DateTime dateTime);
    }
}
