﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Agenda.Repository
{
    public interface IRepositorySchoolMatter : IGetRepository<SchoolMatter>, IPostRepository<SchoolMatter>, IDeleteRepository<SchoolMatter>, IPutRepository<SchoolMatter, SchoolMatter>
    {
        public new IEnumerable<SchoolMatter> Get();
        public new SchoolMatter GetByGuid(Guid guid);
        public new bool Post(SchoolMatter obj);
        public new SchoolMatter PutByGuid(Guid guid, SchoolMatter temporaryObject);
        public new bool RemoveByGuid(Guid guid);
    }
}
