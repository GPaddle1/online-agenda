﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Agenda.Repository
{
    public interface IRepositoryPromotion : IGetRepository<Promotion>, IPostRepository<Promotion>, IDeleteRepository<Promotion>, IPutRepository<Promotion, Promotion>
    {

        public new IEnumerable<Promotion> Get();
        public new Promotion GetByGuid(Guid guid);
        public new bool Post(Promotion obj);
        public new bool RemoveByGuid(Guid guid);
        public new Promotion PutByGuid(Guid guid, Promotion temporaryObject);
    }
}
