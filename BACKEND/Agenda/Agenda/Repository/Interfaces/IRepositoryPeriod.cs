﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Agenda.Repository
{
    public interface IRepositoryPeriod : IGetRepository<Period>, IPostRepository<Period>
    {
        public new IEnumerable<Period> Get();
        public new Period GetByGuid(Guid guid);
        public new bool Post(Period obj);
    }
}
