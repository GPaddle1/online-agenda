﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;

namespace Agenda.Repository
{
    public interface IRepositoryStudent :
        IGetRepository<Student>,
        IPostRepository<Student>,
        IDeleteRepository<Student>,
        IPutRepository<Student, TemporaryUser>
    {
    }
}
