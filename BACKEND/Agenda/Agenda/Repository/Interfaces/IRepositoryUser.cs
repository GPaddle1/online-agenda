﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;

namespace Agenda.Repository
{
    public interface IRepositoryUser :
        IGetRepository<User>,
        IPostRepository<User>,
        IDeleteRepository<User>,
        IPutRepository<User, TemporaryUser>
    {
        public User ContainsUser(Credentials credentials);
    }
}
