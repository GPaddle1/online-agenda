﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Agenda.Repository
{
    public interface IRepositorySemesterSchedule : IGetRepository<SemesterSchedule>, IPostRepository<SemesterSchedule>
    {
        public new IEnumerable<SemesterSchedule> Get();
        public new SemesterSchedule GetByGuid(Guid guid);
        public new bool Post(SemesterSchedule obj);
    }
}
