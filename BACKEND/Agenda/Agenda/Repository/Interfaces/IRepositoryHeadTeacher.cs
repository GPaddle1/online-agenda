﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;

namespace Agenda.Repository
{
    public interface IRepositoryHeadTeacher :
        IGetRepository<HeadTeacher>,
        IPostRepository<HeadTeacher>,
        IDeleteRepository<HeadTeacher>,
        IPutRepository<HeadTeacher, TemporaryUser>
    {
    }
}
