﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;

namespace Agenda.Repository
{
    public interface IRepositoryTeacher :
        IGetRepository<Teacher>,
        IPostRepository<Teacher>,
        IDeleteRepository<Teacher>,
        IPutRepository<Teacher, TemporaryUser>
    {
    }
}
