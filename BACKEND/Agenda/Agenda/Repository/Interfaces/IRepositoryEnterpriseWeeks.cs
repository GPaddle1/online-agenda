using Agenda.Repository;

namespace Agenda
{
    public interface IRepositoryEnterpriseWeeks : IGetRepository<EnterpriseWeek>, IPostRepository<EnterpriseWeek>, IDeleteRepository<EnterpriseWeek>
    {
    }
}
