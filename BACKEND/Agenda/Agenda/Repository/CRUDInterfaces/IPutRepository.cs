﻿using System;

namespace Agenda.Repository
{
    public interface IPutRepository<T, V>
    {
        public T PutByGuid(Guid guid, V temporaryObject);

    }
}