﻿namespace Agenda.Repository
{
    public interface IPostRepository<T>
    {
        public bool Post(T obj);
    }
}