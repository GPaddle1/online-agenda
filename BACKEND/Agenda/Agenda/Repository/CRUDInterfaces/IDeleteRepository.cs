﻿using System;

namespace Agenda.Repository
{
    public interface IDeleteRepository<T>
    {
        public bool RemoveByGuid(Guid guid);
    }
}
