﻿using System;
using System.Collections.Generic;

namespace Agenda.Repository
{
    public interface IGetRepository<T>
    {
        public IEnumerable<T> Get();
        public T GetByGuid(Guid guid);
    }
}
