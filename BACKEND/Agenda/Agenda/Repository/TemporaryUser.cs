﻿namespace Agenda.Repository
{
    public class TemporaryUser
    {

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string password { get; set; }
    }
}
