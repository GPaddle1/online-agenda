using System;

namespace Agenda
{
    public class AgendaException : Exception
    {
        public AgendaException() { }
        public AgendaException(string message) : base(message) { }
        public AgendaException(string message, Exception inner) : base(message, inner) { }
        protected AgendaException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
