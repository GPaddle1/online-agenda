namespace Agenda
{
    public interface IHaveASchedule
    {
        public bool CanSeeLesson(SchoolMatter schoolMatter);
        public bool CanConsultSemesterSchedule(SemesterSchedule semesterSchedule);
    }
}
