﻿using System;
using System.Net.Mail;
using System.Security.Cryptography;

namespace Agenda
{
    public class User
    {
        public Guid UserGuid { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public MailAddress Email { get; set; }
        private string HashedPassword { get; set; }

        private HashSalt hashSalt;
        public bool IsTeacher { get => Role == ERole.HeadTeacher || Role == ERole.Teacher; }
        public ERole Role { get; set; } = ERole.Student;

        public User(string firstName, string lastName, MailAddress email, string password)
        {
            if (string.IsNullOrEmpty(firstName))
            {
                throw new ArgumentException($"« {nameof(firstName)} » ne peut pas être vide ou avoir la valeur Null.", nameof(firstName));
            }

            if (string.IsNullOrEmpty(lastName))
            {
                throw new ArgumentException($"« {nameof(lastName)} » ne peut pas être vide ou avoir la valeur Null.", nameof(lastName));
            }


            if (string.IsNullOrEmpty(password))
            {
                throw new ArgumentException($"« {nameof(password)} » ne peut pas être vide ou avoir la valeur Null.", nameof(password));
            }

            UserGuid = Guid.NewGuid();
            FirstName = firstName;
            LastName = lastName;
            Email = email ?? throw new ArgumentNullException(nameof(email));

            SetNewPassword(password);
        }

        public void SetNewPassword(string password)
        {
            hashSalt = HashSalt.GenerateSaltedHash(64, password);

            HashedPassword = hashSalt.Hash;
        }

        public bool VerifyPassword(string enteredPassword)
        {
            var saltBytes = Convert.FromBase64String(hashSalt.Salt);
            var rfc2898DeriveBytes = new Rfc2898DeriveBytes(enteredPassword, saltBytes, 10000);
            return Convert.ToBase64String(rfc2898DeriveBytes.GetBytes(256)) == hashSalt.Hash;
        }

    }
}