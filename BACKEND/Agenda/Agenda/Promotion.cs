using System;

namespace Agenda
{

    public class Promotion
    {
        public string Title { get; set; }
        public Guid PromotionGuid { get; internal set; }

        public Promotion(string title)
        {
            Title = title;
            PromotionGuid = Guid.NewGuid();
        }
    }
}
