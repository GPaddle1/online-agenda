﻿using System;

namespace Agenda
{
    public class Day : IScheduleView
    {
        private DateTime TargetDateTime { get; set; }

        public bool IsDateShown(DateTime dateTime)
        {
            return TargetDateTime == dateTime;
        }

        public Day(DateTime targetDateTime)
        {
            this.TargetDateTime = targetDateTime;
        }
    }
}
