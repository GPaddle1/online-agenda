using System;
using System.Collections.Generic;
using System.Linq;

namespace Agenda
{
    public class SemesterSchedule
    {
        public Promotion Promotion { get; set; }

        private Dictionary<Period, SchoolMatter> LessonsTimeSlots { get; set; }

        public string SemesterScheduleName { get; set; }
        public IEnumerable<DateTime> Holidays { get; set; }
        public IEnumerable<int> EntrepriseWeeks { get; set; }

        private Dictionary<SchoolMatter, int> SchoolMatterDetailsNumberOfSlot { get; set; }
        public IEnumerable<SchoolMatterDuration> SchoolMatterDurations
        {
            get
            {
                return SchoolMatterDetailsNumberOfSlot.Select(kvp => new SchoolMatterDuration(kvp.Key, kvp.Value)).ToList(); ;
            }
        }
        public Guid SemesterScheduleGuid { get; internal set; }

        //public DateTime StartDate;
        //public DateTime EndDate;

        public Dictionary<Period, SchoolMatter> GetLessonsTimeSlots()
        {
            return LessonsTimeSlots;
        }

        public SemesterSchedule(Promotion promotion, DateTime startDate, DateTime endDate, string semesterScheduleName)
        {
            LessonsTimeSlots = new Dictionary<Period, SchoolMatter>();

            SchoolMatterDetailsNumberOfSlot = new Dictionary<SchoolMatter, int>();
            Promotion = promotion;
            SemesterScheduleGuid = Guid.NewGuid();
            SemesterScheduleName = semesterScheduleName;

            InitHolidays(InMemoryRepositoryHolidays.GetInstance());
            InitEntrepriseWeeks(InMemoryRepositoryEnterpriseWeeks.GetInstance());
            GenerateTimeSlots(startDate, endDate);
        }

        public SchoolMatter GetLesson(Period period)
        {
            if (!LessonsTimeSlots.ContainsKey(period))
            {
                throw new AgendaException("Unavailable period");
            }
            return LessonsTimeSlots[period];
        }

        public bool AddLesson(SchoolMatter schoolMatter, Period period)
        {
            if (!SchoolMatterDetailsNumberOfSlot.ContainsKey(schoolMatter))
            {
                return false;
            }

            if (!schoolMatter.GetTeacher().IsAvailable(period))
            {
                return false;
            }

            if (GetLessonsBySchoolMatter(schoolMatter).Count >= SchoolMatterDetailsNumberOfSlot[schoolMatter])
            {
                return false;
            }

            if (!LessonsTimeSlots.ContainsKey(period))
            {
                throw new AgendaException($"The specified period is not available : {period}");
            }

            LessonsTimeSlots[period] = schoolMatter;

            return LessonsTimeSlots[period] == schoolMatter;

        }

        internal bool IsTeacherRegistered(Teacher teacher)
        {
            foreach (KeyValuePair<SchoolMatter, int> schoolMatterDuration in SchoolMatterDetailsNumberOfSlot)
            {
                if (schoolMatterDuration.Key.TeacherGuid == teacher.UserGuid)
                {
                    return true;
                }
            }
            return false;
        }

        public void SetLessonsDuration(Dictionary<SchoolMatter, int> schoolMatterDetails)
        {
            SchoolMatterDetailsNumberOfSlot = schoolMatterDetails;

            foreach (KeyValuePair<SchoolMatter, int> schoolMatterDuration in schoolMatterDetails)
            {
                schoolMatterDuration.Key.Duration = schoolMatterDuration.Value;
            }
        }

        public void AddLessonDuration(SchoolMatter schoolMatter, int schoolMatterDuration)
        {
            schoolMatter.Duration = schoolMatterDuration;

            if (SchoolMatterDetailsNumberOfSlot.ContainsKey(schoolMatter))
            {
                SchoolMatterDetailsNumberOfSlot[schoolMatter] = schoolMatterDuration;
            }
            else
            {
                SchoolMatterDetailsNumberOfSlot.Add(schoolMatter, schoolMatterDuration);
            }
        }

        public void InitHolidays(IRepositoryHolidays holydaysRepository)
        {
            Holidays = holydaysRepository.Get();
        }

        public void InitEntrepriseWeeks(IRepositoryEnterpriseWeeks entrepriseWeeksRepository)
        {
            EntrepriseWeeks = new List<int>();

            IEnumerable<EnterpriseWeek> currentPromotionEnterpriseWeeks = entrepriseWeeksRepository.Get().Where(enterpriseWeek => enterpriseWeek.PromotionGuid == Promotion.PromotionGuid);

            foreach (EnterpriseWeek item in currentPromotionEnterpriseWeeks)
            {
                EntrepriseWeeks = EntrepriseWeeks.Append(item.WeekNumber);
            }
        }

        public bool RemoveLesson(Period period)
        {
            if (!LessonsTimeSlots.ContainsKey(period) || LessonsTimeSlots[period] == null)
            {
                return false;
            }

            LessonsTimeSlots[period] = null;
            return true;
        }

        public Dictionary<Period, SchoolMatter> GetLessons(IHaveASchedule user, IScheduleView scheduleView = null)
        {

            return LessonsTimeSlots.Where(schoolTimeSlot =>
            {
                bool dateIsInView = (scheduleView == null || scheduleView.IsDateShown(schoolTimeSlot.Key.Date));

                SchoolMatter schoolMatter = schoolTimeSlot.Value;
                bool timeSlotIsNotEmpty = schoolMatter != null;

                return dateIsInView && timeSlotIsNotEmpty && schoolMatter.CanSeeLesson(user);

            }).ToDictionary(i => i.Key, i => i.Value);
        }
        public bool ContainsLesson(SchoolMatter schoolMatter)
        {
            return SchoolMatterDetailsNumberOfSlot.ContainsKey(schoolMatter);
        }

        public Dictionary<Period, SchoolMatter> GetLessonsBySchoolMatter(SchoolMatter schoolMatter)
        {

            return LessonsTimeSlots.Where(schoolTimeSlot =>
            {
                SchoolMatter schoolMatterToCheck = schoolTimeSlot.Value;

                return schoolMatterToCheck == schoolMatter;

            }).ToDictionary(i => i.Key, i => i.Value);
        }

        public static IEnumerable<DateTime> EachDay(DateTime from, DateTime thru)
        {
            for (var day = from.Date; day.Date <= thru.Date; day = day.AddDays(1))
                yield return day;
        }


        private void GenerateTimeSlots(DateTime startDate, DateTime endDate)
        {
            foreach (DateTime day in EachDay(startDate, endDate))
            {
                if (UnavailableDay(day))
                {
                    continue;
                }

                LessonsTimeSlots.Add(new Period(day, EPeriod.AM), null);
                LessonsTimeSlots.Add(new Period(day, EPeriod.PM), null);
            }

        }

        private bool UnavailableDay(DateTime day)
        {
            return Holidays.Contains(day) || EntrepriseWeeks.Contains(Week.GetWeekNumber(day)) || day.DayOfWeek == DayOfWeek.Saturday || day.DayOfWeek == DayOfWeek.Sunday;
        }

        public bool RegisterSchoolMatterByBlock()
        {
            if (!EnoughAvailableSlots())
            {
                throw new AgendaException("There is more lessons to place than the available slots, please check it");
            }

            foreach (KeyValuePair<SchoolMatter, int> schoolMatterNumberOfSlot in SchoolMatterDetailsNumberOfSlot)
            {

                SchoolMatter currentSchoolMatter = schoolMatterNumberOfSlot.Key;
                int alreadyPlacedOnSchedule = GetLessonsBySchoolMatter(currentSchoolMatter).Count;

                if (!PlaceOneSchoolMatterByBlock(schoolMatterNumberOfSlot, alreadyPlacedOnSchedule))
                {
                    throw new AgendaException("Not enough slots to fill all the school matters slots");
                }
            }

            return true;
        }

        private bool PlaceOneSchoolMatterByBlock(KeyValuePair<SchoolMatter, int> schoolMatterNumberOfSlot, int alreadyPlacedOnSchedule)
        {

            int timeSlotsToAdd = schoolMatterNumberOfSlot.Value - alreadyPlacedOnSchedule;

            foreach (KeyValuePair<Period, SchoolMatter> timeSlot in LessonsTimeSlots)
            {
                if (timeSlot.Value == null && AddLesson(schoolMatterNumberOfSlot.Key, timeSlot.Key))
                {
                    timeSlotsToAdd--;
                }

                if (timeSlotsToAdd == 0)
                {
                    break;
                }
            }

            return timeSlotsToAdd == 0;
        }

        public bool PeriodicFillByDate(DayOfWeek dayOfWeek, EPeriod period, SchoolMatter schoolMatter, bool placeByBlockIfFull = false)
        {
            if (!EnoughAvailableSlots())
            {
                throw new AgendaException("There is more lessons to place than the available slots, please check it");
            }

            int nbLessonsToPlace;
            if (!SchoolMatterDetailsNumberOfSlot.TryGetValue(schoolMatter, out nbLessonsToPlace))
            {
                throw new AgendaException("The specified school matter duration has not been set");
            }

            foreach (KeyValuePair<Period, SchoolMatter> timeSlot in LessonsTimeSlots)
            {
                Period currentPeriod = timeSlot.Key;
                SchoolMatter currentSchoolMatter = timeSlot.Value;
                if (currentSchoolMatter == null && currentPeriod.Date.DayOfWeek == dayOfWeek && currentPeriod.EPeriod == period && AddLesson(schoolMatter, timeSlot.Key))
                {
                    nbLessonsToPlace--;
                }

                if (nbLessonsToPlace == 0)
                {
                    return true;
                }
            }

            if (placeByBlockIfFull)
            {
                KeyValuePair<SchoolMatter, int> remainingTimeSlotsToAdd = new KeyValuePair<SchoolMatter, int>(schoolMatter, nbLessonsToPlace);
                if (!PlaceOneSchoolMatterByBlock(remainingTimeSlotsToAdd, 0))
                {
                    throw new AgendaException("Not enough slots to fill all the school matters slots");
                }
                return true;
            }

            return false;
        }

        public void PeriodicScheduleGeneration()
        {

            if (!EnoughAvailableSlots())
            {
                throw new AgendaException("There is more lessons to place than the available slots, please check it");
            }

            int currentSchoolMatterIndex = 0;

            bool needToRelaunchFunction = false;

            int nbSchoolMatters = SchoolMatterDetailsNumberOfSlot.Count;

            List<TimeSlot> timeSlots = TimeSlot.GetAvailableTimeSlots();

            foreach (TimeSlot timeSlot in timeSlots)
            {
                if (currentSchoolMatterIndex >= nbSchoolMatters)
                {
                    break;
                }

                SchoolMatter currentSchoolMatter = SchoolMatterDetailsNumberOfSlot.ElementAt(currentSchoolMatterIndex).Key;

                int nbLessonsAlreadyPlaced = GetLessonsBySchoolMatter(currentSchoolMatter).Count;
                int nbLessonsForCurrentSchoolMatter = SchoolMatterDetailsNumberOfSlot[currentSchoolMatter];

                int nbLessonsToPlace = nbLessonsForCurrentSchoolMatter - nbLessonsAlreadyPlaced;

                Dictionary<Period, SchoolMatter> lessonsTimeSlots = LessonsTimeSlots.Where(
                    lessonTimeSlot => lessonTimeSlot.Key.EPeriod == timeSlot.EPeriodValue
                         && lessonTimeSlot.Key.Date.DayOfWeek == timeSlot.Day
                         && lessonTimeSlot.Value == null).ToDictionary(e => e.Key, e => e.Value);

                List<Period> AllDaysForTheCurrentPeriod = new List<Period>(lessonsTimeSlots.Keys);

                if (AllDaysForTheCurrentPeriod.Count < nbLessonsToPlace)
                {
                    needToRelaunchFunction = true;
                }

                if (AllDaysForTheCurrentPeriod.Count == 0)
                {
                    continue;
                }

                foreach (Period period in AllDaysForTheCurrentPeriod)
                {
                    if (!AddLesson(currentSchoolMatter, period))
                    {
                        continue;
                    }
                }
                currentSchoolMatterIndex++;
            }

            if (needToRelaunchFunction)
            {
                PeriodicScheduleGeneration();
            }
        }

        private bool EnoughAvailableSlots()
        {
            int totalSlotsToAdd = SchoolMatterDetailsNumberOfSlot.Sum(numberOfSlotBySchoolMatter => numberOfSlotBySchoolMatter.Value);

            return LessonsTimeSlots.Count >= totalSlotsToAdd;
        }

        public void SwapTwoLessons(Period p1, Period p2)
        {
            SchoolMatter schoolMatter = LessonsTimeSlots[p1];

            LessonsTimeSlots[p1] = LessonsTimeSlots[p2];
            LessonsTimeSlots[p2] = schoolMatter;
        }

        public IEnumerable<SchoolMatter> GetRegisteredSchoolMatters()
        {
            return SchoolMatterDetailsNumberOfSlot.Keys;
        }
    }
}
