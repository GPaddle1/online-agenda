﻿using System;

namespace Agenda
{
    public interface IScheduleView
    {
        public bool IsDateShown(DateTime dateTime);
    }
}