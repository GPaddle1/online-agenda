using System.Net.Mail;

namespace Agenda
{
    public class Student : User, IHaveASchedule
    {

        public Promotion Promotion { get; set; }

        public Student(string firstName, string lastName, MailAddress email, string password, Promotion promotion) : base(firstName, lastName, email, password)
        {
            Promotion = promotion;
            Role = ERole.Student;
        }

        public bool CanSeeLesson(SchoolMatter schoolMatter)
        {
            return true;
        }

        public bool CanConsultSemesterSchedule(SemesterSchedule semesterSchedule)
        {
            return semesterSchedule.Promotion == this.Promotion;
        }
    }
}
