﻿using System;

namespace Agenda
{
    public class SchoolMatterDuration
    {
        private SchoolMatter SchoolMatter { get; set; }
        public Guid SchoolMatterGuid { get { return SchoolMatter.SchoolMatterGuid; } }
        public int Duration { get; set; }
        public Guid Guid { get; set; }
        public SchoolMatterDuration(SchoolMatter schoolMatter, int duration)
        {
            SchoolMatter = schoolMatter;
            Duration = duration;
            Guid = Guid.NewGuid();
        }
    }
}