using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;

namespace Agenda
{
    public class Teacher : User, IHaveASchedule
    {
        public List<Period> Unavailability { get; set; }


        public Teacher(string firstName, string lastName, MailAddress email, string password) : base(firstName, lastName, email, password)
        {
            if (string.IsNullOrEmpty(firstName))
            {
                throw new ArgumentException($"� {nameof(firstName)} � ne peut pas �tre vide ou avoir la valeur Null.", nameof(firstName));
            }

            if (string.IsNullOrEmpty(lastName))
            {
                throw new ArgumentException($"� {nameof(lastName)} � ne peut pas �tre vide ou avoir la valeur Null.", nameof(lastName));
            }

            if (email is null)
            {
                throw new ArgumentNullException(nameof(email));
            }

            if (string.IsNullOrEmpty(password))
            {
                throw new ArgumentException($"� {nameof(password)} � ne peut pas �tre vide ou avoir la valeur Null.", nameof(password));
            }

            Unavailability = new List<Period>();

            Role = ERole.Teacher;

        }

        public Teacher(string firstName, string lastName, MailAddress email, string password, List<Period> unavailability) : this(firstName, lastName, email, password)
        {
            Unavailability = unavailability;
        }

        public bool CanSeeLesson(SchoolMatter schoolMatter)
        {
            return schoolMatter.GetTeacher().Equals(this);
        }

        public bool IsAvailable(Period period)
        {
            return !Unavailability.Contains(period);
        }

        public bool CanConsultSemesterSchedule(SemesterSchedule semesterSchedule)
        {
            return semesterSchedule.IsTeacherRegistered(this);
        }

        public void RemoveUnavailability(Guid periodGuid)
        {
            Unavailability = Unavailability.Where(period => period.PeriodGuid != periodGuid).ToList();
        }

        public void AddUnavailabilities(IEnumerable<Period> periods)
        {

            foreach (Period period in periods)
            {
                Unavailability.Add(period);
            }
        }

        public void AddUnavailability(Period period)
        {
            if (Unavailability.Contains(period))
            {
                throw new AgendaException("Impossible to add two time the same period");
            }
            
            Unavailability.Add(period);
        }
    }
}
