﻿using System;

namespace Agenda
{
    public class Lesson
    {
        public Period Period { get; set; }
        public SchoolMatter SchoolMatter { get; set; }
        public Guid Guid { get; set; }

        public Lesson(Period period, SchoolMatter schoolMatter)
        {
            Period = period;
            SchoolMatter = schoolMatter;
            Guid = Guid.NewGuid();
        }
    }
}
