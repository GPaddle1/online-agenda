using System;
using System.Text.Json.Serialization;

namespace Agenda
{
    public class SchoolMatter
    {
        public string Name { get; set; }
        [JsonIgnore]
        public Teacher Teacher { get; set; }
        public int Duration { get; set; }

        public Guid TeacherGuid
        {
            get
            {
                return Teacher.UserGuid;
            }
        }
        public Guid SchoolMatterGuid { get; set; }


        public SchoolMatter(string name, Teacher teacher)
        {
            Name = name;
            Teacher = teacher;
            SchoolMatterGuid = Guid.NewGuid();
        }


        public SchoolMatter(string name, Teacher teacher, int duration) : this(name, teacher)
        {
            Duration = duration;
        }

        public Teacher GetTeacher()
        {
            return Teacher;
        }

        public bool CanSeeLesson(IHaveASchedule user)
        {
            return user.CanSeeLesson(this);
        }
    }
}
