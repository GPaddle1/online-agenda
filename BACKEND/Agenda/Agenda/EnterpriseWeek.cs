﻿using System;

namespace Agenda
{
    public class EnterpriseWeek
    {

        public int WeekNumber { get; set; }
        public Promotion Promotion { private get; set; }
        public Guid PromotionGuid { get { return Promotion.PromotionGuid; } }
        public Guid EnterpriseWeekGuid { get; }

        public EnterpriseWeek(int weekNumber, Promotion promotion)
        {
            WeekNumber = weekNumber;
            Promotion = promotion;
            EnterpriseWeekGuid = Guid.NewGuid();
        }
    }
}