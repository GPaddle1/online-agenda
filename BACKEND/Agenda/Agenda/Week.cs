﻿using System;
using System.Globalization;

namespace Agenda
{
    public class Week : IScheduleView
    {
        private int TargetWeekNumber { get; set; }
        private int TargetYear { get; set; }

        public Week(int targetWeekNumber, int targetYear)
        {
            this.TargetWeekNumber = targetWeekNumber;
            this.TargetYear = targetYear;
        }

        public bool IsDateShown(DateTime dateTime)
        {
            int weekNumber = GetWeekNumber(dateTime);
            int year = dateTime.Year;

            return weekNumber == this.TargetWeekNumber && year == this.TargetYear;
        }

        public static int GetWeekNumber(DateTime day)
        {
            return CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(day, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }
    }
}
