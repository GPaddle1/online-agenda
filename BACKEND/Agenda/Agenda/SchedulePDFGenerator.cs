﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Syncfusion.HtmlConverter;
using Syncfusion.Pdf;
using Syroot.Windows.IO;

namespace Agenda
{
    public class SchedulePDFGenerator
    {
        public static bool ScheduleHTMLToPDF(SemesterSchedule semesterSchedule)
        {
            string HTMLSchedule = ScheduleToHTML(semesterSchedule);

            DateTime now = DateTime.Now;

            HtmlToPdfConverter htmlConverter = new HtmlToPdfConverter(HtmlRenderingEngine.Blink);

            BlinkConverterSettings blinkConverterSettings = new BlinkConverterSettings();

            string baseUrl = new KnownFolder(KnownFolderType.Downloads).Path;
            //string baseUrl = @"C:/Temp/HTMLFiles/";

            blinkConverterSettings.BlinkPath = @"BlinkBinariesWindows\\";

            htmlConverter.ConverterSettings = blinkConverterSettings;

            PdfDocument document = htmlConverter.Convert(HTMLSchedule, baseUrl);
            document.PageSettings.Margins.All = 50;

            FileStream fileStream = new FileStream("Schedule_" + now.Day + now.Month + now.Year + now.Hour + now.Minute + now.Second + ".pdf", FileMode.CreateNew, FileAccess.ReadWrite);

            //Save and close the PDF document 
            document.Save(fileStream);
            document.Close(true);

            return true;
        }

        public static string ScheduleToHTML(SemesterSchedule semesterSchedule)
        {
            List<int> schoolWeeks = GetSchoolWeeks(semesterSchedule);

            string HTMLSchedule = "";

            HTMLSchedule += AddHTMLDoctype();

            HTMLSchedule += AddHTMLHeader();

            HTMLSchedule += AddStyle();

            HTMLSchedule += @"<body>";

            HTMLSchedule += AddTitle(semesterSchedule.Promotion.Title);

            HTMLSchedule += AddTimes();

            HTMLSchedule += @"<table>";

            HTMLSchedule += AddTableHeader();

            foreach (int weekNumber in schoolWeeks)
            {
                DateTime dateOnTheCurrentWeek = GetTheFirstDateOfASpecificWeekContainedOnTheSchedule(weekNumber, semesterSchedule.GetLessonsTimeSlots());

                if (dateOnTheCurrentWeek.DayOfWeek == DayOfWeek.Saturday || dateOnTheCurrentWeek.DayOfWeek == DayOfWeek.Sunday)
                {
                    continue;
                }

                DateTime currentMonday = GetMondayOfWeek(dateOnTheCurrentWeek);
                DateTime currentFriday = GetFridayOfWeek(dateOnTheCurrentWeek);

                DateTime currentDate = currentMonday;

                EPeriod period;

                HTMLSchedule += AddSpan();

                HTMLSchedule += @"<tr>";

                HTMLSchedule += AddColHeader(weekNumber, currentMonday, currentFriday);

                while (currentDate != currentFriday.AddDays(1))
                {
                    HTMLSchedule += AddDate(currentDate);
                    currentDate = currentDate.AddDays(1);
                }

                HTMLSchedule += @"</tr>";

                currentDate = currentMonday;

                for (int i = 1; i <= 2; i++)
                {
                    HTMLSchedule += @"<tr>";

                    period = (i == 1 ? EPeriod.AM : EPeriod.PM);

                    while (currentDate != currentFriday.AddDays(1))
                    {
                        if (!semesterSchedule.GetLessonsTimeSlots().TryGetValue(new Period(currentDate, period), out SchoolMatter schoolMatter))
                        {
                            HTMLSchedule += AddHoliday();
                        }
                        else if (schoolMatter is null)
                        {
                            HTMLSchedule += AddEmptyPeriod();
                        }
                        else
                        {
                            HTMLSchedule += AddSchoolPeriod(schoolMatter);
                        }

                        currentDate = currentDate.AddDays(1);
                    }

                    HTMLSchedule += @"</tr>";

                    currentDate = currentMonday;
                }

            }
            HTMLSchedule += @"</table>
                    </body>
                </html>";

            return HTMLSchedule;

        }

        private static string AddSchoolPeriod(SchoolMatter schoolMatter)
        {
            Teacher teacher = schoolMatter.GetTeacher();
            return $"<td>{schoolMatter.Name}<br>{teacher.FirstName} {teacher.LastName}</td>";
        }

        private static List<int> GetSchoolWeeks(SemesterSchedule semesterSchedule)
        {
            List<int> schoolWeeks = new List<int>();

            DateTime startDate = semesterSchedule.GetLessonsTimeSlots().FirstOrDefault().Key.Date;
            int startWeek = Week.GetWeekNumber(startDate);


            DateTime endDate = semesterSchedule.GetLessonsTimeSlots().LastOrDefault().Key.Date;
            int endWeek = Week.GetWeekNumber(endDate);

            if (startWeek < endWeek)
            {
                for (int i = startWeek; i <= endWeek; i++)
                {
                    if (!semesterSchedule.EntrepriseWeeks.Contains(i))
                    {
                        schoolWeeks.Add(i);
                    }
                }
            }
            else
            {
                for (int i = startWeek; i <= 52; i++)
                {
                    if (!semesterSchedule.EntrepriseWeeks.Contains(i))
                    {
                        schoolWeeks.Add(i);
                    }
                }
                for (int i = 1; i <= endWeek; i++)
                {
                    if (!semesterSchedule.EntrepriseWeeks.Contains(i))
                    {
                        schoolWeeks.Add(i);
                    }
                }
            }

            return schoolWeeks;
        }

        private static string AddEmptyPeriod()
        {
            return "<td></td>";
        }

        private static string AddHoliday()
        {
            return "<td style='background-color: darkgray;' ></td>";
        }

        private static string AddDate(DateTime currentDate)
        {
            return @"<th>" + currentDate.ToShortDateString() + "</th>";
        }

        private static string AddColHeader(int weekNumber, DateTime currentMonday, DateTime currentFriday)
        {
            return @"<th rowspan='3'>
                        Semaine " + weekNumber.ToString() + "<br> " +
                        currentMonday.ToShortDateString() + " - " +
                        currentFriday.ToShortDateString() + " " +
                    "</th>";
        }

        private static string AddTimes()
        {
            return @"<p> Horaires : 
                        <br>
                        Matin (8h30 - 12h15)
                        <br>
                        Après-midi (13h30 - 17h15)
                    </p>";
        }

        private static string AddSpan()
        {
            return @"<tr>
                        <th style='border:0' colspan='6'></th>
                    </tr>";
        }

        private static string AddTableHeader()
        {
            return @"<tr>
                        <th></th>
                        <th>Lundi</th>
                        <th>Mardi</th> 
                        <th>Mercredi</th>
                        <th>Jeudi</th> 
                        <th>Vendredi</th>
                      </tr>";
        }

        private static string AddTitle(string title)
        {
            return @"<h1>Emploi du temps</h1>
                     <h2>" + title + "</h2>";
        }

        private static string AddStyle()
        {
            return @"
            <style type='text/css' media='screen'>
                h1, h2, td { text-align: center; }
                table { margin-left:auto;
                        margin-right:auto; }
                table, th, td { border: 1px solid black; border-collapse: collapse;}   
                th, td { padding: 10px 30px; }
                h1 { margin-top : 100px; }
                p { margin: 50px 50px;}
            </style>";
        }

        private static string AddHTMLHeader()
        {
            return @"<head>
                      <meta charset='utf-8'>
                      <title>Schedule</title>
                    </head>";
        }

        private static string AddHTMLDoctype()
        {
            return @"<!doctype html>
                    <html lang='fr'>";
        }

        private static DateTime GetMondayOfWeek(DateTime date)
        {
            int daysDiff = DayOfWeek.Monday - date.DayOfWeek;
            if (date.DayOfWeek == DayOfWeek.Sunday) daysDiff -= 7;
            return date.AddDays(daysDiff).Date;
        }

        private static DateTime GetFridayOfWeek(DateTime date)
        {
            int daysDiff = DayOfWeek.Friday - date.DayOfWeek;
            if (date.DayOfWeek == DayOfWeek.Sunday) daysDiff -= 7;
            return date.AddDays(daysDiff).Date;
        }

        private static DateTime GetTheFirstDateOfASpecificWeekContainedOnTheSchedule(int weekNumber, Dictionary<Period, SchoolMatter> lessonsTimeSlots)
        {
            KeyValuePair<Period, SchoolMatter> currentPeriod = lessonsTimeSlots.FirstOrDefault(x => Week.GetWeekNumber(x.Key.Date) == weekNumber);

            if (currentPeriod.Key is null && currentPeriod.Value is null)
            {
                throw new AgendaException("The specific week has not course on the schedule");
            }

            DateTime currentDate = currentPeriod.Key.Date;

            return currentDate;
        }
    }
}
