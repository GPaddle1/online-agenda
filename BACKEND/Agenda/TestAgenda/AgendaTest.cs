using Agenda;
using System;
using System.Collections.Generic;
using System.Net.Mail;
using Xunit;

namespace TestAgenda
{
    public class AgendaTest
    {

        SemesterSchedule semesterSchedule;
        SchoolMatter TestsSchoolMatter;
        SchoolMatter SecuritySchoolMatter;
        Teacher Edouard;
        Teacher J2M;
        Promotion Cnam2APromotion;

        const int NUMBER_OF_TIME_SLOTS = 30;

        public AgendaTest()
        {
            Cnam2APromotion = new Promotion("Informatique CNAM - 2A - S2");
            semesterSchedule = new SemesterSchedule(Cnam2APromotion, new DateTime(2022, 1, 1), new DateTime(2022, 6, 30), "Semester schedule");

            Dictionary<SchoolMatter, int> schoolMattersNumberOfSlot = new Dictionary<SchoolMatter, int>();
            Edouard = new Teacher("Edouard", "Mangel", new MailAddress("Edouard@Mangel.com"), "ABCD");
            TestsSchoolMatter = new SchoolMatter("Tests", Edouard);
            schoolMattersNumberOfSlot.Add(TestsSchoolMatter, NUMBER_OF_TIME_SLOTS);

            J2M = new Teacher("Jean-Marc", "Muller", new MailAddress("Jean-Marc@Muller.com"), "ABCD");
            SecuritySchoolMatter = new SchoolMatter("S�curit�", J2M);
            schoolMattersNumberOfSlot.Add(SecuritySchoolMatter, NUMBER_OF_TIME_SLOTS);

            semesterSchedule.SetLessonsDuration(schoolMattersNumberOfSlot);
        }

        [Fact]
        public void GetLessonAtSpecifiedTimeSlot()
        {

            Period period = new Period(new DateTime(2022, 4, 27), EPeriod.PM);
            Assert.True(semesterSchedule.AddLesson(TestsSchoolMatter, period));

            Period period2 = new Period(new DateTime(2022, 5, 18), EPeriod.PM);
            Assert.True(semesterSchedule.AddLesson(TestsSchoolMatter, period2));

            Assert.Equal(TestsSchoolMatter, semesterSchedule.GetLesson(period));
            Assert.Equal(TestsSchoolMatter, semesterSchedule.GetLesson(period2));

            Period periodToCheck = new Period(new DateTime(2022, 4, 27), EPeriod.PM);

            Assert.NotNull(semesterSchedule.GetLesson(periodToCheck));
            Assert.Null(semesterSchedule.GetLesson(new Period(new DateTime(2022, 1, 3), EPeriod.AM)));
        }

        [Fact]
        public void TeacherShouldKnowOnlyHisSchedule()
        {
            SetupTwoSchoolMattersWithThreeLessons();

            Assert.All(semesterSchedule.GetLessons(user: Edouard), schoolTimeSlot => schoolTimeSlot.Value.GetTeacher().Equals(Edouard));
            Assert.Equal(2, semesterSchedule.GetLessons(user: Edouard).Count);
        }

        private void SetupTwoSchoolMattersWithThreeLessons()
        {
            SetupOneSchoolMatterWithTwoLessons();

            Period period3 = new Period(new DateTime(2022, 4, 27), EPeriod.AM);
            semesterSchedule.AddLesson(SecuritySchoolMatter, period3);
        }

        private void SetupOneSchoolMatterWithTwoLessons()
        {
            Period period = new Period(new DateTime(2022, 4, 27), EPeriod.PM);
            Period period2 = new Period(new DateTime(2022, 5, 18), EPeriod.PM);
            semesterSchedule.AddLesson(TestsSchoolMatter, period);
            semesterSchedule.AddLesson(TestsSchoolMatter, period2);
        }

        [Fact]
        public void StudentsShouldKnowAllTheSchedule()
        {
            SetupTwoSchoolMattersWithThreeLessons();

            Assert.Equal(3, semesterSchedule.GetLessons(new Student("X", "Y", new MailAddress("x@y.com"), "ABCD", Cnam2APromotion)).Count);
        }

        [Fact]
        public void RemoveAtSpecifiedTimeSlot()
        {

            Period period = new Period(new DateTime(2022, 4, 27), EPeriod.PM);
            Assert.True(semesterSchedule.AddLesson(TestsSchoolMatter, period));

            Assert.Equal(TestsSchoolMatter, semesterSchedule.GetLesson(period));

            Assert.True(semesterSchedule.RemoveLesson(period));

            Assert.Null(semesterSchedule.GetLesson(period));
        }

        [Fact]
        public void ImpossibleToAddLessonOnHoliday()
        {
            Assert.Throws<AgendaException>(() =>
            {
                Assert.True(semesterSchedule.AddLesson(TestsSchoolMatter, new Period(new DateTime(2022, 7, 14), EPeriod.AM)));
            });
        }

        [Fact]
        public void GlobalSchedule()
        {
            SetupOneSchoolMatterWithTwoLessons();
            Assert.Equal(2, semesterSchedule.GetLessons(new Student("X", "Y", new MailAddress("x@y.com"), "ABCD", Cnam2APromotion)).Count);
        }

        [Fact]
        public void MonthScheduleShouldOnlyShowTheCurrentMonth()
        {
            SetupOneSchoolMatterWithTwoLessons();
            Assert.Single(semesterSchedule.GetLessons(new Student("X", "Y", new MailAddress("x@y.com"), "ABCD", Cnam2APromotion), new Month(4, 2022)));
        }

        [Fact]
        public void WeekScheduleShouldOnlyShowTheCurrentWeek()
        {
            SetupOneSchoolMatterWithTwoLessons();
            Assert.Single(semesterSchedule.GetLessons(new Student("X", "Y", new MailAddress("x@y.com"), "ABCD", Cnam2APromotion), new Week(17, 2022)));
        }

        [Fact]
        public void DayScheduleShouldOnlyShowTheCurrentDay()
        {
            SetupOneSchoolMatterWithTwoLessons();
            Assert.Single(semesterSchedule.GetLessons(new Student("X", "Y", new MailAddress("x@y.com"), "ABCD", Cnam2APromotion), new Day(new DateTime(2022, 4, 27))));
        }


        [Fact]
        public void MonthScheduleForTeacherShouldOnlyShowTheCurrentMonth()
        {
            SetupOneSchoolMatterWithTwoLessons();
            Period period = new Period(new DateTime(2022, 4, 27), EPeriod.AM);
            semesterSchedule.AddLesson(SecuritySchoolMatter, period);

            Assert.Single(semesterSchedule.GetLessons(Edouard, new Month(4, 2022)));
        }

        [Fact]
        public void WeekScheduleForTeacherShouldOnlyShowTheCurrentWeek()
        {
            SetupOneSchoolMatterWithTwoLessons();
            Period period = new Period(new DateTime(2022, 4, 27), EPeriod.AM);
            semesterSchedule.AddLesson(SecuritySchoolMatter, period);


            Assert.Single(semesterSchedule.GetLessons(Edouard, new Week(17, 2022)));
        }

        [Fact]
        public void DayScheduleForTeacherShouldOnlyShowTheCurrentDay()
        {
            SetupOneSchoolMatterWithTwoLessons();
            Period period = new Period(new DateTime(2022, 4, 27), EPeriod.AM);
            semesterSchedule.AddLesson(SecuritySchoolMatter, period);


            Assert.Single(semesterSchedule.GetLessons(Edouard, new Day(new DateTime(2022, 4, 27))));
        }

        [Fact]
        public void GenerateScheduleBySchoolMatter()
        {
            semesterSchedule.RegisterSchoolMatterByBlock();

            Assert.Equal(NUMBER_OF_TIME_SLOTS, semesterSchedule.GetLessonsBySchoolMatter(SecuritySchoolMatter).Count);
            Assert.Equal(NUMBER_OF_TIME_SLOTS, semesterSchedule.GetLessonsBySchoolMatter(TestsSchoolMatter).Count);

            semesterSchedule.RegisterSchoolMatterByBlock();

            Assert.Equal(NUMBER_OF_TIME_SLOTS, semesterSchedule.GetLessonsBySchoolMatter(SecuritySchoolMatter).Count);
            Assert.Equal(NUMBER_OF_TIME_SLOTS, semesterSchedule.GetLessonsBySchoolMatter(TestsSchoolMatter).Count);
        }

        [Fact]
        public void NotEnoughSlotsToGenerateScheduleBySchoolMatter()
        {

            Teacher MrX = new Teacher("XXX", "YYY", new MailAddress("XXX@YYY.com"), "ABCD");
            SchoolMatter bigSchoolMatter = new SchoolMatter("XXXX", MrX);

            Dictionary<SchoolMatter, int> schoolMattersNumberOfSlot = new Dictionary<SchoolMatter, int>();
            const int LOT_OF_SLOTS = 10000;
            schoolMattersNumberOfSlot.Add(bigSchoolMatter, LOT_OF_SLOTS);

            semesterSchedule.SetLessonsDuration(schoolMattersNumberOfSlot);


            Assert.Throws<AgendaException>(() =>
            {
                semesterSchedule.RegisterSchoolMatterByBlock();
            });
        }

        [Fact]
        public void PeriodicallyPlacedLessonShouldRespectTheRule()
        {
            Teacher MsKraus = new Teacher("G�raldine", "Krauss", new MailAddress("G�raldine@Krauss.com"), "ABCD");
            SchoolMatter EnglishSchoolMatter = new SchoolMatter("English", MsKraus);

            Dictionary<SchoolMatter, int> schoolMattersNumberOfSlot = new Dictionary<SchoolMatter, int>();
            const int NB_LESSONS_TO_PLACE = 4;
            schoolMattersNumberOfSlot.Add(EnglishSchoolMatter, NB_LESSONS_TO_PLACE);

            semesterSchedule.SetLessonsDuration(schoolMattersNumberOfSlot);

            const DayOfWeek dayToAddLesson = DayOfWeek.Tuesday;
            const EPeriod periodToAddLesson = EPeriod.PM;
            semesterSchedule.PeriodicFillByDate(dayToAddLesson, periodToAddLesson, EnglishSchoolMatter);

            Dictionary<Period, SchoolMatter> englishLessons = semesterSchedule.GetLessonsBySchoolMatter(EnglishSchoolMatter);
            Assert.Equal(NB_LESSONS_TO_PLACE, englishLessons.Count);

            foreach (KeyValuePair<Period, SchoolMatter> englishLesson in englishLessons)
            {
                Assert.Equal(dayToAddLesson, englishLesson.Key.Date.DayOfWeek);
                Assert.Equal(periodToAddLesson, englishLesson.Key.EPeriod);
            }
        }

        [Fact]
        public void PeriodicallyPlacedLessonShouldRespectTheRuleThenFillByBlock()
        {
            Teacher MsKraus = new Teacher("G�raldine", "Krauss", new MailAddress("G�raldine@Krauss.com"), "ABCD");
            SchoolMatter EnglishSchoolMatter = new SchoolMatter("English", MsKraus);

            Dictionary<SchoolMatter, int> schoolMattersNumberOfSlot = new Dictionary<SchoolMatter, int>();
            const int NB_LESSONS_TO_PLACE = 30;
            schoolMattersNumberOfSlot.Add(EnglishSchoolMatter, NB_LESSONS_TO_PLACE);

            semesterSchedule.SetLessonsDuration(schoolMattersNumberOfSlot);

            const DayOfWeek dayToAddLesson = DayOfWeek.Tuesday;
            const EPeriod periodToAddLesson = EPeriod.PM;
            semesterSchedule.PeriodicFillByDate(dayToAddLesson, periodToAddLesson, EnglishSchoolMatter, true);

            Dictionary<Period, SchoolMatter> englishLessons = semesterSchedule.GetLessonsBySchoolMatter(EnglishSchoolMatter);
            Assert.Equal(NB_LESSONS_TO_PLACE, englishLessons.Count);
        }

        [Fact]
        public void PeriodicalScheduleGeneration()
        {
            Dictionary<SchoolMatter, int> schoolMattersNumberOfSlot = new Dictionary<SchoolMatter, int>();

            const int NB_TIME_SLOTS_TO_ADD = 4;

            schoolMattersNumberOfSlot.Add(TestsSchoolMatter, NB_TIME_SLOTS_TO_ADD);
            schoolMattersNumberOfSlot.Add(SecuritySchoolMatter, NB_TIME_SLOTS_TO_ADD);

            semesterSchedule.SetLessonsDuration(schoolMattersNumberOfSlot);

            semesterSchedule.PeriodicScheduleGeneration();

            Dictionary<Period, SchoolMatter> testTimeSlots = semesterSchedule.GetLessonsBySchoolMatter(TestsSchoolMatter);
            Dictionary<Period, SchoolMatter> securityTimeSlots = semesterSchedule.GetLessonsBySchoolMatter(SecuritySchoolMatter);

            Assert.Equal(NB_TIME_SLOTS_TO_ADD, testTimeSlots.Count);
            Assert.Equal(NB_TIME_SLOTS_TO_ADD, securityTimeSlots.Count);

            Assert.All(testTimeSlots, testTimeSlot => Assert.True(testTimeSlot.Key.EPeriod == EPeriod.AM && testTimeSlot.Key.Date.DayOfWeek == DayOfWeek.Monday));
            Assert.All(securityTimeSlots, securityTimeSlot => Assert.True(securityTimeSlot.Key.EPeriod == EPeriod.PM && securityTimeSlot.Key.Date.DayOfWeek == DayOfWeek.Monday));
        }

        [Fact]
        public void PeriodicalScheduleGenerationWithBigNumbersOfSlots()
        {
            Dictionary<SchoolMatter, int> schoolMattersNumberOfSlot = new Dictionary<SchoolMatter, int>();

            const int NB_TIME_SLOTS_TO_ADD = 30;

            schoolMattersNumberOfSlot.Add(TestsSchoolMatter, NB_TIME_SLOTS_TO_ADD);
            schoolMattersNumberOfSlot.Add(SecuritySchoolMatter, NB_TIME_SLOTS_TO_ADD);

            semesterSchedule.SetLessonsDuration(schoolMattersNumberOfSlot);

            semesterSchedule.PeriodicScheduleGeneration();

            Dictionary<Period, SchoolMatter> testTimeSlots = semesterSchedule.GetLessonsBySchoolMatter(TestsSchoolMatter);
            Dictionary<Period, SchoolMatter> securityTimeSlots = semesterSchedule.GetLessonsBySchoolMatter(SecuritySchoolMatter);

            Assert.Equal(NB_TIME_SLOTS_TO_ADD, testTimeSlots.Count);
            Assert.Equal(NB_TIME_SLOTS_TO_ADD, securityTimeSlots.Count);
        }

        [Fact]
        public void PeriodicalScheduleGenerationWithTooMuchLessons()
        {
            Dictionary<SchoolMatter, int> schoolMattersNumberOfSlot = new Dictionary<SchoolMatter, int>();

            const int NB_TIME_SLOTS_TO_ADD = 3000000;

            schoolMattersNumberOfSlot.Add(TestsSchoolMatter, NB_TIME_SLOTS_TO_ADD);

            semesterSchedule.SetLessonsDuration(schoolMattersNumberOfSlot);

            Assert.Throws<AgendaException>(() =>
            {
                semesterSchedule.PeriodicScheduleGeneration();
            });
        }

        [Fact]
        public void ImpossibleToAddLessonOnUnavailableTimeSlot()
        {
            Period period = new Period(new DateTime(2022, 1, 5), EPeriod.AM);
            List<Period> unavailability = new List<Period>()
            {
                period
            };

            Teacher teacher = new Teacher("XXX", "YYY", new MailAddress("XXX@YYY.com"), "ABCD", unavailability);
            SchoolMatter schoolMatter = new SchoolMatter("YYY", teacher);

            Dictionary<SchoolMatter, int> schoolMattersNumberOfSlot = new Dictionary<SchoolMatter, int>();
            schoolMattersNumberOfSlot.Add(schoolMatter, 2);

            semesterSchedule.SetLessonsDuration(schoolMattersNumberOfSlot);

            Assert.False(semesterSchedule.AddLesson(schoolMatter, period));
        }

        [Fact]
        public void SwapLessons()
        {
            Period p1 = new Period(new DateTime(2022, 1, 5), EPeriod.AM);
            Period p2 = new Period(new DateTime(2022, 1, 5), EPeriod.PM);

            semesterSchedule.AddLesson(SecuritySchoolMatter, p1);
            semesterSchedule.AddLesson(TestsSchoolMatter, p2);

            semesterSchedule.SwapTwoLessons(p1, p2);

            Assert.Equal(SecuritySchoolMatter, semesterSchedule.GetLessonsTimeSlots()[p2]);
            Assert.Equal(TestsSchoolMatter, semesterSchedule.GetLessonsTimeSlots()[p1]);
        }

        [Fact]
        public void ScheduleToHTML()
        {
            semesterSchedule.EntrepriseWeeks = new List<int>
            {
                2, 4, 6, 8, 10, 12, 14, 16, 18, 19, 22, 24, 26
            };
            semesterSchedule.PeriodicScheduleGeneration();
            Assert.True(SchedulePDFGenerator.ScheduleHTMLToPDF(semesterSchedule));
        }

        [Fact(Skip = "sample to write test")]
        public void XXX()
        {

        }
    }
}
