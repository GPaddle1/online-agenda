import { state } from '@angular/animations';
import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { User } from 'shared/models/user';
import { AddUser, RemoveUser } from '../actions/user-action';
import { UserStateModel } from './user-state-model';
@State<UserStateModel>({
  name: 'user',
  defaults: {
    user: null!,
  },
})
@Injectable()
export class UserState {

  @Selector()
  static getUser(state: UserStateModel) {
    return state.user;
  }
  
  @Action(AddUser)
  addUser(
    { getState, patchState }: StateContext<UserStateModel>,
    { payload }: AddUser
  ) {
    const state = getState();
    patchState({
      user: payload,
    });
  }

  @Action(RemoveUser)
  removeUser(
    { getState, patchState }: StateContext<UserStateModel>
  ) {
    const state = getState();
    patchState({
      user: null!,
    });
  }
}
