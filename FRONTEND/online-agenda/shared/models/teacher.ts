import { Guid } from "guid-typescript";
import { Period } from "./period";
import { User } from "./user";

export class Teacher extends User {

	unavailability!: Period[]

	constructor(
        id: Guid,
		lastname: string,
		firstname: string,
        email: string,
        phone: string,
		password: string,
		unavailability: Period[]
	) {
        super(id,lastname, firstname, email, phone, password);
		this.unavailability = unavailability;
	}

	static fromJSON(item: any): Teacher {
		return new Teacher(
            item['id'],
			item['lastname'],
			item['firstname'],
			item['email'],
			item['phone'],
			item['password'],
			item['unavailability'],
		);
	}
}