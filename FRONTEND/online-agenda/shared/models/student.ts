import { Guid } from "guid-typescript";
import { Promotion } from "./promotion";
import { User } from "./user";

export class Student extends User {

    promotion: Promotion;

	constructor(
        id: Guid,
		lastname: string,
		firstname: string,
        email: string,
        phone: string,
		password: string,
        promotion: Promotion
	) {
        super(id,lastname, firstname, email, phone, password);
		this.promotion = promotion;
	}

	static fromJSON(item: any): Student {
		return new Student(
            item['id'],
			item['lastname'],
			item['firstname'],
			item['email'],
			item['phone'],
			item['password'],
			item['promotion']
		);
	}
}