import { Guid } from "guid-typescript";
import { Period } from "./period";
import { SchoolMatter } from "./schoolMatter";

export class LessonTimeSlots {

	period: Period;
	schoolMatter: SchoolMatter;
	id: Guid;

	constructor(
		period: Period,
		schoolMatter: SchoolMatter,
		id: Guid
	) {
		this.period = period;
		this.schoolMatter = schoolMatter;
		this.id = id;
	}

	static fromJSON(item: any): LessonTimeSlots {
		return new LessonTimeSlots(
			item['period'],
			item['schoolMatter'],
			item['id'],
		);
	}
}