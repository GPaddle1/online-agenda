import { Guid } from "guid-typescript";
import { Period } from "./period";
import { Teacher } from "./teacher";

export class HeadTeacher extends Teacher {

	constructor(
        id: Guid,
		lastname: string,
		firstname: string,
        email: string,
        phone: string,
		password: string,
		unavailability: Period[]
	) {
        super(id,lastname, firstname, email, phone, password, unavailability);
	}

	static fromJSON(item: any): HeadTeacher {
		return new HeadTeacher(
            item['id'],
			item['lastname'],
			item['firstname'],
			item['email'],
			item['phone'],
			item['password'],
			item['unavailability'],
		);
	}
}