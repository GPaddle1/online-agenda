import { SchoolMatter } from "./schoolMatter";

export class SchoolMatterDetailsNumberOfSlot {

	schoolMatter: SchoolMatter;
    numberOfSlots: number;

	constructor(
		schoolMatter: SchoolMatter,        
		numberOfSlots: number,
	) {
		this.schoolMatter = schoolMatter;
		this.numberOfSlots = numberOfSlots;
	}

	static fromJSON(item: any): SchoolMatterDetailsNumberOfSlot {
		return new SchoolMatterDetailsNumberOfSlot(
			item['schoolMatter'],
			item['numberOfSlots'],
		);
	}
}