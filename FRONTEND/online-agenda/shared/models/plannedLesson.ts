import { Guid } from "guid-typescript";

export class PlannedLesson {

    id: Guid;
	lesson_id: number;
	period: number;
	lessonDate: number;
	promotion_id: number;

	constructor(
        id: Guid,
		lesson_id: number,
		period: number,
		lessonDate: number,
		promotion_id : number
	) {
		this.id = id;
		this.lesson_id = lesson_id;
		this.period = period;
		this.lessonDate = lessonDate;
		this.promotion_id = promotion_id;
	}

	static fromJSON(item: any): PlannedLesson {
		return new PlannedLesson(
            item['id'],
			item['lesson_id'],
			item['period'],
			item['lessonDate'],
			item['promotion_id']
		);
	}
}