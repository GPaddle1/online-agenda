import { Guid } from "guid-typescript";

export class User {

	id: Guid;
	lastname: string;
	firstname: string;
    email: string;
	phone: string;
	password!: string

	constructor(
		id: Guid,
		lastname: string,
		firstname: string,
        email: string,
        phone: string,
		password: string
	) {
		this.id = id;
		this.lastname = lastname;
		this.firstname = firstname;
		this.email = email;
		this.phone = phone;
		this.password = password;
	}

	static fromJSON(item: any): User {
		return new User(
			item['id'],
			item['lastname'],
			item['firstname'],
			item['email'],
			item['phone'],
			item['password'],
		);
	}
}