//import * as internal from "stream";
import { Guid } from "guid-typescript";
import { Teacher } from "./teacher";

export class SchoolMatter {

    id: Guid;
	name: string;
	numberOfLessons: number;
	teacher: Teacher;

	constructor(
        id: Guid,
		name: string,
		numberOfLessons: number,
		teacher: Teacher,
	) {
		this.id = id;
		this.name = name;
		this.numberOfLessons = numberOfLessons;
		this.teacher = teacher;
	}

	static fromJSON(item: any): SchoolMatter {
		return new SchoolMatter(
            item['id'],
			item['name'],
			item['numberOfLessons'],
			item['teacher'],
		);
	}
}