import { Guid } from "guid-typescript";
import { LessonTimeSlots } from "./lessontimeslots";
import { Period } from "./period";
import { Promotion } from "./promotion";
import { SchoolMatter } from "./schoolMatter";
import { SchoolMatterDetailsNumberOfSlot } from "./schoolmatterdetailsnumberofslots";

export class SemesterSchedule {

    id: Guid;
    title: string;
    promotion: Promotion;
    lessonTimeSlots: LessonTimeSlots[];
    holidays!: Date[];
    enterpriseWeeks!: number[];
    schoolMatterDetailsNumberOfSlot!: SchoolMatterDetailsNumberOfSlot[];

	constructor(
        id: Guid,
        title: string,
        promotion: Promotion,
        lessonTimeSlots: LessonTimeSlots[],
        holidays: Date[],
        enterpriseWeeks: number[],
        schoolMatterDetailsNumberOfSlot: SchoolMatterDetailsNumberOfSlot[],
	) {
        this.id = id;
        this.title = title;
		this.promotion = promotion;
        this.lessonTimeSlots = lessonTimeSlots;
        this.holidays = holidays;
        this.enterpriseWeeks = enterpriseWeeks;
        this.schoolMatterDetailsNumberOfSlot = schoolMatterDetailsNumberOfSlot;
	}

	static fromJSON(item: any): SemesterSchedule {
		return new SemesterSchedule(
            item['id'],
            item['title'],
            item['promotion'],
            item['lessonTimeSlots'],
            item['holidays'],
            item['enterpriseWeeks'],
            item['schoolMatterDetailsNumberOfSlot']
		);
	}
}