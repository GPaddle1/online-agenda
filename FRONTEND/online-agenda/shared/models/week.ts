export class Week {

    targetWeekNumber: number;
    targetYear: number;

	constructor(
        targetWeekNumber: number,
        targetYear: number
	) {
		this.targetWeekNumber = targetWeekNumber;
        this.targetYear = targetYear;
	}

	static fromJSON(item: any): Week {
		return new Week(
            item['targetWeekNumber'],
            item['targetYear']
		);
	}
}