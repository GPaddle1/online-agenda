import { Day } from "./day";
import { EPeriod } from "./eperiod";

export class TimeSlot {

    dayOfWeek: Day;
    ePeriodValue: EPeriod;

	constructor(
        dayOfWeek: Day,
        ePeriodValue: EPeriod
	) {
		this.dayOfWeek = dayOfWeek;
        this.ePeriodValue = ePeriodValue;
	}

	static fromJSON(item: any): TimeSlot {
		return new TimeSlot(
            item['dayOfWeek'],
            item['ePeriodValue']
		);
	}
}