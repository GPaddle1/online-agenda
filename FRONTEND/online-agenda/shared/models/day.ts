export class Day {

    targetDateTime: Date;

	constructor(
        targetDateTime: Date
	) {
		this.targetDateTime = targetDateTime;
	}

	static fromJSON(item: any): Day {
		return new Day(
            item['targetDateTime']
		);
	}
}