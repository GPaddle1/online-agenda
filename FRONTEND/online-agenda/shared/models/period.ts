import { EPeriod } from "./eperiod";

export class Period {

    date: Date;
    ePeriod: EPeriod;

	constructor(
        date: Date,
        ePeriod: EPeriod
	) {
		this.date = date;
        this.ePeriod = ePeriod;
	}

	static fromJSON(item: any): Period {
		return new Period(
            item['date'],
            item['ePeriod']
		);
	}
}