import { Guid } from "guid-typescript";

export class Promotion {

    id!: Guid;
    title!: string;

	constructor(
        id: Guid,
        title: string
	) {
		this.id = id;
        this.title = title;
	}

	static fromJSON(item: any): Promotion {
		return new Promotion(
            item['id'],
            item['title']
		);
	}
}