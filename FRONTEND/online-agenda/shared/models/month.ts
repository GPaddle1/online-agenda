export class Month {

    targetMonth: number;
    targetYear: number;

	constructor(
        targetMonth: number,
        targetYear: number
	) {
		this.targetMonth = targetMonth;
        this.targetYear = targetYear;
	}

	static fromJSON(item: any): Month {
		return new Month(
            item['targetMonth'],
            item['targetYear']
		);
	}
}