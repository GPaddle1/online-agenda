import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { User } from 'shared/models/user';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {

  signinForm!: FormGroup;
  submitted: boolean = false;

  subscription !: Subscription;
  user$!: Observable<User>;
  
  errorMsg : String = '';
  successMsg : String = '';

  constructor(private formBuilder: FormBuilder, private authService: AuthService,) { }

  ngOnInit(): void {
    this.signinForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]]
    });
  }

  get f() { return this.signinForm.controls; }

  onSubmit() {

    this.submitted = true;

    if (this.signinForm.invalid) {
        return;
    }
    let tmpUser$!: Observable<User>;
    console.log("login")
    tmpUser$ = this.authService.connect(this.signinForm.value.email, this.signinForm.value.password);
    tmpUser$.subscribe(
      event => {
        this.successMsg = 'Succesfully connected'
        this.errorMsg = '';
        this.user$ = this.authService.getLogin(event.id)
      },
      error => {
        this.successMsg= '';
        this.errorMsg = 'Failed to connect';
      }
      );

  }

}
