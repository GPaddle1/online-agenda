import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Guid } from 'guid-typescript';
import { Observable } from 'rxjs';
import { User } from 'shared/models/user';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  endpointApiLogin = "/User/login";
  endpointApiUser = "/User/";


  constructor(private httpClient : HttpClient) { }

  public connect(email : String, password : String): Observable<User> {
    let data : String;
    let httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
      data = "{\"email\": \""+email+"\",\"password\": \""+password+"\"}"
      console.log(data)
      return this.httpClient.post<User>(environment.baseApiUrl + this.endpointApiLogin, data, httpOptions);
  }

  public getLogin(id : Guid) : Observable<User> {
    return this.httpClient.get<User>(environment.baseApiUrl + this.endpointApiUser + id);
  } 
}
