import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SemesterSchedule } from 'shared/models/semesterSchedule';
import { CalendarService } from '../calendar.service';

@Component({
  selector: 'app-list-holidays',
  templateUrl: './list-holidays.component.html',
  styleUrls: ['./list-holidays.component.scss']
})
export class ListHolidaysComponent implements OnInit {

  constructor(private route: ActivatedRoute, private calendarService: CalendarService) { }

  semesterSchedule$!: Observable<SemesterSchedule>;
  observerStudents: any;

  ngOnInit(): void {
    this.semesterSchedule$ = this.calendarService.getSemesterSchedules().pipe(
      map(semesterSchedules => semesterSchedules.filter(semesterSchedule => semesterSchedule.id == this.route.snapshot.params.id)[0] )
    );
  }

  onDelete(): void {

  }

}
