import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { Observable } from 'rxjs';
import { EPeriod } from 'shared/models/eperiod';
import { LessonTimeSlots } from 'shared/models/lessontimeslots';
import { Promotion } from 'shared/models/promotion';
import { SemesterSchedule } from 'shared/models/semesterSchedule';
import { CalendarService } from '../calendar.service';

@Component({
  selector: 'app-month-calendar',
  templateUrl: './month-calendar.component.html',
  styleUrls: ['./month-calendar.component.scss']
})
export class MonthCalendarComponent implements OnInit {

  @Input() dateFormat : string = '';
  @Input() promotion! : Promotion;
  @Input() semesterNumber : string = '';
  date : Date = new Date();
  numberOfWeeksInMonth: number = 0;
  firstDayOfMonthWeeks: Date[] = [];

  semesterSchedule$!: Observable<SemesterSchedule>;
  observerSemesterSchedule: any;
  lessonTimeSlots$!: Observable<Array<LessonTimeSlots>>;
  observerLessonTimeSlots: any;
  ePeriod = EPeriod;
  
  constructor(private calendarService: CalendarService) { }

  ngOnInit(): void {
    this.date = new Date(this.dateFormat)
    this.numberOfWeeksInMonth = this.countWeeksFromCurrentMonth()
    this.firstDayOfMonthWeeks = [];
    for(let i=0; i<this.numberOfWeeksInMonth; i++){
      this.firstDayOfMonthWeeks.push(new Date(this.date.getFullYear(), this.date.getMonth(), 1+(i*7)))
    }
    console.log(this.firstDayOfMonthWeeks)
  }

  ngOnChanges(changes: SimpleChanges) {
    this.date = new Date(this.dateFormat)
    this.numberOfWeeksInMonth = this.countWeeksFromCurrentMonth()
    this.firstDayOfMonthWeeks = [];
    for(let i=0; i<this.numberOfWeeksInMonth; i++){
      this.firstDayOfMonthWeeks.push(new Date(this.date.getFullYear(), this.date.getMonth(), 1+(i*7)))
    }
    console.log(this.firstDayOfMonthWeeks)
  }

  countWeeksFromCurrentMonth(): number {

    // month_number is in the range 1..12
    console.log(this.date.getFullYear())
    console.log(this.date.getMonth())
    var firstOfMonth = new Date(this.date.getFullYear(), this.date.getMonth()-1, 1);
    var lastOfMonth = new Date(this.date.getFullYear(), this.date.getMonth(), 0);

    var used = firstOfMonth.getDay() + 6 + lastOfMonth.getDate();

    return Math.ceil( used / 7);
}

}
