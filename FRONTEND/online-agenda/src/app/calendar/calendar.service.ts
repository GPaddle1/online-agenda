import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Guid } from 'guid-typescript';
import { Observable } from 'rxjs';
import { LessonTimeSlots } from 'shared/models/lessontimeslots';
import { Promotion } from 'shared/models/promotion';
import { SchoolMatter } from 'shared/models/schoolMatter';
import { SemesterSchedule } from 'shared/models/semesterSchedule';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CalendarService {

  endpointApiSemesterSchedule = "/SemesterSchedule/";
  endpointApiLessonSemesterSchedule = "/Lesson/semesterSchedule/";
  endpointApiDowloadPdf = "/PDF/semesterSchedule/";
  endpointApiGenerateBlock = "/Generation/block/";
  endpointApiGenerateByMoment = "/Generation/periodic/byMoment";
  endpointApiGenerateFull= "/Generation/periodic/full";

  constructor(private httpClient : HttpClient) { }

  public getSemesterSchedules() : Observable<Array<SemesterSchedule>> {
    return this.httpClient.get<Array<SemesterSchedule>>(environment.semesterScheduleUrl);
  }

  public getDownloadPdf(semesterScheduleId: Guid) : Observable<any> {
    return this.httpClient.get<any>(environment.semesterScheduleUrl + semesterScheduleId);
  }
  
  public createSemesterSchedule(promotion: Promotion, semesterNumber: string, startDate: Date, endDate: Date): Observable<SemesterSchedule> {
    let data : String;
    let httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
      data = "{\"promotionGuid\": \""+promotion.id+"\",\"startDate\": \""+new Date(startDate).toISOString()+"\",\"endDate\": \""+new Date(endDate).toISOString()+"\",\"semesterScheduleName\": \""+semesterNumber+"\"}"
      return this.httpClient.post<SemesterSchedule>(environment.baseApiUrl + this.endpointApiSemesterSchedule, data, httpOptions);
  }

  public createLessonSemesterSchedule(semesterSchedule: SemesterSchedule, lessonTimeSlots: LessonTimeSlots): Observable<SemesterSchedule> {
    let data : String;
    let httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
      data = semesterSchedule.id + "/"
      lessonTimeSlots.period.date + "/"
      lessonTimeSlots.period + "/";
      return this.httpClient.post<SemesterSchedule>(environment.baseApiUrl + this.endpointApiLessonSemesterSchedule + data, httpOptions);
  }

  public generateBlock(semesterSchedule: SemesterSchedule): Observable<SemesterSchedule> {
    let data : String;
    let httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
      data = "semesterSchedule=" + semesterSchedule.id;
      return this.httpClient.post<SemesterSchedule>(environment.baseApiUrl + this.endpointApiGenerateBlock, data, httpOptions);
  }

  public generateByMoment(semesterSchedule: SemesterSchedule): Observable<SemesterSchedule> {
    let data : String;
    let httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
      data = "semesterSchedule=" + semesterSchedule.id;
      return this.httpClient.post<SemesterSchedule>(environment.baseApiUrl + this.endpointApiGenerateByMoment, data, httpOptions);
  }

  public generateFull(semesterSchedule: SemesterSchedule): Observable<SemesterSchedule> {
    let data : String;
    let httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
      data = "semesterSchedule=" + semesterSchedule.id;
      return this.httpClient.post<SemesterSchedule>(environment.baseApiUrl + this.endpointApiGenerateFull, data, httpOptions);
  }

  public updateSemesterSchedule(semesterSchedule : SemesterSchedule): Observable<SemesterSchedule> {
    let data : String;
    let httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
      data = "id=" + semesterSchedule.id +
      "&promotion=" + semesterSchedule.promotion.id + 
      "&lessontimeslots=" + semesterSchedule.lessonTimeSlots + 
      "&holidays=" + semesterSchedule.holidays + 
      "&enterpriseWeeks=" + semesterSchedule.enterpriseWeeks + 
      "&SchoolMatterDetailsNumberOfSlot=" + semesterSchedule.schoolMatterDetailsNumberOfSlot;
      return this.httpClient.put<SemesterSchedule>(environment.baseApiUrl + this.endpointApiSemesterSchedule, data, httpOptions);
  }

  public deleteSemesterSchedule(semesterSchedule : SemesterSchedule) {
      return this.httpClient.delete<SemesterSchedule>(environment.baseApiUrl + this.endpointApiSemesterSchedule + semesterSchedule.id);
  }
}
