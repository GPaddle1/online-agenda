import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CalendarService } from '../calendar.service';

@Component({
  selector: 'app-form-holidays',
  templateUrl: './form-holidays.component.html',
  styleUrls: ['./form-holidays.component.scss']
})
export class FormHolidaysComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private calendarService: CalendarService) { }

  holidayForm!: FormGroup;
  submitted: boolean = false;

  ngOnInit(): void {
    this.holidayForm = this.formBuilder.group({
      date: ['', [Validators.required]]
    });
  }

  
  
  get f() { return this.holidayForm.controls; }

  onSubmit() {
    this.submitted = true;

    if (this.holidayForm.invalid) {
        return;
    }
  }

}
