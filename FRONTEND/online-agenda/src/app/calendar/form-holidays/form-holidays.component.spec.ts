import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormHolidaysComponent } from './form-holidays.component';

describe('FormHolidaysComponent', () => {
  let component: FormHolidaysComponent;
  let fixture: ComponentFixture<FormHolidaysComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormHolidaysComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormHolidaysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
