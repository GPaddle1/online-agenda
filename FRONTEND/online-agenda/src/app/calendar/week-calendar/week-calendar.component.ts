import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { EPeriod } from 'shared/models/eperiod';
import { LessonTimeSlots } from 'shared/models/lessontimeslots';
import { Promotion } from 'shared/models/promotion';
import { SemesterSchedule } from 'shared/models/semesterSchedule';
import { CalendarService } from '../calendar.service';

@Component({
  selector: 'app-week-calendar',
  templateUrl: './week-calendar.component.html',
  styleUrls: ['./week-calendar.component.scss']
})
export class WeekCalendarComponent implements OnInit {

  @Input() dateFormat : string = '';
  @Input() promotion! : Promotion;
  @Input() semesterNumber : string = '';
  date : Date = new Date();

  semesterSchedule$!: Observable<SemesterSchedule>;
  observerSemesterSchedule: any;
  lessonTimeSlots$!: Observable<Array<LessonTimeSlots>>;
  observerLessonTimeSlots: any;
  ePeriod = EPeriod;

  weekNumber!: number;
  weekStartDate!: Date;  
  weekEndDate!: Date;
  timeSlotsPerDays: Map<Date, LessonTimeSlots[]> = new Map<Date, LessonTimeSlots[]>()

  constructor(private calendarService: CalendarService) { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {
    this.date = new Date(this.dateFormat)
    this.weekStartDate = this.getDateOfISOWeek(this.getWeek(this.date), this.date.getFullYear())
    this.weekEndDate = new Date(this.weekStartDate)
    this.weekEndDate.setDate(this.weekEndDate.getDate()+4)
    this.semesterSchedule$ = this.calendarService.getSemesterSchedules().pipe(
      map(semesterSchedules => semesterSchedules.filter(semesterSchedule => semesterSchedule.promotion.id == this.promotion.id && semesterSchedule.title == this.semesterNumber)[0]    
      )
    );
    this.lessonTimeSlots$ = this.semesterSchedule$.pipe(map(semesterSchedule => semesterSchedule.lessonTimeSlots.filter(lessonTimeSlot => (new Date(lessonTimeSlot.period.date).getTime() >= this.weekStartDate.getTime()) && (new Date(lessonTimeSlot.period.date).getTime() <= this.weekEndDate.getTime()))));
    let currentDate = new Date(this.weekStartDate)
    this.timeSlotsPerDays.clear();
    
    let lessonTimeSlotsByDay = this.lessonTimeSlots$.pipe(map(lessonsTimeSlots => 
      lessonsTimeSlots.filter(lessonTimeSlot => 
        lessonTimeSlot))); //new Date(lessonTimeSlot.period.date).getTime() <= currentDate.getTime()
    lessonTimeSlotsByDay.subscribe((values) => {
      while(currentDate <= this.weekEndDate){
        let lessonsTimeSlots: LessonTimeSlots[] = [];
        values.forEach(value => {

          if(new Date(value.period.date).toLocaleDateString() == new Date(currentDate).toLocaleDateString()){
            lessonsTimeSlots.push(value);
          }
        })
        this.timeSlotsPerDays.set(new Date(currentDate), lessonsTimeSlots);
        currentDate.setDate(currentDate.getDate()+1);
      }
      });
  }

  asIsOrder(a: any, b: any) {
    return 1;
 }

  getDateOfISOWeek(w: number, y: number): Date{
    var simple = new Date(y, 0, 1 + (w - 1) * 7);
    var dow = simple.getDay();
    var ISOweekStart = simple;
    if (dow <= 4)
        ISOweekStart.setDate(simple.getDate() - simple.getDay() + 1);
    else
        ISOweekStart.setDate(simple.getDate() + 8 - simple.getDay());
    return ISOweekStart;
  }

  getWeek (date: Date) : number {
      let dowOffset = 0; //default dowOffset to zero
      var newYear = new Date(date.getFullYear() ,0,1);
      var day = newYear.getDay() - dowOffset; //the day of week the year begins on
      day = (day >= 0 ? day : day + 7);
      var daynum = Math.floor((date.getTime() - newYear.getTime() - 
      (date.getTimezoneOffset()-newYear.getTimezoneOffset())*60000)/86400000) + 1;
      var weeknum;
      //if the year starts before the middle of a week
      if(day < 4) {
          weeknum = Math.floor((daynum+day-1)/7) + 1;
          if(weeknum > 52) {
              let nYear = new Date(date.getFullYear() + 1,0,1);
              let nday = nYear.getDay() - dowOffset;
              nday = nday >= 0 ? nday : nday + 7;
              /*if the next year starts before the middle of
                the week, it is week #1 of that year*/
              weeknum = nday < 4 ? 1 : 53;
          }
      }
      else {
          weeknum = Math.floor((daynum+day-1)/7);
      }
      return weeknum;
  };

  deleteLessonTimeSlot(lessonTimeSlot: LessonTimeSlots): void {

  }

}
