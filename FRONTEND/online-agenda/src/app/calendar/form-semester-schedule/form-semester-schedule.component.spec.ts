import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormSemesterScheduleComponent } from './form-semester-schedule.component';

describe('FormSemesterScheduleComponent', () => {
  let component: FormSemesterScheduleComponent;
  let fixture: ComponentFixture<FormSemesterScheduleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormSemesterScheduleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormSemesterScheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
