import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { Promotion } from 'shared/models/promotion';
import { SemesterSchedule } from 'shared/models/semesterSchedule';
import { PromotionService } from 'src/app/promotion/promotion.service';
import { SchoolMatterService } from 'src/app/school-matter/school-matter.service';
import { CalendarService } from '../calendar.service';

@Component({
  selector: 'app-form-semester-schedule',
  templateUrl: './form-semester-schedule.component.html',
  styleUrls: ['./form-semester-schedule.component.scss']
})
export class FormSemesterScheduleComponent implements OnInit {

  semesterScheduleForm!: FormGroup;
  submitted: boolean = false;
  promotions$!: Observable<Array<Promotion>>;

  errorMsg : String = '';
  successMsg : String = '';

  constructor( private formBuilder: FormBuilder, private promotionService: PromotionService, private calendarService: CalendarService) { }

  ngOnInit(): void {
    this.promotions$ = this.promotionService.getPromotions();

    this.semesterScheduleForm = this.formBuilder.group({
      promotion: ['', [Validators.required]],
      semesterNumber: ['', [Validators.required]],
      startDate: ['', [Validators.required]],
      endDate: ['', [Validators.required]]
    });
  }

  get f() { return this.semesterScheduleForm.controls; }
  
  onSubmitSemester() {
    this.submitted = true;

    if (this.semesterScheduleForm.invalid) {
        return;
    }

    this.calendarService.createSemesterSchedule(this.semesterScheduleForm.value.promotion, this.semesterScheduleForm.value.semesterNumber, this.semesterScheduleForm.value.startDate, this.semesterScheduleForm.value.endDate).subscribe(
      event => {
        this.successMsg = 'Semester schedule succesfully created'
        this.errorMsg = '';
      },
      error => {
        this.successMsg= '';
        this.errorMsg = 'Failed to create Semester schedule';
      }
      );
  }

  onResetSemester() {
    this.submitted = false;
    this.semesterScheduleForm.reset();
  }
}
