import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
import { SemesterSchedule } from 'shared/models/semesterSchedule';
import { CalendarService } from '../calendar.service';

@Component({
  selector: 'app-list-enterprise-weeks',
  templateUrl: './list-enterprise-weeks.component.html',
  styleUrls: ['./list-enterprise-weeks.component.scss']
})
export class ListEnterpriseWeeksComponent implements OnInit {

  constructor(private route: ActivatedRoute, private calendarService: CalendarService) { }

  semesterSchedule$!: Observable<SemesterSchedule>;
  observerStudents: any;

  ngOnInit(): void {
    this.semesterSchedule$ = this.calendarService.getSemesterSchedules().pipe(
      map(semesterSchedules => semesterSchedules.filter(semesterSchedule => semesterSchedule.id == this.route.snapshot.params.id)[0] )
    );
  }

  onDelete(): void {

  }

}
