import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListEnterpriseWeeksComponent } from './list-enterprise-weeks.component';

describe('ListEnterpriseWeeksComponent', () => {
  let component: ListEnterpriseWeeksComponent;
  let fixture: ComponentFixture<ListEnterpriseWeeksComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListEnterpriseWeeksComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListEnterpriseWeeksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
