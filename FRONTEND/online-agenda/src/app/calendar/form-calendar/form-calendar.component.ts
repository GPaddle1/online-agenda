import { HttpClient } from '@angular/common/http';
import { Component, OnInit, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { SchoolMatter } from 'shared/models/schoolMatter';
import { environment } from 'src/environments/environment';
import { SchoolMatterService } from 'src/app/school-matter/school-matter.service';
import { EPeriod } from 'shared/models/eperiod';
import { Promotion } from 'shared/models/promotion';
import { PromotionService } from 'src/app/promotion/promotion.service';
import { CalendarService } from '../calendar.service';
import { LessonTimeSlots } from 'shared/models/lessontimeslots';
import { SemesterSchedule } from 'shared/models/semesterSchedule';
import { Period } from 'shared/models/period';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-form-calendar',
  templateUrl: './form-calendar.component.html',
  styleUrls: ['./form-calendar.component.scss']
})
export class FormCalendarComponent implements OnInit {

  ePeriod = EPeriod;
  calendarForm!: FormGroup;
  submitted: boolean = false;
  lessons$!: Observable<Array<SchoolMatter>>;
  promotions$!: Observable<Array<Promotion>>;
  semesterSchedules$!: Observable<Array<SemesterSchedule>>;
  currentSemesterSchedule$!: Observable<SemesterSchedule>;

  errorMsg : String = '';
  successMsg : String = '';

  constructor(private formBuilder: FormBuilder, private schoolMatterService: SchoolMatterService, private promotionService: PromotionService, private calendarService: CalendarService) { }

  ngOnInit(): void {

    this.lessons$ = this.schoolMatterService.getSchoolMatters();
    this.promotions$ = this.promotionService.getPromotions();
    
    this.calendarForm = this.formBuilder.group({
      lesson: ['', [Validators.required]],
      period: ['', [Validators.required]],
      date: ['', [Validators.required]],
      promotion: ['', [Validators.required]],
      semesterNumber: ['', [Validators.required]]
    });
  }

  
  get f() { return this.calendarForm.controls; }

  onSubmit() {
    this.submitted = true;

    if (this.calendarForm.invalid) {
        return;
    }

    const lesson = new LessonTimeSlots(
      new Period(this.calendarForm.value.date, this.calendarForm.value.period),
      this.calendarForm.value.lesson,
      null!
    );

    this.currentSemesterSchedule$.subscribe((value)=>this.calendarService.createLessonSemesterSchedule(value, lesson).subscribe(
      event => {
        this.successMsg = 'Lesson succesfully created'
        this.errorMsg = '';
      },
      error => {
        this.successMsg= '';
        this.errorMsg = 'Failed to create lesson';
      }
      ));

  }

  onChangesPromotion() {
    this.semesterSchedules$ = this.calendarService.getSemesterSchedules().pipe(
      map(semesterSchedules => semesterSchedules.filter(semesterSchedule => semesterSchedule.promotion.id == this.calendarForm.value.promotion.id))
    );
  }

  onChangesSemesterNumber() {
    this.currentSemesterSchedule$ = this.semesterSchedules$.pipe(
      map(semesterSchedules => semesterSchedules.filter(semesterSchedule => semesterSchedule.title == this.calendarForm.value.semesterNumber)[0])
    );
  }

  onReset() {
    this.submitted = false;
    this.calendarForm.reset();
  }

  onClickGenerationBlock() {
    this.currentSemesterSchedule$.subscribe((value)=>this.calendarService.generateBlock(value));
  }

  onClickGenerationByMoment() {
    this.currentSemesterSchedule$.subscribe((value)=>this.calendarService.generateByMoment(value));
  }

  onClickGenerationFull() {
    this.currentSemesterSchedule$.subscribe((value)=>this.calendarService.generateFull(value));
  }

}
