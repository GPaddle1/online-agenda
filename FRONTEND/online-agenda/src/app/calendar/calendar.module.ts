import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { CalendarComponent } from './calendar/calendar.component';
import { FormCalendarComponent } from './form-calendar/form-calendar.component';
import { Routes, RouterModule } from '@angular/router';
import { MonthCalendarComponent } from './month-calendar/month-calendar.component';
import { WeekCalendarComponent } from './week-calendar/week-calendar.component';
import { DayCalendarComponent } from './day-calendar/day-calendar.component';
import { FormEnterpriseWeeksComponent } from './form-enterprise-weeks/form-enterprise-weeks.component';
import { ListEnterpriseWeeksComponent } from './list-enterprise-weeks/list-enterprise-weeks.component';
import { FormHolidaysComponent } from './form-holidays/form-holidays.component';
import { ListHolidaysComponent } from './list-holidays/list-holidays.component';
import { FormSemesterScheduleComponent } from './form-semester-schedule/form-semester-schedule.component';

const routes: Routes = [
  {
    path: 'calendar',
    component: CalendarComponent,
  },
  {
    path: 'form-calendar',
    component: FormCalendarComponent,
  },
  {
    path: 'form-enterprise-weeks/:id',
    component: FormEnterpriseWeeksComponent,
  },
  {
    path: 'form-holidays/:id',
    component: FormHolidaysComponent,
  }
]

@NgModule({
  declarations: [
    CalendarComponent,
    FormCalendarComponent,
    MonthCalendarComponent,
    WeekCalendarComponent,
    DayCalendarComponent,
    FormEnterpriseWeeksComponent,
    ListEnterpriseWeeksComponent,
    FormHolidaysComponent,
    ListHolidaysComponent,
    FormSemesterScheduleComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forChild(routes),
  ]
})
export class CalendarModule { }
