import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormEnterpriseWeeksComponent } from './form-enterprise-weeks.component';

describe('FormEnterpriseWeeksComponent', () => {
  let component: FormEnterpriseWeeksComponent;
  let fixture: ComponentFixture<FormEnterpriseWeeksComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormEnterpriseWeeksComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormEnterpriseWeeksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
