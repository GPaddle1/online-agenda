import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CalendarService } from '../calendar.service';

@Component({
  selector: 'app-form-enterprise-weeks',
  templateUrl: './form-enterprise-weeks.component.html',
  styleUrls: ['./form-enterprise-weeks.component.scss']
})
export class FormEnterpriseWeeksComponent implements OnInit {

  enterpriseWeekForm!: FormGroup;
  submitted: boolean = false;

  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute, private calendarService: CalendarService) { }

  ngOnInit(): void {
    this.enterpriseWeekForm = this.formBuilder.group({
      enterpriseWeekNumber: ['', [Validators.required, Validators.pattern("^[0-9]+$")]]
    });
  }

  get f() { return this.enterpriseWeekForm.controls; }

  onSubmit(): void {
    this.submitted = true;

    if (this.enterpriseWeekForm.invalid) {
        return;
    }

    //this.calendarService.addEnterpriseWeek(this.route.snapshot.params.id, this.enterpriseWeekForm.value.enterpriseWeekNumber);
  }

}
