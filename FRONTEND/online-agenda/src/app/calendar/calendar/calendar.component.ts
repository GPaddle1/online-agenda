import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ECalendarView } from 'shared/models/ecalendarview';
import { Promotion } from 'shared/models/promotion';
import { SemesterSchedule } from 'shared/models/semesterSchedule';
import { Week } from 'shared/models/week';
import { PromotionService } from 'src/app/promotion/promotion.service';
import { CalendarService } from '../calendar.service';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit {

  eCalendarView = ECalendarView;
  calendarView = this.eCalendarView.Month;
  calendarForm!: FormGroup;
  semesterSchedules$!: Observable<Array<SemesterSchedule>>;
  currentSemesterSchedule$!: Observable<SemesterSchedule>;

  promotions$!: Observable<Array<Promotion>>;

  constructor(private promotionService: PromotionService, private formBuilder: FormBuilder, private calendarService: CalendarService) { }

  ngOnInit(): void {
    this.promotions$ = this.promotionService.getPromotions();

    this.calendarForm = this.formBuilder.group({
      date: [new Date()],
      promotion: [''],
      semesterNumber: [''],
    });
  }

  get f() { return this.calendarForm.controls; }

  onChangesPromotion() {
    this.semesterSchedules$ = this.calendarService.getSemesterSchedules().pipe(
      map(semesterSchedules => semesterSchedules.filter(semesterSchedule => semesterSchedule.promotion.id == this.calendarForm.value.promotion.id))
    );
  }

  onChangesSemesterNumber() {
    this.currentSemesterSchedule$ = this.semesterSchedules$.pipe(
      map(semesterSchedules => semesterSchedules.filter(semesterSchedule => semesterSchedule.title == this.calendarForm.value.semesterNumber)[0])
    );
  }

  onClickMonthView(): void {
    this.calendarView = this.eCalendarView.Month;
  }

  onClickWeekView(): void {
    this.calendarView = this.eCalendarView.Week;    
  }

  onClickDayView(): void {
    this.calendarView = this.eCalendarView.Day;    
  }

  onClickDownload(): void{
    this.currentSemesterSchedule$.subscribe((value)=>this.calendarService.getDownloadPdf(value.id));
  }

}
