import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { Observable, Observer } from 'rxjs';
import { map } from 'rxjs/operators';
import { EPeriod } from 'shared/models/eperiod';
import { LessonTimeSlots } from 'shared/models/lessontimeslots';
import { Promotion } from 'shared/models/promotion';
import { SemesterSchedule } from 'shared/models/semesterSchedule';
import { CalendarService } from '../calendar.service';

@Component({
  selector: 'app-day-calendar',
  templateUrl: './day-calendar.component.html',
  styleUrls: ['./day-calendar.component.scss']
})
export class DayCalendarComponent implements OnInit {

  @Input() dateFormat : string = '';
  @Input() promotion! : Promotion;
  @Input() semesterNumber : string = '';
  date : Date = new Date();

  semesterSchedule$!: Observable<SemesterSchedule>;
  lessonTimeSlots$!: Observable<Array<LessonTimeSlots>>;
  ePeriod = EPeriod;

  semesterSchedule!: SemesterSchedule
  lessonTimeSlots!: Array<LessonTimeSlots>;

  constructor(private calendarService: CalendarService) { }

  ngOnInit(): void {
  }
  

  ngOnChanges(changes: SimpleChanges) {
    this.date = new Date(this.dateFormat);
    this.semesterSchedule$ = this.calendarService.getSemesterSchedules().pipe(
      map(semesterSchedules => semesterSchedules.filter(semesterSchedule => semesterSchedule.promotion.id == this.promotion.id)[0]    
      )
    );
    this.lessonTimeSlots$ = this.semesterSchedule$.pipe(map(semesterSchedule => semesterSchedule.lessonTimeSlots?.filter(lessonTimeSlot => new Date(lessonTimeSlot.period.date).getTime() == this.date.getTime())));
  }

  deleteLessonTimeSlot(lessonTimeSlot: LessonTimeSlots): void {
  }
}
