import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgxsModule } from '@ngxs/store';


import { AppComponent } from './app.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';

import { ApiHttpInterceptor } from './api-http-interceptor';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { RouterModule, Routes } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { UserState } from 'shared/states/user-state';

const appRoutes: Routes = [
  { path: 'users', 
    loadChildren: () => import('./users/users.module').then(m => m.UsersModule) 
  },
  { path: 'promotion', 
    loadChildren: () => import('./promotion/promotion.module').then(m => m.PromotionModule) 
  },
  { path: 'calendar', 
    loadChildren: () => import('./calendar/calendar.module').then(m => m.CalendarModule) 
  },
  { path: 'school-matter', 
    loadChildren: () => import('./school-matter/school-matter.module').then(m => m.SchoolMatterModule) 
  },
  { path: 'auth', 
    loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule) 
  },
  {
    path: '',
    redirectTo: 'calendar/calendar',
    pathMatch: 'full',
  },
  {
    path: '**',
    component: PageNotFoundComponent,
  },
];

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    NavBarComponent,
    PageNotFoundComponent,
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    NgbModule,
    NgxsModule.forRoot([UserState]),
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass:
      ApiHttpInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
