import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Promotion } from 'shared/models/promotion';
import { PromotionService } from '../promotion.service';

@Component({
  selector: 'app-promotion-detail',
  templateUrl: './promotion-detail.component.html',
  styleUrls: ['./promotion-detail.component.scss']
})
export class PromotionDetailComponent implements OnInit {

  constructor(private route: ActivatedRoute, private promotionService: PromotionService) { }

  editMode = false;
  promotion$ !: Observable<Promotion>;

  ngOnInit(): void {
    this.promotion$ = this.promotionService.getPromotions().pipe(
      map(promotions => promotions.filter(promotion => promotion.id == this.route.snapshot.params.id)[0] )
    );
  }
  
  onEnterEditMode(): void {
    this.editMode = true;
  }

  onExitEditMode($event: any): void {
    this.editMode = !this.editMode;
  }

  onDelete(): void {
    this.promotion$.subscribe((promotion => {
            this.promotionService.deletePromotion(promotion);
    }))
  }
}
