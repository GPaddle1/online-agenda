import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Promotion } from 'shared/models/promotion';
import { PromotionService } from '../promotion.service';

@Component({
  selector: 'app-list-promotion',
  templateUrl: './list-promotion.component.html',
  styleUrls: ['./list-promotion.component.scss']
})
export class ListPromotionComponent implements OnInit {

  constructor(private promotionService: PromotionService) { }

  promotions$!: Observable<Array<Promotion>>;
  observerPromotions: any;

  ngOnInit(): void {
    this.promotions$ = this.promotionService.getPromotions();
    
    if (this.observerPromotions) {
      this.observerPromotions.unsubscribe();
    }
    this.observerPromotions = this.promotions$.subscribe();
  }

}
