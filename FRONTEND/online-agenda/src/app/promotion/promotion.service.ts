import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Promotion } from 'shared/models/promotion';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PromotionService {

  endpointApiPromotion = "/Promotion/";

  constructor(private httpClient : HttpClient) { }

  public getPromotions() : Observable<Array<Promotion>> {
    return this.httpClient.get<Array<Promotion>>(environment.promotionUrl);
  }
  
  public createPromotion(promotion : Promotion): Observable<Promotion> {
    let data : String;
    let httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
      data = "{\"title\":\"" + promotion.title + "\"}"
      return this.httpClient.post<Promotion>(environment.baseApiUrl + this.endpointApiPromotion, data, httpOptions);
  }

  public updatePromotion(promotion : Promotion): Observable<Promotion> {
    let data : String;
    let httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
    data = "{\"title\": \""+promotion.title+"\"}"
      return this.httpClient.put<Promotion>(environment.baseApiUrl + this.endpointApiPromotion + promotion.id, data, httpOptions);
  }

  public deletePromotion(promotion : Promotion) {
      return this.httpClient.delete<Promotion>(environment.baseApiUrl + this.endpointApiPromotion + promotion.id);
  }
}
