import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormpromotionComponent } from './form-promotion.component';

describe('FormpromotionComponent', () => {
  let component: FormpromotionComponent;
  let fixture: ComponentFixture<FormpromotionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormpromotionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormpromotionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
