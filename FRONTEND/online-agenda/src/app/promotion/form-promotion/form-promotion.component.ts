import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Promotion } from 'shared/models/promotion';
import { PromotionService } from '../promotion.service';

@Component({
  selector: 'app-form-promotion',
  templateUrl: './form-promotion.component.html',
  styleUrls: ['./form-promotion.component.scss']
})
export class FormpromotionComponent implements OnInit {

  promotionForm!: FormGroup;
  submitted: boolean = false;

  @Input() promotion !: Promotion;
  @Output() updateEvent: EventEmitter<any> = new EventEmitter();

  errorMsg : String = '';
  successMsg : String = '';

  constructor(private formBuilder: FormBuilder, private promotionService: PromotionService) { }

  ngOnInit(): void {
    this.promotionForm = this.formBuilder.group({
      promotionName: [this.promotion?.title, [Validators.required]]
    });
  }

  get f() { return this.promotionForm.controls; }

  onSubmit() {
    this.submitted = true;

    if (this.promotionForm.invalid) {
        return;
    }

    const promotion = new Promotion(
      null!,
      this.promotionForm.value.promotionName
    );

    this.promotionService.createPromotion(promotion).subscribe(
      event => {
        this.successMsg = 'Promotion succesfully created'
        this.errorMsg = '';
      },
      error => {
        this.successMsg= '';
        this.errorMsg = 'Failed to create promotion';
      }
      );
}

  onUpdate() {
    this.submitted = true;

    if (this.promotionForm.invalid) {
      return;
    }

    const promotion = new Promotion(
      this.promotion.id,
      this.promotionForm.value.promotionName
    );

    this.promotionService.updatePromotion(promotion).subscribe(
      event => {
        this.errorMsg = '';
        this.updateEvent.emit(null);
      },
      error => {
        this.successMsg= '';
        this.errorMsg = 'Failed to update promotion';
      }
      );
  }

    onReset() {
      this.submitted = false;
      this.promotionForm.reset();
  }
}
