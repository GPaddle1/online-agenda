import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormpromotionComponent } from './form-promotion/form-promotion.component';
import { ListPromotionComponent } from './list-promotion/list-promotion.component';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { PromotionDetailComponent } from './promotion-detail/promotion-detail.component';

const routes: Routes = [
  {
    path: 'form-promotion',
    component: FormpromotionComponent,
  },
  {
    path: 'list-promotion',
    component: ListPromotionComponent,
  },
  {
    path: ':id',
    component: PromotionDetailComponent,
  },
]



@NgModule({
  declarations: [
    FormpromotionComponent,
    ListPromotionComponent,
    PromotionDetailComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forChild(routes),
  ]
})
export class PromotionModule { }
