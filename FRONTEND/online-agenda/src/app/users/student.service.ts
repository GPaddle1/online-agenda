import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Student } from 'shared/models/student';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StudentService {
  
  endpointApiStudent = "/User/student/";
  endpointApiUser = "/User/";
  endpointApiGetStudent = "/Student/";


  constructor(private httpClient : HttpClient) { }

  public getStudents() : Observable<Array<Student>> {
    return this.httpClient.get<Array<Student>>(environment.studentUrl);
    //return this.httpClient.get<Array<Student>>(environment.baseApiUrl + this.endpointApiGetStudent);intApiGetStudent);
  }
  
  public createStudent(student : Student): Observable<Student> {
    let data : String;
    let httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
    data = "{\"firstName\": \""+student.firstname+"\",\"lastName\": \""+student.lastname+"\",\"email\": \""+student.email+"\",\"password\": \""+student.password+"\",\"promotionGuid\": \""+student.promotion.id+"\"}"
      return this.httpClient.post<Student>(environment.baseApiUrl + this.endpointApiStudent, data, httpOptions);
  }

  public updateStudent(student : Student): Observable<Student> {
    let data : String;
    let httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
    data = "{\"firstName\": \""+student.firstname+"\",\"lastName\": \""+student.lastname+"\",\"email\": \""+student.email+"\",\"password\": \""+student.password+"\",\"promotionGuid\": \""+student.promotion.id+"\"}"
      return this.httpClient.put<Student>(environment.baseApiUrl + this.endpointApiUser + student.id, data, httpOptions);
  }

  public deleteStudent(student : Student) {
      return this.httpClient.delete<Student>(environment.baseApiUrl + this.endpointApiUser + student.id);
  }
  
}
