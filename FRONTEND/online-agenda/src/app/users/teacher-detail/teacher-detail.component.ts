import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ERole } from 'shared/models/erole';
import { Teacher } from 'shared/models/teacher';
import { TeacherService } from '../teacher.service';

@Component({
  selector: 'app-teacher-detail',
  templateUrl: './teacher-detail.component.html',
  styleUrls: ['./teacher-detail.component.scss']
})
export class TeacherDetailComponent implements OnInit {

  constructor(private route: ActivatedRoute, private teacherService: TeacherService) { }

  editMode = false;
  eRole = ERole;
  teacher$ !: Observable<Teacher>;

  ngOnInit(): void {
    this.teacher$ = this.teacherService.getTeachers().pipe(
      map(teachers => teachers.filter(teacher => teacher.id == this.route.snapshot.params.id)[0] )
    );
  }

  onEnterEditMode(): void {
    this.editMode = true;
  }

  onExitEditMode($event: any): void {
    this.editMode = !this.editMode;
  }

  onDelete(): void {
    this.teacher$.subscribe((teacher => {
      this.teacherService.deleteTeacher(teacher);
    }))
  }

}
