import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormUnavailabilityComponent } from './form-unavailability.component';

describe('FormUnavailabilityComponent', () => {
  let component: FormUnavailabilityComponent;
  let fixture: ComponentFixture<FormUnavailabilityComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormUnavailabilityComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormUnavailabilityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
