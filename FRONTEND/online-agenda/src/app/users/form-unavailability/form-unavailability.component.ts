import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { EPeriod } from 'shared/models/eperiod';
import { Period } from 'shared/models/period';
import { Teacher } from 'shared/models/teacher';
import { TeacherService } from '../teacher.service';

@Component({
  selector: 'app-form-unavailability',
  templateUrl: './form-unavailability.component.html',
  styleUrls: ['./form-unavailability.component.scss']
})
export class FormUnavailabilityComponent implements OnInit {

  ePeriod = EPeriod;
  unavailabilityForm!: FormGroup;
  submitted: boolean = false;
  teacher$ !: Observable<Teacher>;

  errorMsg : String = '';
  successMsg : String = '';

  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute, private teacherService: TeacherService) { }

  ngOnInit(): void {

    
    this.teacher$ = this.teacherService.getTeachers().pipe(
      map(teachers => teachers.filter(teacher => teacher.id == this.route.snapshot.params.id)[0] )
    );
    
    this.unavailabilityForm = this.formBuilder.group({
      date: ['', [Validators.required]],
      period: ['', [Validators.required]]
    });
  }

  
  get f() { return this.unavailabilityForm.controls; }

  onSubmit() {
    this.submitted = true;

    if (this.unavailabilityForm.invalid) {
        return;
    }

    let period: Period = new Period(this.unavailabilityForm.value.date, this.unavailabilityForm.value.period);

    this.teacher$.subscribe((teacher => {
      teacher.unavailability.push(new Period(this.unavailabilityForm.value.date, this.unavailabilityForm.value.period));

      this.teacherService.createUnavailability(teacher, period).subscribe(
        event => {
          this.successMsg = 'Unavailability succesfully created'
          this.errorMsg = '';
        },
        error => {
          this.successMsg= '';
          this.errorMsg = 'Failed to create unavailability';
        }
        );
    }));
}

}
