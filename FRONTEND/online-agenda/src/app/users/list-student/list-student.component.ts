import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Student } from 'shared/models/student';
import { StudentService } from '../student.service';

@Component({
  selector: 'app-list-student',
  templateUrl: './list-student.component.html',
  styleUrls: ['./list-student.component.scss']
})
export class ListStudentComponent implements OnInit {

  constructor(private studentService: StudentService) { }

  students$!: Observable<Array<Student>>;
  observerStudents: any;

  ngOnInit(): void {
    this.students$ = this.studentService.getStudents();
    
    if (this.observerStudents) {
      this.observerStudents.unsubscribe();
    }
    this.observerStudents = this.students$.subscribe();
  }

}
