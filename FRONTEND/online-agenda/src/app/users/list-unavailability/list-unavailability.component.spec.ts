import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListUnavailabilityComponent } from './list-unavailability.component';

describe('ListUnavailabilityComponent', () => {
  let component: ListUnavailabilityComponent;
  let fixture: ComponentFixture<ListUnavailabilityComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListUnavailabilityComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListUnavailabilityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
