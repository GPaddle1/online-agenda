import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Period } from 'shared/models/period';
import { Teacher } from 'shared/models/teacher';
import { TeacherService } from '../teacher.service';

@Component({
  selector: 'app-list-unavailability',
  templateUrl: './list-unavailability.component.html',
  styleUrls: ['./list-unavailability.component.scss']
})
export class ListUnavailabilityComponent implements OnInit {

  constructor(private route: ActivatedRoute, private teacherService: TeacherService) { }

  teacher$ !: Observable<Teacher>;

  ngOnInit(): void {
    this.teacher$ = this.teacherService.getTeachers().pipe(
      map(teachers => teachers.filter(teacher => teacher.id == this.route.snapshot.params.id)[0] )
    );
  }

  onDelete(unavailability: Period): void {
    this.teacher$.subscribe((teacher => {
      teacher.unavailability.forEach((period, index) => {
        if(period.date == unavailability.date && period.ePeriod == unavailability.ePeriod){
          teacher.unavailability.splice(index, 1);
        }
      });

      this.teacherService.updateTeacher(teacher);
    }));
  }
}
