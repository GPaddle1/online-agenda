import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { FormUserComponent } from './form-user.component';

describe('FormUserComponent', () => {
  let component: FormUserComponent;
  let fixture: ComponentFixture<FormUserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormUserComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  //nom, prenom, mail, tel, promo, role (eleve/prof) 
  it('should create student', () => {
    fixture = TestBed.createComponent(FormUserComponent);
    component = fixture.componentInstance;
    
    component.signUpForm.value.firstname = "fistname";
    component.signUpForm.value.lastname = "lastname";
    component.signUpForm.value.email = "firstname.lastname@mail.com";
    component.signUpForm.value.phone = "0672667266";
    component.signUpForm.value.promotion = "2020-2023";
    component.signUpForm.value.role = "student";

    let buttonSubmit: HTMLElement = fixture.debugElement.query(By.css('buttonSubmit')).nativeElement;
    buttonSubmit.click()    
    expect(component.submitted).toBeTruthy();
  });

  it('should create teacher', () => {
    fixture = TestBed.createComponent(FormUserComponent);
    component = fixture.componentInstance;
    
    component.signUpForm.value.firstname = "fistname";
    component.signUpForm.value.lastname = "lastname";
    component.signUpForm.value.email = "firstname.lastname@mail.com";
    component.signUpForm.value.phone = "0672667266";
    component.signUpForm.value.role = "teacher";
  });
});
