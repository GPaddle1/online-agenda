import { Component, EventEmitter, inject, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Teacher } from 'shared/models/teacher';
import { Student } from 'shared/models/student';
import { User } from 'shared/models/user';
import { Observable } from 'rxjs';
import { ERole } from 'shared/models/erole';
import { Promotion } from 'shared/models/promotion';
import { SchoolMatter } from 'shared/models/schoolMatter';
import { HeadTeacher } from 'shared/models/headerteacher';
import { PromotionService } from 'src/app/promotion/promotion.service';
import { StudentService } from '../student.service';
import { TeacherService } from '../teacher.service';
import { map } from 'rxjs/operators';
import { waitForAsync } from '@angular/core/testing';

@Component({
  selector: 'app-form-user',
  templateUrl: './form-user.component.html',
  styleUrls: ['./form-user.component.scss']
})
export class FormUserComponent implements OnInit {
  signUpForm!: FormGroup;
  submitted: boolean = false;
  
  @Input() user !: User;
  @Input() role !: ERole;
  @Output() updateEvent: EventEmitter<any> = new EventEmitter();

  eRole = ERole;
  promotions$!: Observable<Array<Promotion>>;
  lessons$!: Observable<Array<SchoolMatter>>;
  
  errorMsg : String = '';
  successMsg : String = '';

  constructor(private formBuilder: FormBuilder, private teacherService: TeacherService, private studentService: StudentService, private promotionService: PromotionService) { }

  ngOnInit() {
    this.promotions$ = this.promotionService.getPromotions(); 

      this.signUpForm = this.formBuilder.group({
        firstname: [this.user?.firstname, [Validators.required, Validators.pattern("^[A-Za-zÀ-ÖØ-öø-ÿ-]+$")]],
        lastname: [this.user?.lastname, [Validators.required, Validators.pattern("^[A-Za-zÀ-ÖØ-öø-ÿ-]+$")]],
        email: [this.user?.email, [Validators.required, Validators.email]],
        phone: [this.user?.phone, [Validators.required, Validators.pattern("^([0-9]{2}){5}$")]],
        role: [this.role == undefined ? '' : this.role, [Validators.required]],
        promotion: [this.role == this.eRole.Student ? (this.user as Student).promotion : '' , [this.promotionConditionallyRequiredValidator]]
      });
  }

  get f() { return this.signUpForm.controls; }

  promotionConditionallyRequiredValidator(formControl: FormGroup) {  
    
    if (!formControl.parent) {
      return null;
    }
    
    if (formControl.parent.get('role')?.value == ERole.Student) {
      return Validators.required(formControl); 
    }

    return null;
  }

  onSubmit() {
      this.submitted = true;

      if (this.signUpForm.invalid) {
          return;
      }

      if (this.signUpForm.value.role == this.eRole.Student) {
        this.promotions$.pipe(map(promotions => promotions.filter(promotion => promotion.id == (this.signUpForm.value.promotion as Promotion).id)[0])).subscribe((promotion: Promotion) => {
          const student = new Student(
            null!,
            this.signUpForm.value.lastname,
            this.signUpForm.value.firstname,
            this.signUpForm.value.email,
            this.signUpForm.value.phone,
            "123", //password
            promotion
          );
          this.studentService.createStudent(student).subscribe(
            event => {
              this.successMsg = 'Student succesfully created'
              this.errorMsg = '';
              this.signUpForm.reset();
            },
            error => {
              this.successMsg= '';
              this.errorMsg = 'Failed to create student'
            }
            );

        });

      } else if (this.signUpForm.value.role == this.eRole.Teacher) {

        const teacher = new Teacher(
          null!,
          this.signUpForm.value.lastname,
          this.signUpForm.value.firstname,
          this.signUpForm.value.email,
          this.signUpForm.value.phone,
          "123", //password
          []
        );
        
        this.teacherService.createTeacher(teacher).subscribe(
          event => {
            this.successMsg = 'Teacher succesfully created'
            this.errorMsg = '';
            this.signUpForm.reset();
          },
          error => {
            this.successMsg= '';
            this.errorMsg = 'Failed to create teacher';
          }
          );

      } else if (this.signUpForm.value.role == this.eRole.HeadTeacher) {

        const teacher = new HeadTeacher(
          null!,
          this.signUpForm.value.lastname,
          this.signUpForm.value.firstname,
          this.signUpForm.value.email,
          this.signUpForm.value.phone,
          "123", //password
          []
        );
        
        this.teacherService.createTeacher(teacher).subscribe(
          event => {
            this.successMsg = 'Head teacher succesfully created'
            this.errorMsg = '';
            this.signUpForm.reset();
          },
          error => {
            this.successMsg= '';
            this.errorMsg = 'Failed to create head teacher';
          }
          );

      }
      
      this.submitted = false;
  }

  onUpdate() {
    this.submitted = true;

    if (this.signUpForm.invalid) {
      return;
  }

  if (this.signUpForm.value.role == this.eRole.Student) {

    const student = new Student(
      this.user.id,
      this.signUpForm.value.lastname,
      this.signUpForm.value.firstname,
      this.signUpForm.value.email,
      this.signUpForm.value.phone,
      "123", //password
      this.signUpForm.value.promotion
    );
    
    this.studentService.updateStudent(student).subscribe(
      event => {
        this.errorMsg = '';
        this.updateEvent.emit(null);
      },
      error => {
        this.successMsg= '';
        this.errorMsg = 'Failed to update student'
      }
      );;

  } else if (this.signUpForm.value.role == this.eRole.Teacher) {

    const teacher = new Teacher(
      this.user.id,
      this.signUpForm.value.lastname,
      this.signUpForm.value.firstname,
      this.signUpForm.value.email,
      this.signUpForm.value.phone,
      "123", //password
      (this.user as Teacher).unavailability
    );
    
    this.teacherService.updateTeacher(teacher).subscribe(
      event => {
        this.errorMsg = '';
        this.updateEvent.emit(null);
      },
      error => {
        this.successMsg= '';
        this.errorMsg = 'Failed to update teacher'
      }
      );;

  } else if (this.signUpForm.value.role == this.eRole.HeadTeacher) {

    const teacher = new HeadTeacher(
      this.user.id,
      this.signUpForm.value.lastname,
      this.signUpForm.value.firstname,
      this.signUpForm.value.email,
      this.signUpForm.value.phone,
      "123", //password
      (this.user as Teacher).unavailability
    );
    
    this.teacherService.updateTeacher(teacher).subscribe(
      event => {
        this.errorMsg = '';
        this.updateEvent.emit(null);
      },
      error => {
        this.successMsg= '';
        this.errorMsg = 'Failed to update head teacher'
      }
      );;

  }

  }

  onReset() {
      this.submitted = false;
      this.signUpForm.reset();
  }
}