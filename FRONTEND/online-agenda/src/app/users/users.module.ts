import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListTeacherComponent } from './list-teacher/list-teacher.component';
import { ListStudentComponent } from './list-student/list-student.component';
import { StudentDetailComponent } from './student-detail/student-detail.component';
import { TeacherDetailComponent } from './teacher-detail/teacher-detail.component';
import { Routes, RouterModule } from '@angular/router';
import { FormUnavailabilityComponent } from './form-unavailability/form-unavailability.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { FormUserComponent } from './form-user/form-user.component';
import { ListUnavailabilityComponent } from './list-unavailability/list-unavailability.component';

const routes: Routes = [  
  {
    path: 'form-user',
    component: FormUserComponent,
  },
  {
    path: 'list-teacher',
    component: ListTeacherComponent,
  },
  {
    path: 'list-student',
    component: ListStudentComponent,
  },
  {
    path: 'teacher/:id',
    component: TeacherDetailComponent,
  },
  {
    path: 'student/:id',
    component: StudentDetailComponent,
  },
]

@NgModule({
  declarations: [    
    ListTeacherComponent,
    ListStudentComponent,
    StudentDetailComponent,
    TeacherDetailComponent,
    FormUnavailabilityComponent,
    FormUserComponent,
    ListUnavailabilityComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forChild(routes),
  ]
})
export class UsersModule { }
