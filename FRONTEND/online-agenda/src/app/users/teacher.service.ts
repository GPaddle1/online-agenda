import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { EPeriod } from 'shared/models/eperiod';
import { Period } from 'shared/models/period';
import { Teacher } from 'shared/models/teacher';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TeacherService {
  
  endpointApiTeacher = "/User/teacher/";  
  endpointApiUser = "/User/";

  constructor(private httpClient : HttpClient) { }

  public getTeachers() : Observable<Array<Teacher>> {
    return this.httpClient.get<Array<Teacher>>(environment.teacherUrl);
  }
  
  public createTeacher(teacher : Teacher): Observable<Teacher> {
    let data : String;
    let httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
    data = "{\"firstName\": \""+teacher.firstname+"\",\"lastName\": \""+teacher.lastname+"\",\"email\": \""+teacher.email+"\",\"password\": \""+teacher.password+"\",\"unavailability\": \""+JSON.stringify(teacher.unavailability)+"\"}"
      return this.httpClient.post<Teacher>(environment.baseApiUrl + this.endpointApiTeacher, data, httpOptions);
  }

  public updateTeacher(teacher : Teacher): Observable<Teacher> {
    let data : String;
    let httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
    data = "{\"firstName\": \""+teacher.firstname+"\",\"lastName\": \""+teacher.lastname+"\",\"email\": \""+teacher.email+"\",\"password\": \""+teacher.password+"\",\"unavailability\": \""+JSON.stringify(teacher.unavailability)+"\"}"
      return this.httpClient.put<Teacher>(environment.baseApiUrl + this.endpointApiUser + teacher.id, data, httpOptions);
  }

  
  public createUnavailability(teacher: Teacher, period: Period): Observable<Teacher> {
    let data : String;
    let httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
    data = "[{\"date\": \""+period.date+"\",\"ePeriod\": \""+period.ePeriod+"\"}]"
     
    return this.httpClient.put<Teacher>(environment.baseApiUrl + this.endpointApiUser + teacher.id, data, httpOptions);

  }

  public deleteTeacher(teacher : Teacher) {
      return this.httpClient.delete<Teacher>(environment.baseApiUrl + this.endpointApiUser + teacher.id);
  }
}
