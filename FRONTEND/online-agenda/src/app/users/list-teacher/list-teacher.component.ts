import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Teacher } from 'shared/models/teacher';
import { TeacherService } from '../teacher.service';

@Component({
  selector: 'app-list-teacher',
  templateUrl: './list-teacher.component.html',
  styleUrls: ['./list-teacher.component.scss']
})
export class ListTeacherComponent implements OnInit {

  constructor(private teacherService: TeacherService) { }

  teachers$!: Observable<Array<Teacher>>;
  observerTeachers: any;

  ngOnInit(): void {
    this.teachers$ = this.teacherService.getTeachers();
    
    if (this.observerTeachers) {
      this.observerTeachers.unsubscribe();
    }
    this.observerTeachers = this.teachers$.subscribe(
    );
  }

}
