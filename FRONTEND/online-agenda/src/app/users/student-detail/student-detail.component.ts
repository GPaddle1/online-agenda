import { Component, OnInit } from '@angular/core';
import { async } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Student } from 'shared/models/student';
import { StudentService } from '../student.service';
import { ERole } from 'shared/models/erole';

@Component({
  selector: 'app-student-detail',
  templateUrl: './student-detail.component.html',
  styleUrls: ['./student-detail.component.scss']
})
export class StudentDetailComponent implements OnInit {


  constructor(private route: ActivatedRoute, private studentService: StudentService) { }

  editMode = false;
  eRole = ERole;
  student$ !: Observable<Student>;

  ngOnInit(): void {
    this.student$ = this.studentService.getStudents().pipe(
      map(students => students.filter(student => student.id == this.route.snapshot.params.id)[0] )
    );
  }

  onEnterEditMode(): void {
    this.editMode = true;
  }

  onExitEditMode($event: any): void {
    this.editMode = !this.editMode;
  }

  onDelete(): void {    
    this.student$.subscribe((student => {
      this.studentService.deleteStudent(student);
    }))
  }

}
