import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListSchoolMatterComponent } from './list-school-matter.component';

describe('ListSchoolMatterComponent', () => {
  let component: ListSchoolMatterComponent;
  let fixture: ComponentFixture<ListSchoolMatterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListSchoolMatterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListSchoolMatterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
