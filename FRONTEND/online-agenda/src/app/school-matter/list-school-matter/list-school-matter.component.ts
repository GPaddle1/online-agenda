import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { SchoolMatter } from 'shared/models/schoolMatter';
import { SchoolMatterService } from '../school-matter.service';

@Component({
  selector: 'app-list-school-matter',
  templateUrl: './list-school-matter.component.html',
  styleUrls: ['./list-school-matter.component.scss']
})
export class ListSchoolMatterComponent implements OnInit {

  constructor(private schoolMatterService: SchoolMatterService) { }

  schoolMatters$!: Observable<Array<SchoolMatter>>;
  observerSchoolMatters: any;

  ngOnInit(): void {
    this.schoolMatters$ = this.schoolMatterService.getSchoolMatters();
    
    if (this.observerSchoolMatters) {
      this.observerSchoolMatters.unsubscribe();
    }
    this.observerSchoolMatters = this.schoolMatters$.subscribe();
  }

}
