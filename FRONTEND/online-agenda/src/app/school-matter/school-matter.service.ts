import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SchoolMatter } from 'shared/models/schoolMatter';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SchoolMatterService {

  endpointApiSchoolMatter = "/SchoolMatter/";

  constructor(private httpClient : HttpClient) { }

  public getSchoolMatters() : Observable<Array<SchoolMatter>> {
    return this.httpClient.get<Array<SchoolMatter>>(environment.schoolMatterUrl);

    //return this.httpClient.post<SchoolMatter>(environment.baseApiUrl + this.endpointApiSchoolMatter, httpOptions);
  }
  
  public createSchoolMatter(schoolMatter : SchoolMatter): Observable<SchoolMatter> {
    let data : String;
    let httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
      data = "{\"teacherGuid\": \""+schoolMatter.teacher.id+"\",\"duration\": \""+schoolMatter.numberOfLessons+"\",\"name\": \""+schoolMatter.name+"\"}"
      return this.httpClient.post<SchoolMatter>(environment.baseApiUrl + this.endpointApiSchoolMatter, data, httpOptions);
  }

  public updateSchoolMatter(schoolMatter : SchoolMatter): Observable<SchoolMatter> {
    let data : String;
    let httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
      data = "{\"teacherGuid\": \""+schoolMatter.teacher.id+"\",\"duration\": \""+schoolMatter.numberOfLessons+"\",\"name\": \""+schoolMatter.name+"\"}"

      return this.httpClient.put<SchoolMatter>(environment.baseApiUrl + this.endpointApiSchoolMatter + schoolMatter.id, data, httpOptions);
  }

  public deleteSchoolMatter(schoolMatter : SchoolMatter) {
    let httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
      return this.httpClient.delete<SchoolMatter>(environment.baseApiUrl + this.endpointApiSchoolMatter + schoolMatter.id);
  }
}
