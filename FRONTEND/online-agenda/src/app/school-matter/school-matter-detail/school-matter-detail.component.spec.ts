import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SchoolMatterDetailComponent } from './school-matter-detail.component';

describe('SchoolMatterDetailComponent', () => {
  let component: SchoolMatterDetailComponent;
  let fixture: ComponentFixture<SchoolMatterDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SchoolMatterDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SchoolMatterDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
