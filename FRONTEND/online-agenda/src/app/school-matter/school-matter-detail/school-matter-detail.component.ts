import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SchoolMatter } from 'shared/models/schoolMatter';
import { SchoolMatterService } from '../school-matter.service';

@Component({
  selector: 'app-school-matter-detail',
  templateUrl: './school-matter-detail.component.html',
  styleUrls: ['./school-matter-detail.component.scss']
})
export class SchoolMatterDetailComponent implements OnInit {

  constructor(private route: ActivatedRoute, private schoolMatterService: SchoolMatterService) { }

  editMode = false;
  schoolMatter$ !: Observable<SchoolMatter>;

  ngOnInit(): void {
    this.schoolMatter$ = this.schoolMatterService.getSchoolMatters().pipe(
      map(schoolMatters => schoolMatters.filter(schoolMatter => schoolMatter.id == this.route.snapshot.params.id)[0] )
    );
  }
  
  onEnterEditMode(): void {
    this.editMode = true;
  }

  onExitEditMode($event: any): void {
    this.editMode = !this.editMode;
  }

  onDelete(): void {
    this.schoolMatter$.subscribe((schoolMatter => {
      this.schoolMatterService.deleteSchoolMatter(schoolMatter);
}))
  }

}
