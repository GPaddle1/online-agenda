import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListSchoolMatterComponent } from './list-school-matter/list-school-matter.component';
import { FormSchoolMatterComponent } from './form-school-matter/form-school-matter.component';
import { SchoolMatterDetailComponent } from './school-matter-detail/school-matter-detail.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';


const routes: Routes = [
  {
    path: 'form-school-matter',
    component: FormSchoolMatterComponent,
  },
  {
    path: 'list-school-matter',
    component: ListSchoolMatterComponent,
  },
  {
    path: ':id',
    component: SchoolMatterDetailComponent,
  },
]

@NgModule({
  declarations: [
    ListSchoolMatterComponent,
    FormSchoolMatterComponent,
    SchoolMatterDetailComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forChild(routes),
  ]
})
export class SchoolMatterModule { }
