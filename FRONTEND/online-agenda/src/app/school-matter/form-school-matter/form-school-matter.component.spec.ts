import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormSchoolMatterComponent } from './form-school-matter.component';

describe('FormSchoolMatterComponent', () => {
  let component: FormSchoolMatterComponent;
  let fixture: ComponentFixture<FormSchoolMatterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormSchoolMatterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormSchoolMatterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
