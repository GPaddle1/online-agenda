import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SchoolMatter } from 'shared/models/schoolMatter';
import { Teacher } from 'shared/models/teacher';
import { TeacherService } from 'src/app/users/teacher.service';
import { SchoolMatterService } from '../school-matter.service';

@Component({
  selector: 'app-form-school-matter',
  templateUrl: './form-school-matter.component.html',
  styleUrls: ['./form-school-matter.component.scss']
})
export class FormSchoolMatterComponent implements OnInit {

  schoolMatterForm!: FormGroup;
  submitted: boolean = false;

  @Input() schoolMatter !: SchoolMatter;
  @Output() updateEvent: EventEmitter<any> = new EventEmitter();

  teachers$!: Observable<Array<Teacher>>;

  errorMsg : String = '';
  successMsg : String = '';

  constructor(private formBuilder: FormBuilder, private schoolMatterService: SchoolMatterService, private teacherService: TeacherService) { }

  ngOnInit(): void {
    this.teachers$ = this.teacherService.getTeachers();

    this.schoolMatterForm = this.formBuilder.group({
      schoolMatterName: [this.schoolMatter?.name, [Validators.required, Validators.pattern("^[A-Za-zÀ-ÖØ-öø-ÿ-\\s]+$")]],
      numberOfLessons: [this.schoolMatter?.numberOfLessons, [Validators.required, Validators.pattern("^[0-9]+$")]],
      teacher: [this.schoolMatter?.teacher, [Validators.required]]
    });
  }

  get f() { return this.schoolMatterForm.controls; }

  onSubmit() {
    this.submitted = true;

    if (this.schoolMatterForm.invalid) {
        return;
    }

    this.teachers$.pipe(map(teachers => teachers.filter(teacher => teacher.id == (this.schoolMatterForm.value.teacher as Teacher).id)[0])).subscribe((teacher: Teacher) => {
      const schoolMatter = new SchoolMatter(
        null!,
        this.schoolMatterForm.value.lessonName,
        this.schoolMatterForm.value.numberOfLessons,
        this.schoolMatterForm.value.teacher
      );
      this.schoolMatterService.createSchoolMatter(schoolMatter).subscribe(
        event => {
          this.successMsg = 'School matter succesfully created'
          this.errorMsg = '';
        },
        error => {
          this.successMsg= '';
          this.errorMsg = 'Failed to create school matter';
        }
        );
    });   
  }

  onUpdate() {
    this.submitted = true;

    if (this.schoolMatterForm.invalid) {
      return;
  }

  const schoolMatter = new SchoolMatter(
    this.schoolMatter.id,
    this.schoolMatterForm.value.lessonName,
    this.schoolMatterForm.value.numberOfLessons,
    this.schoolMatterForm.value.teacher
  );

  this.schoolMatterService.updateSchoolMatter(schoolMatter).subscribe(
    event => {
      this.errorMsg = '';
      this.updateEvent.emit(null);
    },
    error => {
      this.successMsg= '';
      this.errorMsg = 'Failed to update school matter';
    }
    );
  }

    onReset() {
      this.submitted = false;
      this.schoolMatterForm.reset();
  }

}
