// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {

  baseApiUrl: "https://localhost:49163",
  roleUrl: 'assets/mock/role.json',
  promotionUrl: 'assets/mock/promotion.json',
  lessonUrl: 'assets/mock/lesson.json',
  teacherUrl: 'assets/mock/teacher.json',
  studentUrl: 'assets/mock/student.json',
  semesterScheduleUrl: 'assets/mock/semesterSchedule.json',
  schoolMatterUrl: 'assets/mock/schoolMatter.json',
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
