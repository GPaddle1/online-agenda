describe('Test create lesson time slot', () => {
  it('Visits the home project page', () => {
    cy.visit('/')
  })

  it('Navigate to the create lesson time slot form page', () => {
    cy.get('[data-test="nav-bar-btn-create-calendar"]').click()
    cy.url().should('contain', '/calendar/form-calendar')
    cy.contains('Create lesson time slot')
    cy.contains('Lesson')
    cy.contains('Promotion')
    cy.contains('Semester number')
    cy.contains('Date')
    cy.contains('Period')
  })

  it('Create a lesson time slot', () => {
    cy.get('[data-test="form-calendar-lesson-select-lesson"]').select('Test et validation')
    cy.get('[data-test="form-calendar-lesson-select-promotion"]').select('2020-2023')
    cy.get('[data-test="form-calendar-lesson-select-semester-number"]').select('1')
    cy.get('[data-test="form-calendar-lesson-input-date"]').type('2022-10-06')
    cy.get('[data-test="form-calendar-lesson-select-period"]').select('AM')

    cy.server();
    cy.route('POST', '**/Lesson/semesterSchedule/*/*/*/schoolMatter/*', "200").as('postLesson');

    cy.get('[data-test="form-calendar-lesson-btn-validate"]').click()

    cy.wait('@postLesson').its("status").should("eq", 200);
  })  
})
