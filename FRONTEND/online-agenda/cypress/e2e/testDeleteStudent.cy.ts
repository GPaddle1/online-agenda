describe('Test create promotion', () => {
  it('Visits the home project page', () => {
    cy.visit('/')
  })

  it('Navigate to the student list page', () => {
    cy.get('[data-test="nav-bar-btn-list-student"]').click()
    cy.url().should('contain', '/users/list-student')
  })

  it('Navigate to the student detail page of first element', () => {
    cy.get('[data-test="list-student-btn-detail"]').first().click()
    cy.url().should('contain', '/users/student/1')
  })

  it('Delete student', () => {
    cy.get('[data-test="student-detail-btn-delete"]').click()
  })

})
