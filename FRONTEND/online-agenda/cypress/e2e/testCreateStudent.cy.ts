describe('Test create user student', () => {
  it('Visits the home project page', () => {
    cy.visit('/')
  })

  it('Navigate to the create user form page', () => {
    cy.get('[data-test="nav-bar-btn-form-user"]').click()
    cy.url().should('contain', '/users/form-user')
    cy.contains('Role')
    cy.contains('Lastname')
    cy.contains('Firstname')
    cy.contains('Email')
    cy.contains('Phone')
  })

  it('Create a student', () => {
    
    cy.get('[data-test="form-user-select-role"]').select('student')
    cy.get('[data-test="form-user-input-lastname"]').type('X')
    cy.get('[data-test="form-user-input-firstname"]').type('Y')
    cy.get('[data-test="form-user-input-email"]').type('xx@yy.com')
    cy.get('[data-test="form-user-select-promotion"]').select('2020-2023')
    cy.get('[data-test="form-user-input-phone"]').type('0628692957')

    cy.server();
    cy.route('POST', '**/User/student*', "200").as('postStudent');

    cy.get('[data-test="form-user-btn-validate"]').click()

    cy.wait('@postStudent').its("status").should("eq", 200);
  })  
})
