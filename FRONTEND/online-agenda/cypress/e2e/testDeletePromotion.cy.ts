describe('Test create promotion', () => {
  it('Visits the home project page', () => {
    cy.visit('/')
  })

  it('Navigate to the promotion list page', () => {
    cy.get('[data-test="nav-bar-btn-list-promotion"]').click()
    cy.url().should('contain', '/promotion/list-promotion')
  })

  it('Navigate to the promotion detail page of first element', () => {
    cy.get('[data-test="list-promotion-btn-detail"]').first().click()
    cy.url().should('contain', '/promotion/1')
  })

  it('Delete promotion', () => {
    // cy.server();
    // cy.route('DELETE', '**/Promotion/*', "200").as('deletePromotion');

    cy.get('[data-test="promotion-detail-btn-delete"]').click()

    // cy.wait('@deletePromotion').its("status").should("eq", 200);
  })

})
