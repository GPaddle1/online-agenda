describe('Test create user head teacher', () => {
  it('Visits the home project page', () => {
    cy.visit('/')
  })

  it('Navigate to the create user form page', () => {
    cy.get('[data-test="nav-bar-btn-form-user"]').click()
    cy.url().should('contain', '/users/form-user')
    cy.contains('Role')
    cy.contains('Lastname')
    cy.contains('Firstname')
    cy.contains('Email')
    cy.contains('Phone')
  })

  it('Create a head teacher', () => {
    cy.get('[data-test="form-user-select-role"]').select('head teacher')
    cy.get('[data-test="form-user-input-lastname"]').type('BossX')
    cy.get('[data-test="form-user-input-firstname"]').type('BossY')
    cy.get('[data-test="form-user-input-email"]').type('BossX@BossY.com')
    cy.get('[data-test="form-user-input-phone"]').type('0628692957')

    cy.server();
    cy.route('POST', '**/User/teacher*', "200").as('postTeacher');

    cy.get('[data-test="form-user-btn-validate"]').click()

    cy.wait('@postTeacher').its("status").should("eq", 200);
  })  
})
