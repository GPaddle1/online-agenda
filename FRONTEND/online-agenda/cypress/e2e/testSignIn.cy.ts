describe('Test sign in', () => {
  it('Visits the home project page', () => {
    cy.visit('/')
  })

  it('Navigate to the sign in page', () => {
    cy.get('[data-test="header-btn-sign-in"]').click()
    cy.url().should('contain', '/auth/sign-in')
    cy.contains('Sign In')
  })

  it('Sign in', () => {
    cy.get('[data-test="sign-in-input-email"]').type('BossX@BossY.com')
    cy.get('[data-test="sign-in-input-password"]').type('XXX')

    cy.server();
    cy.route('PUT', '**/User/login*', "200").as('postLogin');

    cy.get('[data-test="sign-in-btn-submit"]').click()

    cy.wait('@postLogin').its("status").should("eq", 200);
  })

})
