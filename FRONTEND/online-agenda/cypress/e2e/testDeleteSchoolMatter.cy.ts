describe('Test create school matter', () => {
  it('Visits the home project page', () => {
    cy.visit('/')
  })

  it('Navigate to the school matter list page', () => {
    cy.get('[data-test="nav-bar-btn-list-school-matter"]').click()
    cy.url().should('contain', '/school-matter/list-school-matter')
  })

  it('Navigate to the school matter detail page of first element', () => {
    cy.get('[data-test="list-school-matter-btn-detail"]').first().click()
    cy.url().should('contain', '/school-matter/1')
  })

  it('Delete school matter', () => {
    cy.get('[data-test="school-matter-detail-btn-delete"]').click()
  })

})
