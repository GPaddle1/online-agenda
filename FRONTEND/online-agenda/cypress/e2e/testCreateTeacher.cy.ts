describe('Test create user teacher', () => {
  it('Visits the home project page', () => {
    cy.visit('/')
  })

  it('Navigate to the create user form page', () => {
    cy.get('[data-test="nav-bar-btn-form-user"]').click()
    cy.url().should('contain', '/users/form-user')
    cy.contains('Role')
    cy.contains('Lastname')
    cy.contains('Firstname')
    cy.contains('Email')
    cy.contains('Phone')
  })

  it('Create a teacher', () => {
    cy.get('[data-test="form-user-select-role"]').select('teacher')
    cy.get('[data-test="form-user-input-lastname"]').type('X')
    cy.get('[data-test="form-user-input-firstname"]').type('Y')
    cy.get('[data-test="form-user-input-email"]').type('X@Y.com')
    cy.get('[data-test="form-user-input-phone"]').type('0628692957')

    cy.server();
    cy.route('POST', '**/User/teacher*', "200").as('postTeacher');

    cy.get('[data-test="form-user-btn-validate"]').click()

    cy.wait('@postTeacher').its("status").should("eq", 200);
  })  
})
