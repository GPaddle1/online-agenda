describe('Test create promotion', () => {
  it('Visits the home project page', () => {
    cy.visit('/')
  })

  it('Navigate to the teacher list page', () => {
    cy.get('[data-test="nav-bar-btn-list-teacher"]').click()
    cy.url().should('contain', '/users/list-teacher')
  })

  it('Navigate to the teacher detail page of first element', () => {
    cy.get('[data-test="list-teacher-btn-detail"]').first().click()
    cy.url().should('contain', '/users/teacher/1')
  })

  it('Create unavailability', () => {
    cy.get('[data-test="form-unavailability-input-date"]').type('2022-06-07');
    cy.get('[data-test="form-unavailability-select-period"]').select('AM')

    cy.server();
    cy.route('PUT', '**/User/*', "200").as('putTeacherUnavailability');

    cy.get('[data-test="form-unavailability-btn-validate"]').click()

    cy.wait('@putTeacherUnavailability').its("status").should("eq", 200);
  })  

})
