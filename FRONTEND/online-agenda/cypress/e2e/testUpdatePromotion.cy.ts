describe('Test create promotion', () => {
  it('Visits the home project page', () => {
    cy.visit('/')
  })

  it('Navigate to the promotion list page', () => {
    cy.get('[data-test="nav-bar-btn-list-promotion"]').click()
    cy.url().should('contain', '/promotion/list-promotion')
  })

  it('Navigate to the promotion detail page of first element', () => {
    cy.get('[data-test="list-promotion-btn-detail"]').first().click()
    cy.url().should('contain', '/promotion/1')
  })

  it('Enter edit mode', () => {
    cy.get('[data-test="promotion-detail-btn-edit"]').click()
  })

  it('Update a promotion', () => {
    cy.get('[data-test="form-promotion-input-name"]').clear()
    cy.get('[data-test="form-promotion-input-name"]').type('new promotion')

    cy.server();
    cy.route('PUT', '**/Promotion/*', "200").as('putPromotion');

    cy.get('[data-test="form-promotion-btn-validate"]').click()

    cy.wait('@putPromotion').its("status").should("eq", 200);
  })

})
