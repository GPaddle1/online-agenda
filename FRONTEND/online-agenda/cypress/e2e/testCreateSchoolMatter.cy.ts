describe('Test create school matter', () => {
  it('Visits the home project page', () => {
    cy.visit('/')
  })

  it('Navigate to the school matter form page', () => {
    cy.get('[data-test="nav-bar-btn-form-school-matter"]').click()
    cy.url().should('contain', '/school-matter/form-school-matter')
    cy.contains('School matter Name')
  })

  it('Create a school matter', () => {
    cy.get('[data-test="form-school-matter-input-name"]').type('test school matter')
    cy.get('[data-test="form-school-matter-input-number-of-lessons"]').type('15')
    cy.get('[data-test="form-school-matter-select-teacher"]').select('firstname teacher')

    cy.server();
    cy.route('POST', '**/Promotion*', "200").as('postPromotion');

    cy.get('[data-test="form-school-matter-btn-validate"]').click()

    cy.wait('@postPromotion').its("status").should("eq", 200);
  })

})
