describe('Test create promotion', () => {
  it('Visits the home project page', () => {
    cy.visit('/')
  })

  it('Navigate to the teacher list page', () => {
    cy.get('[data-test="nav-bar-btn-list-teacher"]').click()
    cy.url().should('contain', '/users/list-teacher')
  })

  it('Navigate to the teacher detail page of first element', () => {
    cy.get('[data-test="list-teacher-btn-detail"]').first().click()
    cy.url().should('contain', '/users/teacher/1')
  })

  it('Delete unavailability', () => {
    cy.get('[data-test="list-unavailability-btn-delete"]').first().click()
  })  

})
