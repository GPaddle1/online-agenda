describe('Test create promotion', () => {
  it('Visits the home project page', () => {
    cy.visit('/')
  })

  it('Navigate to the teacher list page', () => {
    cy.get('[data-test="nav-bar-btn-list-teacher"]').click()
    cy.url().should('contain', '/users/list-teacher')
  })

  it('Navigate to the teacher detail page of first element', () => {
    cy.get('[data-test="list-teacher-btn-detail"]').first().click()
    cy.url().should('contain', '/users/teacher/1')
  })

  it('Enter edit mode', () => {
    cy.get('[data-test="teacher-detail-btn-edit"]').click()
  })

  it('Update teacher', () => {
    cy.get('[data-test="form-user-select-role"]').select('teacher')
    cy.get('[data-test="form-user-input-lastname"]').clear()
    cy.get('[data-test="form-user-input-lastname"]').type('X')
    cy.get('[data-test="form-user-input-firstname"]').clear()
    cy.get('[data-test="form-user-input-firstname"]').type('Y')
    cy.get('[data-test="form-user-input-email"]').clear()
    cy.get('[data-test="form-user-input-email"]').type('xx@yy.com')
    cy.get('[data-test="form-user-input-phone"]').clear()
    cy.get('[data-test="form-user-input-phone"]').type('0628692957')

    cy.server();
    cy.route('PUT', '**/User/*', "200").as('putTeacher');

    cy.get('[data-test="form-user-btn-validate"]').click()

    cy.wait('@putTeacher').its("status").should("eq", 200);
  })  

})
