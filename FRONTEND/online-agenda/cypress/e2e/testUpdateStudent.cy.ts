describe('Test create promotion', () => {
  it('Visits the home project page', () => {
    cy.visit('/')
  })

  it('Navigate to the student list page', () => {
    cy.get('[data-test="nav-bar-btn-list-student"]').click()
    cy.url().should('contain', '/users/list-student')
  })

  it('Navigate to the student detail page of first element', () => {
    cy.get('[data-test="list-student-btn-detail"]').first().click()
    cy.url().should('contain', '/users/student/1')
  })

  it('Enter edit mode', () => {
    cy.get('[data-test="student-detail-btn-edit"]').click()
  })

  it('Update student', () => {
    cy.get('[data-test="form-user-select-role"]').select('student')
    cy.get('[data-test="form-user-input-lastname"]').clear()
    cy.get('[data-test="form-user-input-lastname"]').type('X')
    cy.get('[data-test="form-user-input-firstname"]').clear()
    cy.get('[data-test="form-user-input-firstname"]').type('Y')
    cy.get('[data-test="form-user-input-email"]').clear()
    cy.get('[data-test="form-user-input-email"]').type('xx@yy.com')
    cy.get('[data-test="form-user-input-phone"]').clear()
    cy.get('[data-test="form-user-input-phone"]').type('0628692957')

    cy.server();
    cy.route('PUT', '**/User/*', "200").as('putStudent');

    cy.get('[data-test="form-user-btn-validate"]').click()

    cy.wait('@putStudent').its("status").should("eq", 200);
  })  

})
