describe('Test create promotion', () => {
  it('Visits the home project page', () => {
    cy.visit('/')
  })

  it('Navigate to the create promotion form page', () => {
    cy.get('[data-test="nav-bar-btn-form-promotion"]').click()
    cy.url().should('contain', '/promotion/form-promotion')
    cy.contains('Promotion Name')
  })

  it('Create a promotion', () => {
    cy.get('[data-test="form-promotion-input-name"]').type('X')

    cy.server();
    cy.route('POST', '**/Promotion*', "200").as('postPromotion');

    cy.get('[data-test="form-promotion-btn-validate"]').click()

    cy.wait('@postPromotion').its("status").should("eq", 200);
  })
  
})
