describe('Test create semester schedule', () => {
  it('Visits the home project page', () => {
    cy.visit('/')
  })

  it('Navigate to the create semester schedule form page', () => {
    cy.get('[data-test="nav-bar-btn-create-calendar"]').click()
    cy.url().should('contain', '/calendar/form-calendar')
    cy.contains('Create semester schedule')
    cy.contains('Promotion')
    cy.contains('Semester number')
    cy.contains('Start date')
    cy.contains('End date')
  })

  it('Create a semester schedule', () => {
    cy.get('[data-test="form-calendar-select-promotion"]').select('2020-2023')
    cy.get('[data-test="form-calendar-input-semester-number"]').type('1')
    cy.get('[data-test="form-calendar-input-start-date"]').type('2022-08-06')
    cy.get('[data-test="form-calendar-input-end-date"]').type('2022-10-06')

    cy.server();
    cy.route('POST', '**/SemesterSchedule*', "200").as('postSemesterSchedule');

    cy.get('[data-test="form-calendar-btn-validate"]').click()

    cy.wait('@postSemesterSchedule').its("status").should("eq", 200);
  })  
})
