describe('Test create school matter', () => {
  it('Visits the home project page', () => {
    cy.visit('/')
  })

  it('Navigate to the school matter list page', () => {
    cy.get('[data-test="nav-bar-btn-list-school-matter"]').click()
    cy.url().should('contain', '/school-matter/list-school-matter')
  })

  it('Navigate to the school matter detail page of first element', () => {
    cy.get('[data-test="list-school-matter-btn-detail"]').first().click()
    cy.url().should('contain', '/school-matter/1')
  })

  it('Enter edit mode', () => {
    cy.get('[data-test="school-matter-detail-btn-edit"]').click()
  })

  it('Update a school matter', () => {
    cy.get('[data-test="form-school-matter-input-name"]').clear()
    cy.get('[data-test="form-school-matter-input-name"]').type('new school matter')
    cy.get('[data-test="form-school-matter-input-number-of-lessons"]').clear()
    cy.get('[data-test="form-school-matter-input-number-of-lessons"]').type('25')
    cy.get('[data-test="form-school-matter-select-teacher"]').select('firstname teacher')
    cy.get('[data-test="form-school-matter-btn-validate"]').click()

    cy.server();
    cy.route('PUT', '**/SchoolMatter/*', "200").as('putSchoolMatter');

    cy.get('[data-test="form-user-btn-validate"]').click()

    cy.wait('@putSchoolMatter').its("status").should("eq", 200);
  })

})
